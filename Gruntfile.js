
var xml = require('libxmljs');
var fs = require('fs');
var libPath = require('path');
var _ = require("underscore")._;

module.exports = function(grunt) {

    // Below environment variables are used by build system
    var ENV = process.env.PMTOOL_ENV || "local";
    var BUILD_CONFIG_FILE = process.env.PMTOOL_BUILD_CONFIG;
    var APP_CONFIG_FILE = process.env.PMTOOL_APP_CONFIG;

    var unixTimestamp = Date.now(); // in milliseconds
    var minuteStamp = (unixTimestamp / 1000 / 60).toFixed(0);
    var BUILD_VERSION = minuteStamp;

    var SRC_ROOT_FOLDER = "web";
    var DEST_ROOT_FOLDER = "webminified";
    var PACKAGE_ROOT_FOLDER = "dist/pmtool";
    var SRC_APPVERSION_FOLDER = SRC_ROOT_FOLDER + "/appversion";
    var DEST_APPVERSION_FOLDER = DEST_ROOT_FOLDER + "/" + BUILD_VERSION;

    // The default application configuration file referenced from index.html
    var DEFAULT_APP_CONFIG_FILE = SRC_APPVERSION_FOLDER + "/app/core/config.js";

    // The optional build configuration data used by build system
    var BUILD_CONFIG = {};
    if (BUILD_CONFIG_FILE && grunt.file.exists(BUILD_CONFIG_FILE)) {
        BUILD_CONFIG = grunt.file.readJSON(BUILD_CONFIG_FILE);
        //grunt.log.write("BUILD_CONFIG: " + JSON.stringify(BUILD_CONFIG));
    }

    var parseHtml = function(filePath) {
        var str, doc;
        try {
            str = fs.readFileSync(filePath);
            doc = xml.parseHtml(str, {
                recover: true
            });
        } catch (err) {
            grunt.log.error("Can't parse html file: " + filePath);
        }

        if (doc) {
            // get referenced scripts
            var scripts = [];
            var scriptElements = doc.find('head/script');
            _.each(scriptElements, function(e) {
                var attr = e.attr('src');
                if (attr) {
                    var src = attr.value();
                    if (src && !_.contains(scripts, src)) {
                        scripts.push(src);
                    }
                }
            });

            // get referenced css files
            var styles = [];
            var linkElements = doc.find('head/link');
            _.each(linkElements, function(e) {
                if (e.attr('href') && e.attr('rel')) {
                    if (e.attr('rel').value() == 'stylesheet') {
                        var href = e.attr('href').value();
                        if (href && !_.contains(styles, href)) {
                            styles.push(href);
                        }
                    }
                }
            });

            return {
                html: filePath,
                scripts: scripts,
                styles: styles
            };
        }

        return undefined;
    };

    var parseHtmls = function(patterns) {
        var groups = [];
        var option = {
            filter: 'isFile',
            matchBase: true
        };
        var paths = grunt.file.expand(patterns);
        _.each(paths, function(path) {
            if (grunt.file.isMatch(option, "*.html", path)) {
                var ret = parseHtml(path);
                if (ret) {
                    groups.push(ret);
                }
            }
        });

        return groups;
    };

    var replaceHtml = function(content, filePath) {
        var minifiedFile = getAppJsMinifiedFile(filePath);
        //var minifiedFile = getAppJsCombinedFile(filePath);
        if (!grunt.file.exists(minifiedFile)) {
            grunt.log.error("Can't find minified file: " + minifiedFile);
            return content;
        }

        try {
            var doc = xml.parseHtml(content, {
                recover: true,
                noblanks: true,
                noent: true
            });

            // Replace the single script with a combined/minified version
            var scriptElements = doc.find('head/script');
            _.each(scriptElements, function(e) {
                var attr = e.attr('src');
                if (attr) {
                    var src = attr.value();
                    if (src && grunt.file.isMatch("appversion/app/**", src)) {
                        var pre = e.prevSibling();
                        if (pre.type() == 'text' && !/\S/.test(pre.text())) {
                            pre.remove();
                        }

                        e.remove();
                    }
                }
            });

            // Add the combined and minified version of app script file
            var headerNode = doc.get('head');
            var node = headerNode.node('script');

            var relpath = libPath.relative(DEST_ROOT_FOLDER, minifiedFile);
            node.attr({
                type: "text/javascript",
                src: relpath
            });

            content = doc.toString();
        } catch (err) {
            grunt.log.error("Can't replace html file: " + filePath);
        }

        return content;
    };

    var replaceVersion = function(content) {
        return content.replace(/appversion\//g, BUILD_VERSION + "/");
    };

    var getAppJsCombinedFile = function(filePath) {
        var destFile = DEST_APPVERSION_FOLDER + "/app.js";
        var o = libPath.parse(filePath);
        if (o.name !== "index") {
            destFile = destFolder + o.name + ".js";
        }

        return destFile;
    };

    var getAppJsMinifiedFile = function(filePath) {
        var destFile = getAppJsCombinedFile(filePath);
        return destFile.replace(".js", ".min.js");
    };

    var getPageAppJsFile = function(page) {
        var appFiles = [];
        _.each(page.scripts, function(path) {
            if (grunt.file.isMatch("appversion/app/**", path)) {
                appFiles.push(libPath.join(SRC_ROOT_FOLDER, path));
            }
        });

        // Use the environment specific configuration
        var configFile = APP_CONFIG_FILE;
        if (configFile && grunt.file.exists(configFile)) {
            grunt.log.writeln("The app configuration is : " + configFile);
            var index = _.indexOf(appFiles, DEFAULT_APP_CONFIG_FILE);
            appFiles[index] = configFile;
        }

        return {
            src: appFiles,
            dest: getAppJsCombinedFile(page.html)
        };
    };

    var pages = parseHtmls("web/*.html");

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        'http-server': {

            'dev': {

                // the server root directory
                root: SRC_ROOT_FOLDER,

                // the server port
                // can also be written as a function, e.g.
                // port: function() { return 8282; }
                port: 8001,

                // the host ip address
                // If specified to, for example, "127.0.0.1" the server will
                // only be available on that ip.
                // Specify "0.0.0.0" to be available everywhere
                host: "0.0.0.0",

                cache: 0,
                showDir: true,
                autoIndex: true,

                // server default file extension
                ext: "html",

                // run in parallel with other tasks
                runInBackground: false,

                // // specify a logger function. By default the requests are
                // // sent to stdout.
                // logFn: function(req, res, error) { },

                // // Proxies all requests which can't be resolved locally to the given url
                // // Note this this will disable 'showDir'
                // proxy: "http://someurl.com",

                // /// Use 'https: true' for default module SSL configuration
                // /// (default state is disabled)
                // https: {
                //     cert: "cert.pem",
                //     key : "key.pem"
                // },

                // Tell grunt task to open the browser
                openBrowser: false
            },
            'minified': {
                // the server root directory
                root: './webminified',

                // the server port
                // can also be written as a function, e.g.
                // port: function() { return 8282; }
                port: 8002,

                // the host ip address
                // If specified to, for example, "127.0.0.1" the server will
                // only be available on that ip.
                // Specify "0.0.0.0" to be available everywhere
                host: "localhost",

                cache: 0,
                showDir: true,
                autoIndex: true,

                // server default file extension
                ext: "html",

                // run in parallel with other tasks
                runInBackground: false,

                // Tell grunt task to open the browser
                openBrowser: true
            },
            'dist': {
                // the server root directory
                root: './dist',

                // the server port
                // can also be written as a function, e.g.
                // port: function() { return 8282; }
                port: 8003,

                // the host ip address
                // If specified to, for example, "127.0.0.1" the server will
                // only be available on that ip.
                // Specify "0.0.0.0" to be available everywhere
                host: "0.0.0.0",

                cache: 0,
                showDir: true,
                autoIndex: true,

                // server default file extension
                ext: "html",

                // run in parallel with other tasks
                runInBackground: false,

                // Tell grunt task to open the browser
                openBrowser: true
            }
        },
        jshint: {
            all: [
                'appversion/app/*',
                'Gruntfile.js'
            ],
            options: {
                jshintrc: '.jshintrc'
            }
        },
        bump: {
            options: {
                files: ['package.json'],
                updateConfigs: [],
                commit: true,
                commitMessage: 'Release v%VERSION%',
                commitFiles: ['package.json'],
                createTag: true,
                tagName: 'v%VERSION%',
                tagMessage: 'Version %VERSION%',
                push: true,
                pushTo: 'upstream',
                gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d',
                globalReplace: false,
                prereleaseName: false,
                metadata: '',
                regExp: false
            }
        },
        clean: {
            minified: [DEST_ROOT_FOLDER],
            dist: ["dist/"]
        },
        concat: {
            options: {
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */\n\n',
                process: function(content, srcPath) {
                    var msg = 'Copy and replace "appversion": ' + srcPath;
                    grunt.log.writeln(msg);
                    return replaceVersion(content);
                }
            },
            dist: {
                files: _.map(pages, getPageAppJsFile)
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            build: {
                files: _.map(pages, function(page) {
                    return {
                        src: getAppJsCombinedFile(page.html),
                        dest: getAppJsMinifiedFile(page.html)
                    };
                })
            }
        },
        copy: {
            'dev-html': {
                // Copy and modify all *.html file with including minified version file
                options: {
                    process: function(content, srcPath) {
                        var msg = 'Copy and replace "appversion": ' + srcPath;
                        grunt.log.writeln(msg);
                        return replaceVersion(replaceHtml(content, srcPath));
                    }
                },
                files: [{
                    expand: true,
                    cwd: SRC_ROOT_FOLDER,
                    src: ['*.html'],
                    dest: DEST_ROOT_FOLDER
                }]
            },
            'dev-libs': {
                // Copy the included 3d library script/css files under web/libs directly
                files: [{
                    expand: true,
                    cwd: SRC_APPVERSION_FOLDER,
                    src: ['libs/**', '!libs/angular-1.4.8/docs/**'],
                    dest: DEST_APPVERSION_FOLDER
                }]
            },
            'dev-other': {
                options: {
                    process: function(content, srcPath) {
                        // var patterns = ["*.html", "*.js", "*.json", "*.css", "*.less"];
                        // var option = {
                        //     filter: 'isFile',
                        //     matchBase: true
                        // };
                        // if (grunt.file.isMatch(option, patterns, path)) {
                        //     var msg = 'Copy and replace "appversion": ' + srcPath;
                        //     grunt.log.writeln(msg);
                        //     return replaceVersion(content);
                        // }

                        var msg = 'Copy and replace "appversion": ' + srcPath;
                        grunt.log.writeln(msg);
                        return replaceVersion(content);
                    }
                },
                files: [
                    // copy all resource files under res folder directly
                    {
                        expand: true,
                        cwd: SRC_APPVERSION_FOLDER,
                        src: ['res/**', '!res/images/**'],
                        dest: DEST_APPVERSION_FOLDER
                    },

                    // copy all non-script files under app folder directly
                    {
                        expand: true,
                        cwd: SRC_APPVERSION_FOLDER,
                        src: ['app/**'],
                        dest: DEST_APPVERSION_FOLDER,
                        filter: function(filepath) {
                            return !grunt.file.isMatch(SRC_APPVERSION_FOLDER + '/app/**/*.js', filepath);
                        }
                    }
                ]
            },
            'dev-images': {
                files: [
                    // copy all images files under res folder directly
                    {
                        expand: true,
                        cwd: SRC_APPVERSION_FOLDER,
                        src: ['res/images/**'],
                        dest: DEST_APPVERSION_FOLDER
                    }
                ]
            },
            'minified': {
                // Copy all files under webminified folder to dist folder
                files: [{
                    expand: true,
                    cwd: DEST_ROOT_FOLDER,
                    src: ['**'],
                    dest: PACKAGE_ROOT_FOLDER
                }]
            }
        },
        compress: {
            archive: {
                options: {
                    archive: 'dist/pmtool.zip'
                },
                files: [{
                    expand: true,
                    cwd: PACKAGE_ROOT_FOLDER,
                    src: ['**'],
                    dest: ''
                }]
            }
        },
        aws: BUILD_CONFIG["AWS"],
        s3: {
            options: {
                accessKeyId: "<%= aws.accessKeyId %>",
                secretAccessKey: "<%= aws.secretAccessKey %>",
                bucket: "<%= aws.bucket %>",
                region: "<%= aws.region %>",
                gzip: true
            },
            all: {
                cwd: PACKAGE_ROOT_FOLDER,
                src: "**"
            },
            index: {
                options: {
                    headers: {
                        CacheControl: 'max-age=900, public, must-revalidate'
                    },
                },
                cwd: PACKAGE_ROOT_FOLDER,
                src: "*.html"
            }
        }
    });

    grunt.loadNpmTasks('grunt-http-server');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-bump');
    grunt.loadNpmTasks('grunt-aws');

    grunt.registerTask('copy-dev', ['copy:dev-html', 'copy:dev-libs', 'copy:dev-other', 'copy:dev-images']);
    grunt.registerTask('build', ['clean:minified', 'concat', 'uglify', 'copy-dev']);
    grunt.registerTask('package', ['clean:dist', 'copy:minified', 'compress']);
    grunt.registerTask('upload', ['s3:all', 's3:index']);

    // Default task.
    grunt.registerTask('default', ['jshint']);
};
