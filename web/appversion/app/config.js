
/*
    Configure service here. The codes are running before the codes in robinApp.run()

*/
// $http service
robinApp.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
}]);
robinApp.factory('authInterceptor',
    ["$rootScope", "$q", "StorageService", "AUTH_EVENTS", "$injector",
    function ($rootScope, $q, StorageService, AUTH_EVENTS, $injector) {
    return {
        request: function (config) {

            var isAPI = (function (url){
                var r = new RegExp('^\/?(mw|robin|seek)', 'i');
                return r.test(url);
            })(config.url);

            if (isAPI) {
                config.headers = config.headers || {};

                var token = StorageService.get("access_token");
                if (token) {
                    config.headers.Authorization = "Bearer " + token;
                }

                // use the absolute url for API call
                config.url = robinConsts.apiEndpoint + "/" + config.url.replace(/^\//, ""); // Remove the header slash
            }

            return config;
        },
        responseError: function (response) {
            // TODO: Refine below error handling once server side respond the correct status code
            var $route = $injector.get('$route');
            var $scope = $route.current.scope;
            $scope.loading = false;

            var args = {
                message: response.status + " " + response.statusText + " </br>" + response.data + ": " + response.config.method + " " + response.config.url,
                redirect: false
            };
            if (response.status === 401 && response.data == "No permission") {
                $rootScope.$broadcast(AUTH_EVENTS.notAuthorized, args);
            } else if (response.status === 401 && response.data == "Incorrect password") {
                $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated, args);
            } else if (response.status === 401 && response.data == "Invalid access token") {
                args.redirect = true;
                $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated, args);
            } else if (response.status === 401 && response.data == "Session not found/Session timeout") {
                args.redirect = true;
                $rootScope.$broadcast(AUTH_EVENTS.sessionTimeout, args);
            }

            return $q.reject(response);
        }
    };
}]);


//localization
robinApp.constant('LOCALES', {
    'locales':{
        'zh_CN' : '\u4e2d\u6587',
        'en_US' : 'English'
    },
    'preferredLocale' : 'en_US'
});

robinApp.config(['$translateProvider', function($translateProvider){
    $translateProvider.useMissingTranslationHandlerLog();

    $translateProvider.useStaticFilesLoader({
        prefix: 'appversion/app/resources/locale-',
        suffix: '.json'
    });
    $translateProvider.preferredLanguage('en_US');
    $translateProvider.useLocalStorage();
}]);

robinApp.config(['tmhDynamicLocaleProvider', function(tmhDynamicLocaleProvider){
    tmhDynamicLocaleProvider.localeLocationPattern('appversion/libs/angular-1.4.8/i18n/angular-locale_{{locale}}.js');
}]);
