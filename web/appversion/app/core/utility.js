(function(module) {
    var Logger = window.console;

    var Utility = {};
    Utility.inherits = function(childCtor, parentCtor) {
        /** @constructor */
        function tempCtor() {};
        tempCtor.prototype = parentCtor.prototype;
        childCtor.superClass_ = parentCtor.prototype;
        childCtor.prototype = new tempCtor();
        /** @override */
        childCtor.prototype.constructor = childCtor;
    };

    // polyfill utility
    var polyfill = function (target, name, polyfillfunc) {
        if (target[name]) {
            // skip adding polyfill if it already exists
            return;
        }

        // try add polyfill method via Object.defineProperty
        if (Object.defineProperty) {
            Object.defineProperty(target, name, {
                value: polyfillfunc,
                configurable: true,
                enumerable: false,
                writable: true
            });
        }

        // add polyfill method directly
        if (!target[name]) {
            target[name] = polyfillfunc;
        }
    };

    polyfill(Array.prototype, 'includes', function (searchElement, fromIndex) {
        var O = Object(this);
        var len = parseInt(O.length) || 0;
        if (len === 0) {
            return false;
        }
        var n = parseInt(arguments[1]) || 0;
        var k;
        if (n >= 0) {
            k = n;
        } else {
            k = len + n;
            if (k < 0) {k = 0;}
        }
        var currentElement;
        while (k < len) {
            currentElement = O[k];
            if (searchElement === currentElement ||
                (searchElement !== searchElement && currentElement !== currentElement)) {
                return true;
            }
            k++;
        }
        return false;
    });

    polyfill(Array.prototype, 'contains', Array.prototype.includes);

    polyfill(Array.prototype, 'isArray', function(arg) {
        return Object.prototype.toString.call(arg) === '[object Array]';
    });

    polyfill(String.prototype, 'trim', function() {
        return String(this).replace(/^\s+|\s+$/g, '');
    });


    var TreeVisitor = {};
    TreeVisitor.filterNode = function (root, filter) {
        var _filterNode = function (node) {
            if (!filter(node)) {
                return undefined;
            }

            var newNode = {};
            for (var key in node) {
                if (node.hasOwnProperty(key)) {
                    newNode[key] = node[key];
                }
            }

            newNode.children = [];
            if (node.children) {
                node.children.forEach(function (_node) {
                    var _newNode = _filterNode(_node);
                    if (_newNode) {
                        newNode.children.push(_newNode);
                    }
                });
            }
            return newNode;
        };

        if (Array.isArray(root)) {
            var nodes = [];
            root.forEach(function (node) {
                var newNode = _filterNode(node);
                if (newNode) {
                    nodes.push(newNode);
                }
            });
            return nodes;
        } else {
            return _filterNode(root);
        }
    };

    TreeVisitor.findFirst = function (node, filter) {
        return findNode(node, filter, FIND_RULE.First);
    };

    TreeVisitor.findAll = function (node, filter) {
        return findNode(node, filter, FIND_RULE.All);
    };

    TreeVisitor.findBranch = function (node, filter) {
        return findNode(node, filter, FIND_RULE.Branch);
    };

    var FIND_RULE = {
        First: 0, // return the first match node
        All: 1, // return all match nodes
        Branch: 2 // return all the match node with exclude it children
    };

    var findNode = function (root, filter, rule) {
        var targets = [];

        var _rule = rule || FIND_RULE.All;

        var _findNode = function (node) {
            var found = filter(node);
            if (found) {
                targets.push(node);

                if (FIND_RULE.First == rule) {
                    return true;
                }
            }
            if (node.children) {
                if (FIND_RULE.All == rule ||
                    (!found && (FIND_RULE.Branch == rule || FIND_RULE.First == rule))) {
                    for (var i = 0; i < node.children.length; ++i) {
                        if (_findNode(node.children[i])) {
                            found = true;
                            if (FIND_RULE.First == rule) {
                                break;
                            }
                        }
                    }
                }
            }

            return found;
        };

        if (Array.isArray(root)) {
            for (var i = 0; i < root.length; ++i) {
                var found = _findNode(root[i])
                if (found && FIND_RULE.First == rule) {
                    break;
                }
            }
        } else {
            _findNode(root);
        }

        return targets;
    };

    module.TreeVisitor = TreeVisitor;
    module.Logger = Logger;
    module.Utility = Utility;
})(window);
