var robinConsts = {
    env: 'local',

    // server global values
    appVersion: '1.5.9',
    //tenantId: 'hsm', // use $rootScpe.tenantId instead
    lang: 'en_US',
    s3IconPathCategory: 'https://hsm-dev-assets.s3.amazonaws.com/Categories/NewVersion/',
    s3IconPathTenant: 'https://hsm-dev-assets.s3.amazonaws.com/Tenants/',
    s3ISOPathCategory: 'https://hsm-dev-assets.s3.amazonaws.com/Categories/iso/',

    // js global values
    languages: [
        { code: "en_US", name: "English" },
        { code: "zh_CN", name: "Chinese" },
        { code: "de_DE", name: "German" },
        { code: "es_ES", name: "Spanish" },
        { code: "fr_FR", name: "French" },
        { code: "it_IT", name: "Italian" },
        { code: "ja", name: "Japanese" },
        { code: "pt_BR", name: "Portuguese" },
        { code: "ru", name: "Russian" }
    ],

    apiEndpoint: "http://localhost:9001",
    URLFor3DViewer: ""
};
