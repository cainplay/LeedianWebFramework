(function(module) {
    var AppModule = {
        // Tag Group
        Categories:"categories",
        Brands:"brands",
        Attributes:"attributes",
        Retailers:"retailers",
        Families:"families",

        // Products group
        Products:"products",
        UploadProduct:"upload-product",
        Variation:"variation",
        Search:"search",
        SearchFamily:"searchFamily",
        EditFamily:"editFamily",
        ProductsCategorization:"products-categorization",

        // Users group
        Users:"users",

        // Migration/tenants group
        Migration:"migration",
        Tenants:"tenants",
        CreateTenant:"create-tenant",
        EditTenant:"edit-tenant",
        AddPackageToTenant:"addPackageToTenant",

        // I don't know
        MostSearchedKeywords:"mostSearchedKeywords",
        MenuProductAddEdit:"menuProductAddEdit",

        // User group
        EditProfile:"edit-profile" ,
        Upload:"upload",
        Login:"login",
        SignUp:"signup",
        ForgotPassword:"forgot-password",
        ResetPassword:"reset-password",
        ResetPasswordError:"reset-password-error",

        defaultLanguage:"en_US",

        TopView:"topView",

        ModelingGuideline:"modeling-guideline",
        ChineseGuideline:"chinese-guideline",
    };

    var Method = {
        GET: 101,
        PUT: 102,
        PATCH: 103,
        DELETE: 104,
        POST: 105,
        ALL:106
    };


    var CategoryAttributes = {
        PocketMaterial: 'pocket'
    };

    var AttributeNames = {
       ZIndex : "zindex",
       IsScalable : "isScalable",
       HasPocket : "hasPocket",
       PocketMaterial : "pocketMaterial",
       ContentType : "ContentType",
       CouponType : "couponType",
       AssetType : "AssetType",
       TileSize : "tileSize",
       UnitSize : "unitSize",
       Variation : "variations",
       Styles : "Styles",
       Materials : "materials",
       SvgFile : "svgFile",
       VariationTags : "vt",
       MeshReduceTag : "meshReduce",
       ProductLabel : "ProductLabel",
       Dimension : "Dimension",
       DefaultHeight : "defaultHeight",
       DwgFile : "dwgFile",
       MeshReduce : "meshReduce"
    };

    var AttributeIds = {
      Label : "attr_label",
      Variations: "variations",
      Dimension : "attr_dimension",
      Profile : "attr_profile",
      DefaultHeight : "attr_defaultHeight",
      DwgFile : "attr_dwgFile",
      WallHole : "attr_wallHole",
      ProfileHigh : "attr_profileHigh",
      ProfileSize : "attr_profileSize",
      CurtainWidgets : "attr_modelComponents"
    };

    module.AppModule = AppModule;
    module.Method = Method;
    module.CategoryAttr = CategoryAttributes;
    module.AttributeNames = AttributeNames;
    module.AttributeIds = AttributeIds;
})(window);
