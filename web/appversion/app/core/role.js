(function(module) {
    function User(email, userId, sessionId, role, tenants) {
        this.email = email;
        this.userId = userId;
        this.sessionId = sessionId;
        this.role = role;
        this.tenants = tenants;
    };

    User.prototype.checkPermission = function(tenantId, moduleId, action) {
        return this.role.checkPermission(this, tenantId, moduleId, action)
    };

    var RoleEnum = {
        ADMIN: "admin",
        TENANT_ADMIN: "tenant_admin",
        VENDOR: "vendor",
        VENDOR_EDIT: "vendor_edit",
        MANUFACTURER: "manufacturer"
    };

    function Role() {
        this.name = "";
        this.permittedResources = {};
    };
    Role.prototype.checkPermission = function (user, tenantId, moduleId, action) {
        //Logger.debug("Role.checkPermission: user is " + user.email + ", tenant is " + tenantId + ", module is " + moduleId);

        // The common check
        var ret = false;
        if (user.tenants.contains(tenantId)) {
            if (this.permittedResources[moduleId]) {
                if (action) {
                    var actions = this.permittedResources[moduleId];
                    if (actions.contains(Method.ALL)) {
                        //Logger.debug("Role.checkPermission: pass " + moduleId);
                        ret = true;
                    } else {
                        if (actions.contains(action)) {
                            //Logger.debug("Role.checkPermission: pass " + moduleId+ ", action = " + action);
                            ret = true;
                        } else {
                            //Logger.debug("Role.checkPermission: failed " + moduleId+ ", action = " + action);
                        }
                    }
                } else {
                    ret = true;
                    //Logger.debug("Role.checkPermission(no action): pass " + moduleId);
                }
            } else {
                //Logger.debug("Role.checkPermission: failed " + moduleId);
            }
        } else {
            //Logger.debug("Role.checkPermission: failed. No tenant: " + tenantId + " assigned yet");
        }
        return ret;
    };


    function AdminRole() {
        this.name = RoleEnum.ADMIN;
        this.permittedResources = {};
    };
    Utility.inherits(AdminRole, Role);

    AdminRole.prototype.checkPermission = function (user, tenantId, moduleId, action) {
        if (user.tenants.contains(tenantId)) {
            //Logger.debug("AdminRole.checkPermission: admin user." + moduleId + " pass");
            return true;
        } else {
            //Logger.debug("Role.checkPermission: failed. No tenant: " + tenantId + " assigned yet");
        }
    };

    function VendorRole() {
        this.name = RoleEnum.VENDOR;

        var p = {};

        // Tag group
        p[AppModule.Families] = [Method.ALL];

        // Product group
        p[AppModule.Products] = [Method.ALL];
        p[AppModule.UploadProduct] = [Method.ALL];
        p[AppModule.Variation] = [Method.ALL];
        p[AppModule.Search] = [Method.ALL];
        p[AppModule.SearchFamily] = [Method.ALL];
        p[AppModule.EditFamily] = [Method.ALL];
        p[AppModule.ProductsCategorization] = [Method.ALL];
        p[AppModule.MenuProductAddEdit] = [Method.ALL];

        // used by 'Edit Profile'
        p[AppModule.EditProfile] = [Method.ALL];
        // get tenant branches
        p[AppModule.Tenants] = [Method.ALL];

        this.permittedResources = p;
    };
    Utility.inherits(VendorRole, Role);


    function ManufacturerRole() {
        this.name = RoleEnum.MANUFACTURER;

        var p = {};

        // Product group
        p[AppModule.Products] = [Method.ALL];
        p[AppModule.UploadProduct] = [Method.ALL];
        p[AppModule.Search] = [Method.ALL];
        p[AppModule.MenuProductAddEdit] = [Method.ALL];

        // used by 'Edit Profile'
        p[AppModule.EditProfile] = [Method.ALL];
        // get tenant branches
        p[AppModule.Tenants] = [Method.ALL];

        p[AppModule.ModelingGuideline] = [Method.ALL];
        p[AppModule.ChineseGuideline] = [Method.ALL];
        
        // allow topview/max2obj
        p[AppModule.TopView] = [Method.ALL];

        this.permittedResources = p;
    }
    Utility.inherits(ManufacturerRole, Role);


    function VendorEditRole() {
        this.name = RoleEnum.VENDOR_EDIT;

        var p = {};

        // Tag group
        p[AppModule.Families] = [Method.ALL];

        // Product group
        p[AppModule.Products] = [Method.ALL];
        p[AppModule.UploadProduct] = [Method.ALL];
        p[AppModule.Search] = [Method.ALL];
        p[AppModule.SearchFamily] = [Method.ALL];
        p[AppModule.EditFamily] = [Method.ALL];
        p[AppModule.ProductsCategorization] = [Method.ALL];
        p[AppModule.MenuProductAddEdit] = [Method.ALL];

        // used by 'Edit Profile'
        p[AppModule.EditProfile] = [Method.ALL];
        // get tenant branches
        p[AppModule.Tenants] = [Method.ALL];

        this.permittedResources = p;
    };
    Utility.inherits(VendorEditRole, Role);


    function TenantAdminRole() {
        this.name = RoleEnum.TENANT_ADMIN;

        var p = {};

        // Tag group
        p[AppModule.Categories] = [Method.ALL];
        p[AppModule.Brands] = [Method.ALL];
        p[AppModule.Retailers] = [Method.ALL];
        p[AppModule.Families] = [Method.ALL];

        // Product group
        p[AppModule.Products] = [Method.ALL];
        p[AppModule.UploadProduct] = [Method.ALL];
        p[AppModule.Search] = [Method.ALL];
        p[AppModule.SearchFamily] = [Method.ALL];
        p[AppModule.EditFamily] = [Method.ALL];
        p[AppModule.ProductsCategorization] = [Method.ALL];
        p[AppModule.MenuProductAddEdit] = [Method.ALL];

        p[AppModule.EditTenant] = [Method.ALL];

        // used by 'Edit Profile'
        p[AppModule.EditProfile] = [Method.ALL];
        // get tenant branches
        p[AppModule.Tenants] = [Method.ALL];

        this.permittedResources = p;
    }
    Utility.inherits(TenantAdminRole, Role);


    function getRole(role) {
        switch (role) {
            case RoleEnum.ADMIN:
                return new AdminRole();
            case RoleEnum.TENANT_ADMIN:
                return new TenantAdminRole();
            case RoleEnum.VENDOR_EDIT:
                return new VendorEditRole();
            case RoleEnum.MANUFACTURER:
                return new ManufacturerRole();
            default:
                return new VendorRole();
        }
    };

    function getRoleList() {
        return [
            RoleEnum.ADMIN,
            RoleEnum.TENANT_ADMIN,
            RoleEnum.VENDOR,
            RoleEnum.VENDOR_EDIT,
            RoleEnum.MANUFACTURER
        ];
    };

    module.User = User;
    module.RoleEnum = RoleEnum;
    module.getRole = getRole;
    module.getRoleList = getRoleList;
})(window);
