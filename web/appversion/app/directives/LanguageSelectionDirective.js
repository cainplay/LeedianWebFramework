robinApp.directive('ngTranslateLanguageSelect', [
    "LocaleService",
    function (LocaleService) { 'use strict';

    return {
        restrict: 'A',
        replace: true,
        template: ''+
        '<li lass="dropdown" ng-if="visible">'+
            '<a href="" class="dropdown-toggle" data-toggle="dropdown">{{currentLocaleDisplayName}}<span class="caret"></span></a>'+
            '<ul class="dropdown-menu" role="menu" style="overflow-y:scroll">'+
            '<li ng-repeat="localeDisplayName in localesDisplayNames"><a href="" ng-click="changeLanguage(localeDisplayName)">{{localeDisplayName}}</a></li>'+
            '</ul>'+
        '</li>'+
        '',

        controller: ["$scope", function ($scope) {

            $scope.currentLocaleDisplayName = LocaleService.getLocaleDisplayName();
            $scope.localesDisplayNames = LocaleService.getLocalesDisplayNames();
            $scope.visible = $scope.localesDisplayNames &&
            $scope.localesDisplayNames.length > 1;

            $scope.changeLanguage = function (locale) {
                $scope.currentLocaleDisplayName = locale;
                LocaleService.setLocaleByDisplayName(locale);
            };
        }]
    };
}]);
