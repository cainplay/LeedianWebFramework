/*
    Register work which should be performed when the injector is done loading all modules.

    The AngularJs Main function, it provide the same function as $.ready(function() {
        //
    }) in jQuery

*/
robinApp.run(
    ["$rootScope", "$location", "$http", 'StorageService', 'AuthService', 'AUTH_EVENTS', 'UserService', 'ViewManifest', '$route', '$q',function (
    $rootScope, $location, $http, StorageService, AuthService, AUTH_EVENTS, UserService, ViewManifest, $route, $q) {

    var urlLogin = "/login";
    var urlSignup = "/signup";
    var urlDefault = "/";
    var urlRedirectToAfterLogin = urlDefault;
    $rootScope.redirectTo = function(url) {
        if($location.path().toLowerCase() == url) return;
        $location.path(url);
    };
    $rootScope.redirectToDefault = function() {
        if($location.path().toLowerCase() == urlDefault) return;
        $location.path(urlDefault);
    };
    $rootScope.redirectToLogin = function(force) {
        if (force) {
            $location.path(urlLogin);
            return;
        }

        if($location.path().toLowerCase() == urlLogin) return;
        if($location.path().toLowerCase() == urlSignup) return;
        urlRedirectToAfterLogin = $location.path();
        $location.path(urlLogin);
    };
    $rootScope.redirectToAfterLogin = function() {
        if($location.path().toLowerCase() == urlRedirectToAfterLogin) return;
        $location.path(urlRedirectToAfterLogin);
    };

    $rootScope.checkPermission = function() {
        var current = $route.current;
        var tenantId = current.params["tenantId"] || $rootScope.tenantId;
        var data = current.$$route.data;
        return $q(function(resolve, reject) {
            AuthService.authenticate().then(function() {
                // Redirect to default page if user has no permission
                if (data) {
                    var permission = data["permission"];
                    if (permission) {
                        var user = UserService.get();
                        if (!user.checkPermission(tenantId, permission)) {
                            reject(AUTH_EVENTS.notAuthorized);
                            return;
                        }
                    }
                }

                $rootScope.tenantId = tenantId;

                resolve();

            }, function () {
                reject(AUTH_EVENTS.notAuthenticated);
            });
        });
    }

    $rootScope.initApp = function() {
        var promises = [
            $rootScope.checkPermission()
        ];
        return $q.all(promises);
    };

    // Handle the route authentication and authorization check error
    $rootScope.$on("$routeChangeError", function(evt, current, previous, rejection) {
        if(rejection == AUTH_EVENTS.notAuthenticated) {
            $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
        } else if (rejection == AUTH_EVENTS.notAuthorized) {
            $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
        }
    });

    var handleAuthEvent = function (evt, args) {
        // TODO: show these message in a different place
        // A walk around to avoid alert message override by other message
        // when handling same event, since it has the high priority.
        var delayAlert = function (msg) {
            setTimeout(function () {
                robinDialog.alert(msg);
            }, 1)
        };
        if (evt.name == AUTH_EVENTS.notAuthenticated) {
            var msg = "Check authentication: failed."
            if (args && args.message) {
                msg = args.message;
            }

            if (!args || args.redirect) {
                msg += ". Redirect to login page."
                //robinDialog.alert(msg);
                console.debug(msg);
                $rootScope.redirectToLogin();
            } else {
                delayAlert(msg);
            }
        } else if (evt.name == AUTH_EVENTS.sessionTimeout) {
            //console.debug("Session timeout. Redirect to login page.");
            robinDialog.alert("Session timeout. Redirect to login page.");
            $rootScope.redirectToLogin();
        } else if (evt.name == AUTH_EVENTS.notAuthorized) {
            var msg = "You have no permission to access: " + $location.path();
            if (args && args.message) {
                msg = args.message;
            }

            if (!args || args.redirect) {
                msg += ". Redirect to default page."
                robinDialog.alert(msg);
                $rootScope.redirectToDefault();
            } else {
                delayAlert(msg);
            }
        } else if (evt.name == AUTH_EVENTS.loginSuccess) {
            $rootScope.redirectToAfterLogin();
        } else if (evt.name == AUTH_EVENTS.logoutSuccess) {
            $rootScope.redirectToLogin();
        }
    };
    // Register the global authentication event handler
    for (var key in AUTH_EVENTS) {
        if (AUTH_EVENTS.hasOwnProperty(key)) {
            $rootScope.$on(AUTH_EVENTS[key], function (evt, args) {
                handleAuthEvent(evt, args);
            })
        }
    }


    //// The global variables. Store in session storage for refresh.

    $rootScope.tenantId = StorageService.get("tenantId") || '';

    $rootScope.setTenant = function(tenantId) {
        var oldId = $rootScope.tenantId;
        if (oldId === tenantId) return;

        $rootScope.tenantId = tenantId;
        StorageService.set("tenantId", tenantId);

        // Redirect to the url with new tenantId
        var url = $location.path().replace(oldId, tenantId);
        $rootScope.redirectTo(url);
    }

    $rootScope.AppModule = window.AppModule;
    $rootScope.RoleEnum = window.RoleEnum;

    ////
}]);
