/*
    This is a placeholder entry file to load modules.

    We will use some 3rd module dependency tool to manage our modules. There are 3 options:
    1) RequireJs
    2) The common js
    3) Webpack

*/

angular.module('filters', []);

//Define an angular module for our app
var robinApp = angular.module('robinApp', [
    "ngRoute",
    "ngCookies",
    "pascalprecht.translate",
    "tmh.dynamicLocale",
    "xeditable",
    "services",
    "filters",
    "ui.bootstrap"
    ]);
