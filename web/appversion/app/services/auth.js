'use strict';

angular.module('services').
    constant('AUTH_EVENTS', {
        loginSuccess: 'auth-login-success',
        loginFailed: 'auth-login-failed',
        logoutSuccess: 'auth-logout-success',
        sessionTimeout: 'auth-session-timeout',
        authenticated: 'auth-authenticated',
        notAuthenticated: 'auth-not-authenticated',
        notAuthorized: 'auth-not-authorized'
    });

angular.module('services').
    service('UserService',
        [ "StorageService",
        function (StorageService) {
            var key = "currentUser";

            var currentUser = undefined;

            var createUser = function (data) {
                var role = getRole(data.role);
                return new User(data.email, data.userId, data.sessionId, role, data.tenants);
            };

            this.set = function (data) {
                currentUser = undefined;
                if (data) {
                    StorageService.set(key, data, -1);
                } else {
                    StorageService.remove(key);
                }
            };

            this.get = function () {
                if (currentUser) return currentUser;

                var data = StorageService.get(key);
                if (data) {
                    currentUser = createUser(data);
                }

                return currentUser;
            };

            return this;
        }
    ]);

angular.module('services').
    factory('AuthService',
        ["AUTH_EVENTS", "$rootScope", "$http", "$location", "UserService", "StorageService", "$q",
        function (AUTH_EVENTS, $rootScope, $http, $location, UserService, StorageService, $q) {
        var authService = {};

        var saveToken = function (token, permanent) {
            StorageService.set("access_token", token);
        };

        var destoryToken = function () {
            StorageService.remove("access_token")
        };

        var saveUser = function (data) {
            UserService.set(data);
        };

        var destoryUser = function () {
            UserService.set(null);
        };

        authService.authenticate = function () {
            var token = this.getToken();
            return $q(function(resolve, reject) {
                var _resolve = function () {
                    $rootScope.$broadcast(AUTH_EVENTS.authenticated);
                    resolve();
                };

                var _reject = function () {
                    $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
                    reject();
                };

                if (!token) {
                    _reject();
                } else if (authService.isAuthenticated()) {
                    _resolve();
                } else {
                    return authService.getUserData().then(function (res) {
                        if (res.data) {
                            saveUser(res.data);
                            _resolve();
                        } else {
                            _reject();
                        }
                    }, function (res) {
                        _reject();
                    });
                }
            });
        };

        authService.isAuthenticated = function () {
            return !!UserService.get();
        };

        authService.login = function (credentials) {
            return $http
            .post('/mw/login', credentials)
            .then(function (res) {
                var data = res.data;
                if (data.er == -1) {
                    // Clean up below codes once API change is available on production
                    var token;
                    if (data["accessToken"]) {
                        var tokenData = data["accessToken"]
                        token = tokenData.accessToken;
                    } else if (res.headers("Authorization")) {
                        var items = res.headers("Authorization").split(" ");
                        if (items.length > 1 && items[0] == "Bearer") {
                            var token = items[1];
                            saveToken(token, credentials["rememberMe"])
                        }
                    }

                    if (token) {
                        saveToken(token, credentials["rememberMe"])
                    }

                    var userData = data["user"] || data;
                    saveUser(userData);
                    var tenants = userData.tenants;
                    if (tenants && tenants.length > 0) {
                        $rootScope.setTenant(tenants[0]);
                    }

                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                } else {
                    $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
                }
                return res;
            }, function (res) {
                $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
            });
        };

        authService.logout = function () {
            var deferred = $q.defer();
            var handleSuccess = function (res) {
                destoryToken();
                destoryUser();

                $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);

                deferred.resolve();
            };
            var handleError = function (res) {
                deferred.reject();
            };

            $http.get('/mw/logout').then(function (res) {
                handleSuccess(res);
            }, function (res) {
                // TODO: Once API change the response from current behavior:
                // redirect to /robin/login after log out, we need change below codes.
                if (res.status == -1) {
                    handleSuccess(res);
                } else {
                    handleError(res);
                }
            });
            return deferred.promise;
        };

        authService.signup = function (credentials) {
            return $http.post('/mw/signup', credentials);
        };

        authService.getToken = function () {
            return StorageService.get("access_token");
        };

        authService.getUserData = function () {
            return $http.get('/mw/user');
        };

        return authService;
    }]);
