'use strict';

angular.module('services')
    .service('ProductService', [
        'ProductIO',
        function ProductService(ProductIO) {
            var prodService = this;

            var Product = function(languageCode, branch){
                this.languageCode = languageCode;
                this.branch = branch;
            };

            // expose it
            prodService.Product = Product;

            // Create New Product
            Product.prototype.create = function(formData, attributeDB) {
                var postData = formData2PostData(formData, attributeDB);
                return ProductIO.createProduct(postData, this.languageCode, this.branch);
            };

            // Patch/Update product
            Product.prototype.patch = function(formData, attributeDB) {
                var postData = formData2PostData(formData, attributeDB);
                return ProductIO.updateProduct(postData, this.languageCode, this.branch);
            };

            // Get Product by ID
            Product.prototype.getById = function(id) {
                return ProductIO.getProduct(id, this.languageCode, this.branch).then(function(result) {
                    return responseData2FormData(result.data);
                })
            };

            var buildAttributesDB = function(attributes) {
                var attributesDB = {};
                attributes.forEach(function(item) {
                    if (!item.name) return;
                    if (attributesDB[item.name]) {
                        console.warn("Found duplicated attribute name.");
                    }
                    if (item.id === AttributeIds.Variations) {
                        attributesDB.variations = item;
                    } else {
                        attributesDB[item.name] = item;
                    }
                });
                return attributesDB;  
            };

            var responseData2FormData = function(data) {
                var formData = {
                    id: data.id,
                    name: data.name,
                    label: data.label,
                    path: data.path,
                    status: data.status,
                    externalId: data.externalId,
                    version: data.version,
                    brandId: data.brandId,
                    retailer: data.retailer,
                    groups: data.groups,
                    files: data.files,
                    requestTime: data.requestTime,
                    postProcessingStatus: data.postProcessingStatus,
                    postProcessingLog: data.postProcessingLog,
                    lastTimeProcessed: data.lastTimeProcessed,
                    description: data.description,
                    retailerProductUrl: data.retailerProductUrl,
                    retailerPrice: data.retailerPrice,
                    categoryIds: data.categoryIds,
                    familyIds: $.isEmptyObject(data.familyIds)?null:data.familyIds,
                    rawAttributes: data.attributes
                };

                // Parse attributes.
                var attributesDB = buildAttributesDB(data.attributes);

                var defulatValue = function(attrName) {
                    var value = undefined;
                    var attribute = attributesDB[attrName];
                    if (!attribute) return value;
                    if (attribute.free && attribute.free.length > 0) {
                        value = attribute.free[0];
                    } else if (attribute.valuesIds && attribute.valuesIds.length > 0) {
                        value = attribute.valuesIds[0];
                    }
                    return value;
                };

                var attributes = {
                    variations : attributesDB[AttributeNames.Variation],
                    dimension : defulatValue(AttributeNames.Dimension),
                    style : defulatValue(AttributeNames.Styles),
                    material : defulatValue(AttributeNames.Materials),
                    isScalable : defulatValue(AttributeNames.IsScalable),
                    hasPocket : defulatValue(AttributeNames.HasPocket),
                    pocketMaterial : defulatValue(AttributeNames.PocketMaterial),
                    zindex : defulatValue(AttributeNames.ZIndex),
                    contentType : defulatValue(AttributeNames.ContentType),
                    couponType : defulatValue(AttributeNames.CouponType),
                    defaultHeight : defulatValue(AttributeNames.DefaultHeight),
                    dwgFile : defulatValue(AttributeNames.DwgFile),
                    productType : defulatValue(AttributeNames.AssetType),
                    meshReduce : defulatValue(AttributeNames.MeshReduce)
                };

                var couponTypeAttr = attributesDB[AttributeNames.CouponType];
                if (couponTypeAttr && couponTypeAttr.valuesIds && couponTypeAttr.valuesIds.length > 0) {
                    attributes.couponTypeValues = couponTypeAttr.valuesIds;
                }

                var tileSizeAttr = attributesDB[AttributeNames.TileSize];
                if (tileSizeAttr && tileSizeAttr.free && tileSizeAttr.free.length === 2) {
                    attributes.tileSize = {x: tileSizeAttr.free[0], y: tileSizeAttr.free[1]};
                }

                var unitSizeAttr = attributesDB[AttributeNames.UnitSize];
                if (unitSizeAttr && unitSizeAttr.free && unitSizeAttr.free.length === 2) {
                    attributes.unitSize = {width: unitSizeAttr.free[0], height: unitSizeAttr.free[1]};
                }
                formData.attributes = attributes;
                return formData;
            };


            var formData2PostData = function(formData, attributes) {
               
                var attributeDB = buildAttributesDB(attributes);
                var removeAttributes = [];
                var attributesData = [];

                // Build product attribute. Please NOTE: if you want to remove an attribute from product, make sure:
                // 1. The attribute ID is in removeAttributes.
                // 2. The attribute data is NOT in attributesData.
                //
                // Adding label attribute.
                if (formData.label) {
                    var labelAttrData = attributeDB[AttributeNames.ProductLabel];
                    attributesData.push({
                        id: labelAttrData.id,
                        valuesIds: [],
                        free: [formData.label]
                    }); 
                }

                // Adding product type attribute.
                if (formData.productType) {
                    var productAttrData = attributeDB[AttributeNames.AssetType];
                    attributesData.push({
                        id: productAttrData.id,
                        valuesIds: [formData.productType.id]
                    });
                }

                // Adding style attribute.
                var styleAttrData = attributeDB[AttributeNames.Styles];
                if (formData.style) {
                    attributesData.push({
                        id: styleAttrData.id,
                        valuesIds: [formData.style.id]
                    });
                } else {
                    removeAttributes.push(styleAttrData.id);
                }

                // Adding material attribute.
                if (formData.material) {
                    var materialAttrData = attributeDB[AttributeNames.Materials];
                    attributesData.push({
                        id: materialAttrData.id,
                        valuesIds: [formData.material.id]
                    });
                }

                // Adding svgFile attribute.
                if (formData.svgFile) {
                    var svgFileAttrData = attributeDB[AttributeNames.SvgFile];
                    attributesData.push({
                        id: svgFileAttrData.id,
                        valuesIds: [formData.svgFile.id]
                    });
                }

                // Adding dimension attribute.
                if (formData.dimension) {
                    var dimensionData = attributeDB[AttributeNames.Dimension];
                    attributesData.push({
                        id: dimensionData.id,
                        valuesIds: [],
                        free: [formData.dimension]
                    });
                }

                // Adding default height attribute.
                if (formData.defaultHeight) {
                    var defaultHeightData = attributeDB[AttributeNames.DefaultHeight];
                    attributesData.push({
                        id: defaultHeightData.id,
                        valuesIds: [],
                        free: [formData.defaultHeight]
                    }); 
                }

                // Adding dwgFile attribute.
                if (formData.dwgFile) {
                    var dwgFileData = attributeDB[AttributeNames.DwgFile];
                    attributesData.push({
                        id: dwgFileData.id,
                        valuesIds: [],
                        free: [formData.dwgFile]
                    }); 
                }

                // Adding svgFile attribute.
                if (formData.svgFile) {
                    var svgFileData = attributeDB[AttributeNames.SvgFile];
                    attributesData.push({
                        id: svgFileData.id,
                        valuesIds: [formData.svgFile.id]
                    }); 
                }

                // Adding unitSize attribute.
                if (formData.isContentTypeWallpaper) {
                    var unitSizeData = attributeDB[AttributeNames.UnitSize];
                    var w = formData.unitDimensionWidth.toString();
                    var h = formData.unitDimensionHeight.toString();
                    attributesData.push({
                        id: unitSizeData.id,
                        valuesIds: [],
                        free: [w, h]
                    });
                }
  
                 // Adding contentType attribute.
                if (formData.contentType) {
                    var contentTypeData = attributeDB[AttributeNames.ContentType];
                    attributesData.push({
                        id: contentTypeData.id,
                        valuesIds: [formData.contentType.id]
                    });   
                }

                // Adding product type specific attributes.
                var pType = formData.productType.name;
                if (pType === "3D" || pType === "Assembly" ) {
                    // Adding zindex attribute.
                    if (formData.zindex) {
                        var zindexData = attributeDB[AttributeNames.ZIndex];
                        attributesData.push({
                            id: zindexData.id,
                            valuesIds: [formData.zindex.id]
                        });
                    }

                    // Adding isScalable attribute.
                    if (formData.isScalable) {
                        var isScalableData = attributeDB[AttributeNames.IsScalable];
                        attributesData.push({
                            id: isScalableData.id,
                            valuesIds: [formData.isScalable.id]
                        });  
                    }

                    // Adding isScalable attribute.
                    if (formData.hasPocket) {
                        var hasPocketData = attributeDB[AttributeNames.HasPocket];
                        attributesData.push({
                            id: hasPocketData.id,
                            valuesIds: [formData.hasPocket.id]
                        });
                    }

                    // Adding pocket material attribute.
                    if (formData.pocketMaterial) {
                        var pocketMaterialData = attributeDB[AttributeNames.PocketMaterial];
                        attributesData.push({
                            id: pocketMaterialData.id,
                            valuesIds: [],
                            free: [formData.pocketMaterial]
                        });
                    }
                }

                // Adding tileSize attribute.
                if (formData.isStyledProduct) {
                    var x = formData.xDimension.toString();
                    var y = formData.yDimension.toString();
                    var tileSizeData = attributeDB[AttributeNames.TileSize];
                    attributesData.push({
                        id: tileSizeData.id,
                        valuesIds: [],
                        free: [x, y]
                    });
                }

                // Adding meshReduce attribute.
                if (formData.meshReduce) {
                    var meshReduceData = attributeDB[AttributeNames.MeshReduce];
                    attributesData.push({
                        id: meshReduceData.id,
                        valuesIds: [formData.meshReduce.id]
                    }); 
                }

                // Adding couponType attribute.
                var couponTypeData = attributeDB[AttributeNames.CouponType];
                if (formData.couponType && formData.couponType.length > 0) {
                    attributesData.push({
                        id: couponTypeData.id,
                        valuesIds: formData.couponType
                    }); 
                } else {
                    removeAttributes.push(couponTypeData.id);
                }
                
                if (formData.sizeTypeAttributes && formData.sizeTypeAttributes.length>0) { 	
                    $.each(formData.sizeTypeAttributes, function(index, attr) {
                        attributesData.push({
                            id: attr.id,
                            valuesIds: [attr.valueId]
                        }); 
                    });
                }

                var newDescription = formData.description != null && formData.description != "" ?
                    formData.description : " ";

                var retailer = null;
                if (formData.retailer != null && formData.retailer.id != "") {
                    var price = formData.retailerPrice ? parseFloat(formData.retailerPrice) : undefined;
                    retailer = {
                        url: formData.retailerProductUrl,
                        name: formData.retailer.name,
                        price: price,
                        id: formData.retailer.id
                    }
                }

                var postData = {
                    name: formData.name,
                    label: formData.label,
                    defaultName: formData.name,
                    description: newDescription,
                    defaultDescription: newDescription,
                    status: formData.status.id ,
                    path: formData.path,
                    externalId: formData.externalId,
                    brandId: formData.brand != null ? formData.brand.id : null,
                    retailer: retailer,
                    familyIds: formData.familyIds,
                    attributes: attributesData,
                    removeAttributes: removeAttributes,
                    categoryIds: formData.categoryIds,
                    //newFlag: true,
                    files: formData.files,
                    newKey: formData.newKey1,
                    spec: formData.spec,
                    forceTopViewGenerate: formData.forceTopViewGenerate,
                    contentTypeName: formData.contentType.name,
                    productTypeName: formData.productType.name
                };

                if (formData.id1) {
                    postData.id = formData.id1;
                }
                return postData;
            };
        }
    ]);
