'use strict';

angular.module('services')
    .service('PageData', [
        '$http',
        '$rootScope',
        '$location',
        '$window',
        '$filter',
        'ProductIO',
        'LocaleService',
        function Product($http, $rootScope, $location, $window, $filter, ProductIO, LocaleService) {

            // init page data
            this.pageData = {
                statuses: [],
                brands: [],
                retailers: [],
                isScalableValues: null,
                hasPocketValues: null,
                contentTypeValues: null,
                couponTypeValues: null,
                zIndexValues: null,
                fileTypes: [
                    {metaData: '', name: ''},
                    {metaData: 'iso', name: 'ISO File'},
                    {metaData: 'obj', name: 'Model Obj'},
                    {metaData: 'txtr', name: 'Model Texture'},
                    {metaData: 'normal', name: 'Material Normal'},
                    {metaData: 'wallfloor', name: 'Wallpaper/Floor Jpg'},
                    {metaData: '3ds', name: '3DStudioMax File'},
                    {metaData: 'topView', name: 'TopView File'},
                    {metaData: 'scene', name: 'Scene Graph'},
                    {metaData: 'thumbnail', name: 'Thumbnail Image' }
                ]
            };

            this.findAttribute = function(attName, attId) {
                var attributes = this.pageData.attributeDB;
                if (!attributes) return undefined;

                return attributes.find(function(item) {
                    if (attId) {
                        return attId === item.id;
                    } else if (attName) {
                        return attName === item.name;
                    }
                });
            };

            this._buildContentTypeTree = function() {
                this.ctypeTreeData = [];
                var contentTypeDataMap = {};
                var id = 0;
                $.each(this.pageData.contentTypeValues, function(index, contentTypeValue) {
                    var name = $filter('translate')(contentTypeValue.name);
                    var ctList = name.split("/");
                    var added = contentTypeDataMap[ctList[0]] !== undefined;
                    for (var childIndex = 0; childIndex < ctList.length; ++childIndex){
                        var data = {};
                        var childStr = ctList.slice(0, childIndex + 1).join("/");
                        var parentStr = ctList.slice(0, childIndex).join("/");
                        if (contentTypeDataMap[childStr] === undefined) {
                            data.title = ctList[childIndex];
                            data.id = ++id;
                            data.contentTypeValueId = contentTypeValue.id;
                            data.parentId = null;
                            data.fullName = childStr;
                            data.children = [];
                            if (contentTypeDataMap[parentStr]) {
                                data.parentId = contentTypeDataMap[parentStr].id;
                                contentTypeDataMap[parentStr].children.push(data);
                            };
                            contentTypeDataMap[childStr] = data;
                        }
                    }
                    if (!added) {
                        this.ctypeTreeData.push(contentTypeDataMap[ctList[0]]);
                    }
                }.bind(this));
            };


            this._buildSvgFileValues = function() {
                this.pageData.svgFileValues = [];
                var svgFileAttr = this.findAttribute(AttributeNames.SvgFile);
                if (svgFileAttr) {
                    $.each(svgFileAttr.valuesIds, function(index, svgFileValue) {
                        var svgUrl = svgFileValue.name;
                        var svgUrlList = svgUrl.split("/");
                        var svgName = svgUrlList[svgUrlList.length - 1];
                        svgFileValue.url = svgUrl;
                        svgFileValue.name = svgName;
                        this.pageData.svgFileValues.push(svgFileValue);
                    }.bind(this));
                }
            };

            this._buildProductTypeValues = function() {
                this.pageData.productTypeValues = [];
                var productTypeAttr = this.findAttribute(AttributeNames.AssetType);
                if (productTypeAttr) {
                    $.each(productTypeAttr.valuesIds, function(index, productTypeValue) {
                        // Fix for bug HROB-8, delete seek for productType for products except seek.
                        // Currently hardcode to remove the productType.
                        var keyword = "seek";
                        if (productTypeValue.name.toLowerCase() !== keyword || tenantId.toLowerCase() === keyword) {
                            this.pageData.productTypeValues.push(productTypeValue);
                        }
                    }.bind(this));
                }
            };

            this._buildCouponTypeValues = function() {
                var couponTypeAttr = this.findAttribute(AttributeNames.CouponType);
                this.pageData.couponTypeValues = [];
                this.pktypeTreeData = [];
                if (couponTypeAttr) {
                    $.each(couponTypeAttr.valuesIds, function(index, couponTypeValue) {
                        this.pageData.couponTypeValues.push(couponTypeValue);
                        var data = {};
                        data.title = couponTypeValue.name;
                        data.id = couponTypeValue.id;
                        data.parentId = null;
                        data.fullName = couponTypeValue.name;
                        data.children = [];
                        this.pktypeTreeData.push(data);
                    }.bind(this));
                }
            }

            this.populateData = function(lang, branch) {
                return ProductIO.getProductUploadData(lang, branch).then(function(data) {
                    // Save attribute.
                    this.pageData.attributeDB = data.attributes;

                    // populate statuses
                    this.pageData.statuses = data.statuses;
                    // populate brands
                    this.pageData.brands = data.brands.slice(0);
                    this.pageData.brands.sort(LocaleService.localeStringSort(lang, function(item) {return item.name;}));
                    // populate retailers
                    this.pageData.retailers = data.retailers.slice(0);
                    this.pageData.retailers.sort(LocaleService.localeStringSort(lang, function(item) {return item.name;}));

                    // populate isScalable values
                    var isScalableAttr = this.findAttribute(AttributeNames.IsScalable);
                    this.pageData.isScalableValues = isScalableAttr ? isScalableAttr.valuesIds : [];

                    // populate hasPocket values
                    var hasPocketAttr = this.findAttribute(AttributeNames.HasPocket);
                    this.pageData.hasPocketValues = hasPocketAttr ? hasPocketAttr.valuesIds : [];

                    // populate contentType values
                    var contentTypeAttr = this.findAttribute(AttributeNames.ContentType);
                    this.pageData.contentTypeValues = contentTypeAttr ? contentTypeAttr.valuesIds : [];

                    // construct content type tree used for robin UI as well.
                    this._buildContentTypeTree();
                    // populate svgFileValues
                    this._buildSvgFileValues();
                    // populate productTypes values
                    this._buildProductTypeValues();
                    // populate couponTypes values
                    this._buildCouponTypeValues();

                    // populate styles values
                    var styleAttr = this.findAttribute(AttributeNames.Styles);
                    this.pageData.styleValues = styleAttr ? styleAttr.valuesIds : [];

                    // populate material values
                    var materialAttr = this.findAttribute(AttributeNames.Materials);
                    this.pageData.materialValues = materialAttr ? materialAttr.valuesIds : [];
                    // add an property to materialValues for sorting
                    this.pageData.materialValues.forEach(function(element, index){
                        var result = /\d+(?=\){2})/.exec(element.name);
                        if(result)
                            this.pageData.materialValues[index].order = parseInt(result[0]);
                        else
                            this.pageData.materialValues[index].order = -1;
                    }.bind(this));

                    // populate z-index values
                    var zIndexAttr = this.findAttribute(AttributeNames.ZIndex);
                    this.pageData.zIndexValues = zIndexAttr ? zIndexAttr.valuesIds : [];

                    //populate categories tree
                    this.treeData = data.categoriesTree;
                    // populate families tree
                    this.familyTreeData = data.familiesTree;

                    //populate mesh reduce values
                    var meshReduceAttr = this.findAttribute(AttributeNames.MeshReduceTag);
                    this.pageData.meshReduceValues = meshReduceAttr ? meshReduceAttr.valuesIds : [];

                    $rootScope.$broadcast("pageDataLoaded");
                }.bind(this), 

                function(err) {
                    if (err.status == 401) {
                        robinDialog.alertWithCallback('You do not have permission to acess this page.', function(){
                            $rootScope.$apply(function() {
                                $location.path("/login");
                            });
                        });
                    } else {
                        var statusText = err.statusText;
                        var msg = err.data || 'Populate data error, please try again.';
                        if (statusText) {
                            msg = (statusText + ': ' + msg);
                        }
                        robinDialog.alertWithCallback(msg, function () {
                            $window.location.reload();
                        });
                    }
                });
            };
        }
    ]);
