'use strict';

angular.module('services')
    .service('AttributeIO', [
        '$http',
        '$rootScope',
        function AttributeIO($http, $rootScope) {

            this.getAttributes = function(attributeType, lang) {
                var url = '/mw/attributes/' + $rootScope.tenantId + '?a=0';
                if (attributeType) url += ('&attributeType=' + attributeType);
                if (lang) url += ('&lang=' + lang);
                return $http({method: 'GET', url: url});
            };

            this.updatesValues = function(values, aId, password, lang) {
                var url = '/mw/attributes/values/' + $rootScope.tenantId + '/' + aId + '?password=' + password;
                if (lang) url += ('&lang=' + lang);
                return $http({method: 'PUT', url: url, data: values});
            };

            this.createAttribute = function(data, aId, password, lang) {
                var url = '/mw/attributes/' + $rootScope.tenantId;
                if (aId) url += ('/' + aId);
                url += ('?password=' + password);
                if (lang) url += ('&lang=' + lang);
                return $http({method: 'POST', url: url, data: data});
            };
        }
    ]);