'use strict';

angular.module('services')
    .service('FamilyIO', [
        '$http',
        '$rootScope',
        '$q',
        'StorageService',
        function FamilyIO($http, $rootScope, $q, StorageService) {
            this.getFamilyTree = function (lang) {
                var familyKey = $rootScope.tenantId;
                if (lang) familyKey += ('_' + lang);
                familyKey += '_family_tree';
                var data = StorageService.get(familyKey);
                if (data) {
                    return $q.when(data);
                } else {
                    var url = '/mw/families/tree/' + $rootScope.tenantId;
                    if (lang) url += ('?lang=' + lang);
                    return $http.get(url).then(function(result) {
                        StorageService.set(familyKey, result.data, -1, true);
                        return result.data;
                    });
                }
            };

            this.addFamily = function (data, lang) {
                var url = '/mw/family/' + $rootScope.tenantId;
                if (lang) url += ('?lang=' + lang);
                return $http({
                    method: 'POST',
                    url: url,
                    headers: {
                      'Content-Type': 'application/json'
                    },
                    data: data
                });
            };

            this.updateFamily = function (data, fId, lang) {
                var url = '/mw/family/' + $rootScope.tenantId + '/' + fId;
                if (lang) url += ('?lang=' + lang);
                return $http({
                    method: 'PUT',
                    url: url,
                    headers: {
                      'Content-Type': 'application/json'
                    },
                    data: data
                });
            };

            this.deleteFamily = function (fId) {
                return $http.delete('/mw/family/' + $rootScope.tenantId + '/' + fId);
            };

            this.updateFamilyTree = function (tree, lang) {
                var url = '/mw/families/tree/' + $rootScope.tenantId;
                if (lang) url += ('?lang=' + lang);
                return $http({method: 'PUT', url: url, data: tree});
            };
        }
    ]);
