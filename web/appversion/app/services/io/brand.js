'use strict';

angular.module('services')
    .service('BrandIO', [
        '$http',
        '$rootScope',
        function BrandIO($http, $rootScope) {

            this.getBrands = function() {
                var url = '/mw/brands/tree/' + $rootScope.tenantId;
                return $http({method: 'GET', url: url});
            };

            this.addBrand = function(data) {
                return $http({
                    method: 'POST',
                    url: '/mw/brand/' + $rootScope.tenantId,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: data
                });
            };

            this.updateBrand = function(data, bId) {
                return $http({
                    method: 'PUT',
                    url: '/mw/brand/' + $rootScope.tenantId + '/' + bId,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: data
                });
            };

            this.deleteBrand = function(bId) {
                return $http({
                    method: 'DELETE',
                    url: '/mw/brand/' + $rootScope.tenantId + '/' + bId
                });
            };

            this.updateBrands = function(data) {
                return $http({
                    method: 'PUT',
                    url: '/mw/brands/tree/' + $rootScope.tenantId,
                    data: data
                })
            };
        }
    ]);