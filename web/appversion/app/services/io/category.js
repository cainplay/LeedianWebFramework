'use strict';

angular.module('services')
    .service('CategoryIO', [
        '$http',
        '$rootScope',
        function CategoryIO($http, $rootScope) {

            this.getCategoryTree = function(lang, branch) {
                var url = '/mw/categories/tree/' + $rootScope.tenantId + '?a=0';
                if (lang) url += ('&lang=' + lang);
                if (branch) url += ('&branch=' + branch);
                return $http({ method: 'GET', url: url});
            };

            this.updateCategoryTree = function(data, lang, branch) {
                var url = '/mw/categories/tree/' + $rootScope.tenantId + '?a=0';
                if (lang) url += ('&lang=' + lang);
                if (branch) url += ('&branch=' + branch);
                return $http({method: 'PUT', url: url, data: data});
            };

            this.createCategory = function(data, lang, branch) {
                var url = '/mw/category/' + $rootScope.tenantId + '?a=0';
                if (lang) url += ('&lang=' + lang);
                if (branch) url += ('&branch=' + branch);
                return $http({
                    method: 'POST',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: data
                });
            };

            this.updateCategory = function(data, cId, lang, branch) {
                var url = '/mw/category/' + $rootScope.tenantId + '/' + cId + '?a=0';
                if (lang) url += ('&lang=' + lang);
                if (branch) url += ('&branch=' + branch);
                return $http({
                    method: 'PUT',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: data
                });
            };

            this.deleteCategory = function(cId) {
                return $http({
                    method: 'DELETE',
                    url: '/mw/category/' + $rootScope.tenantId + '/' + cId
                });
            };
        }
    ]);