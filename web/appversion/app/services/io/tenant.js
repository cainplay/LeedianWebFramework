'use strict';

angular.module('services')
    .service('TenantIO', [
        '$http',
        '$q',
        '$rootScope',
        'StorageService',
        function TenantIO($http, $q, $rootScope, StorageService) {
            var self = this;

            this.getTenantBranch = function() {
                var branchKey = $rootScope.tenantId + "_tenant_branch";
                var data = StorageService.get(branchKey);
                if (data) {
                    return $q.when(data);
                } else {
                    return $http({
                        method: 'GET',
                        url: '/mw/tenant/tree/' + $rootScope.tenantId
                    }).then(function(result) {
                        StorageService.set(branchKey, result.data, -1, true);
                        return result.data;
                    });
                }
            };

            this.getTenants = function(lang) {
                var url = '/mw/tenants';
                if (lang) url += ('?lang=' + lang);
                return $http({
                    method : 'GET',
                    url : url,
                    headers : {
                        'Content-Type' : 'application/json'
                    }
                });
            };
        }
    ]);