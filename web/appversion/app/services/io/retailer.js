'use strict';

angular.module('services')
    .service('RetailerIO', [
        '$http',
        function RetailerIO($http) {

            this.getRetailers = function(lang) {
                var url = '/mw/retailers';
                if (lang) url += ('?lang=' + lang);
                return $http({method: 'GET', url: url});
            };

            this.createRetailer = function(data, lang) {
                var url = '/mw/retailer';
                if (lang) url += ('?lang=' + lang);
                return $http({
                    method: 'POST',
                    url: url,
                    data: data
                });
            };

            this.updateRetailer = function(data, rId, lang) {
                var url = '/mw/retailer' + '/' + rId;
                if (lang) url += ('?lang=' + lang);
                return $http({
                    method: 'PUT',
                    url: url,
                    data: data
                });
            };

            this.deleteRetailer = function(rId, lang) {
                var url = '/mw/retailer' + '/' + rId;
                if (lang) url += ('?lang=' + lang);
                return $http({method: 'DELETE', url: url});
            };
        }
    ]);