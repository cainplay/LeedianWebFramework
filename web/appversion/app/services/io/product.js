'use strict';

angular.module('services')
    .service('ProductIO', [
        '$http',
        '$rootScope',
        '$q',
        'StorageService',
        function ProductIO($http, $rootScope, $q, StorageService) {

            // Request for getting product information.
            this.getProduct = function(productId, lang, branch) {
                var url = '/mw/product/upload/' + $rootScope.tenantId + '/' + productId + '?a=0';
                if (lang) url += ('&lang=' + lang);
                if (branch) url += ('&branch=' + branch);
                return $http({
                    method: 'GET',
                    url: url
                });
            };

            // Request for creating a new product.
            this.createProduct = function(data, lang, branch) {
                var url = '/mw/product/v2.0/upload/' + $rootScope.tenantId + '?a=0';
                if (lang) url += ('&lang=' + lang);
                if (branch) url += ('&branch=' + branch);
                return $http({
                    method: 'POST',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: data
                });
            };

            // Request for updating a product.
            this.updateProduct = function(data, lang, branch) {
                var url = '/mw/product/v2.0/upload/' + $rootScope.tenantId + '?a=0';
                if (lang) url += ('&lang=' + lang);
                if (branch) url += ('&branch=' + branch);
                return $http({
                    method: 'PUT',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: data
                });
            };

            // Request for batch update products.
            this.patchProducts = function(patchData, lang, branch) {
                var url = '/mw/products/upload/' + $rootScope.tenantId + '?a=0';
                if (lang) url += ('&lang=' + lang);
                if (branch) url += ('&branch=' + branch);
                return $http({
                    method: 'PATCH',
                    url: url,
                    data: patchData
                })
            };

            // Request for creating product variation.
            this.createVariation = function(postData, porductId, lang, branch) {
                var url = '/mw/product/upload/variation/' + $rootScope.tenantId + '/' + porductId + '?a=0';
                if (lang) url += ('&lang=' + lang);
                if (branch) url += ('&branch=' + branch);
                return $http({
                    method: 'POST',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: postData
                });
            };

            // Request for update product variation.
            this.updateVariation = function(postData, porductId, variationId, lang, branch) {
                var url = '/mw/product/upload/variation/' + $rootScope.tenantId + '/' + porductId + '/' + variationId + '?a=0';
                if (lang) url += ('&lang=' + lang);
                if (branch) url += ('&branch=' + branch);
                return $http({
                    method: 'PUT',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: postData
                });
            };

            // Request for delete product variation.
            this.deleteVariation = function(porductId, variationId, lang, branch) {
                var url = '/mw/product/upload/variation/' + $rootScope.tenantId + '/' + porductId + '/' + variationId + '?a=0';
                if (lang) url += ('&lang=' + lang);
                if (branch) url += ('&branch=' + branch);
                return $http({
                    method: 'DELETE',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
            };

            // Request for patch products.
            this.patchProducts = function(patchProducts, lang, branch) {
                var url = '/mw/products/upload/' + $rootScope.tenantId + '?a=0';
                if (lang) url += ('&lang=' + lang);
                if (branch) url += ('&branch=' + branch);
                return $http({
                    method: 'PATCH',
                    url: url,
                    data: patchProducts
                });
            };

            // Request for getting product categorization initial data.
            this.getProductCategorizationData = function(branch) {
                var productKey = $rootScope.tenantId;
                if (branch) productKey += ('_' + branch);
                productKey += '_product_categorization_data';
                var data = StorageService.get(productKey);
                if (data) {
                    return $q.when(data);
                } else {
                    var url = '/mw/products/categorization/' + $rootScope.tenantId + '?a=0';
                    if (branch) url += ('&branch=' + branch);
                    return  $http({
                        method: 'GET',
                        url: url
                    }).then(function(result) {
                        StorageService.set(productKey, result.data, -1, true);
                        return result.data;
                    });
                }
            };

            // Request for getting product creation page initial data.
            this.getProductUploadData = function(lang, branch) {
                var productKey = $rootScope.tenantId;
                if (branch) productKey += ('_' + branch);
                if (lang) productKey += ('_' + lang);
                productKey += '_product_upload_data';
                var data = StorageService.get(productKey);
                if (data) {
                    return $q.when(data);
                } else {
                    var url = '/mw/product/upload/data/' + $rootScope.tenantId + '?a=0';
                    if (lang) url += ('&lang=' + lang);
                    if (branch) url += ('&branch=' + branch);
                    return  $http({
                        method: 'GET',
                        url: url
                    }).then(function(result) {
                        StorageService.set(productKey, result.data, -1, true);
                        return result.data;
                    });
                }
            };

            // Request for updating product categories.
            this.updateProductCategories = function(data, branch) {
                var url = '/mw/products/categories/' + $rootScope.tenantId + '?a=0';
                if (branch) url += ('&branch=' + branch);
                return  $http({
                    method: 'PUT',
                    url: url,
                    data: data
                });
            };

            // Request for updating product families.
            this.updateProductFamilies = function(data) {
                return  $http({
                    method: 'PUT',
                    url: '/mw/products/families/' + $rootScope.tenantId,
                    data: data
                });
            };

            // Request for search product.
            this.searchProducts = function(param) {
                 var url = '/mw/products/search/' + $rootScope.tenantId + '?a=0';
                 if (param.offset !== undefined) url += ('&offset=' + param.offset);
                 if (param.limit  !== undefined) url += ('&limit=' + param.limit);
                 if (param.status !== undefined) url += ('&status=' + param.status);
                 if (param.freeText) url += ('&freeText=' + param.freeText);
                 if (param.categoryIds) url += ('&categoryIds=["' + encodeURIComponent(param.categoryIds.join('","')) + '"]');
                 if (param.brandIds) url += ('&brandIds=["' + encodeURIComponent(param.brandIds.join('","')) + '"]');
                 if (param.path) url += ('&path=' + param.path);
                 if (param.guid) url += ('&guid=' + param.guid);
                 if (param.sku) url += ('&sku=' + param.sku);
                 if (param.branch) url += ('&branch=' + param.branch);
                 if (param.attributeIds !== undefined) url += ('&attributeIds=' + param.attributeIds);
                 return $http({method: 'GET',url: url});
            };
        }
    ]);
