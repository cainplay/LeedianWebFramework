//Define Routing for Robin app only.

robinApp.constant("ViewManifest", {
    '/': {
        templateUrl: 'appversion/app/components/home/view.html'
    },
    '/:tenantId/categories': {
        templateUrl: 'appversion/app/components/category/view.html',
        controller: 'CtrlCategories',
        data: {
            permission: AppModule.Categories
        }
    },
    '/:tenantId/upload-product': {
        templateUrl: 'appversion/app/components/upload-product/view.html',
        controller: 'CtrlEditProduct',
        data: {
            permission: AppModule.UploadProduct,
        }
    },
    '/:tenantId/variation': {
        templateUrl: 'appversion/app/components/variation/view.html',
        controller: 'CtrlEditVariation',
        data: {
            permission: AppModule.Variation,
        }
    },
    '/:tenantId/modeling-guideline': {
        templateUrl: 'appversion/app/components/modeling-guideline/view.html',
        data: {}
    },
    '/:tenantId/chinese-guideline': {
        templateUrl: 'appversion/app/components/chinese-guideline/view.html',
        data: {
            permission: AppModule.ChineseGuideline
        }
    },
    '/:tenantId/search': {
        templateUrl: 'appversion/app/components/search/view.html',
        controller: 'CtrlSearch',
        data: {
            permission: AppModule.Search
        }
    },
    '/:tenantId/families': {
        templateUrl: 'appversion/app/components/families/view.html',
        controller: 'CtrlFamilies',
        data: {
            permission: AppModule.Families
        }
    },
    '/:tenantId/searchFamily': {
        templateUrl: 'appversion/app/components/searchFamily/view.html',
        controller: 'CtrlSearchFamily',
        data: {
            permission: AppModule.SearchFamily
        }
    },
    '/:tenantId/editFamily': {
        templateUrl: 'appversion/app/components/editFamily/view.html',
        controller: 'CtrlEditFamily',
        data: {
            permission: AppModule.EditFamily
        }
    },
    '/:tenantId/products-categorization': {
        templateUrl: 'appversion/app/components/products-categorization/view.html',
        controller: 'CtrlProductsCategorization',
        data: {
            permission: AppModule.ProductsCategorization
        }
    },
    '/:tenantId/brands': {
        templateUrl: 'appversion/app/components/brand/view.html',
        controller: 'CtrlBrands',
        data: {
            permission: AppModule.Brands
        }
    },
    '/:tenantId/attributes': {
        templateUrl: 'appversion/app/components/attributes/view.html',
        controller: 'CtrlAttributes',
        data: {
            permission: AppModule.Attributes
        }
    },
    '/:tenantId/retailers': {
        templateUrl: 'appversion/app/components/retailers/view.html',
        controller: 'CtrlRetailers',
        data: {
            permission: AppModule.Retailers
        }
    },
    '/:tenantId/analyticssearch': {
        templateUrl: 'appversion/app/components/analyticssearch/view.html',
        controller: 'CtrlAnalyticsSearch',
        data: {}
    },
    '/:tenantId/tenant-edit': {
        templateUrl: 'appversion/app/components/tenant-edit/view.html',
        controller: 'CtrlTenantEdit',
        data: {
            permission: AppModule.EditTenant
        }
    },
    '/:tenantId/users': {
        templateUrl: 'appversion/app/components/users/view.html',
        controller: 'CtrlUsers',
        data: {
            permission: AppModule.Users
        }
    },
    '/:tenantId/create-tenant': {
        templateUrl: 'appversion/app/components/tenant-create/view.html',
        controller: 'CtrlCreateTenant',
        data: {
            permission: AppModule.CreateTenant
        }
    },
    '/:tenantId/addPackageToTenant': {
        templateUrl: 'appversion/app/components/tenant-add-package/view.html',
        controller: 'CtrlAddPackageToTenant',
        data: {
            permission: AppModule.AddPackageToTenant
        }
    }
});

robinApp.config(['$routeProvider', 'ViewManifest', function($routeProvider, ViewManifest) {

    var addGlobalResolveToRoutes = function(manifest) {
        for(var p in manifest){
            if(manifest.hasOwnProperty(p)){
                manifest[p].resolve = {
                    app: [ '$rootScope', function($rootScope){
                        return $rootScope.initApp();
                    }]
                };
            }
        }
        return manifest;
    };

    var routes = addGlobalResolveToRoutes(ViewManifest);
    for(var p in routes){
        if(routes.hasOwnProperty(p)){
            $routeProvider.when(p, routes[p]);
        }
    }

    $routeProvider.
        when('/signup', {
            templateUrl: 'appversion/app/components/signup/view.html',
            controller: 'CtrlSignup',
        }).
        when('/login', {
            templateUrl: 'appversion/app/components/login/view.html',
            controller: 'CtrlLogin',
        }).
        otherwise({
            redirectTo: '/'
        });
}]);
