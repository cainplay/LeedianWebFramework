angular.module('filters').
filter('StatusFilter', function() {

    var statuses = [
        {
            id: -1,
            description: ""
        },
        {
            id: 0,
            description: "Deleted"
        },
        {
            id: 1,
            description: "Active"
        },
        {
            id: 2,
            description: "Hidden"
        },
        {
            id: 3,
            description: "Private"
        }
    ];

    // In the return function, we must pass in a single parameter which will be the data we will work on.
    // We have the ability to support multiple other parameters that can be passed into the filter optionally
    return function(status) {
        for (var i = 0; i < statuses.length; ++i) {
            var s = statuses[i];
            if (s.id == status) {
                return s.description;
            }
        }
        return status;
    }
});
