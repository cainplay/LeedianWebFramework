robinApp.controller('CtrlEditProduct', [
    '$scope',
    '$rootScope',
    '$http',
    'PageData',
    'ProductService',
    'ProductIO',
    'TenantIO',
    'UserService',
    'StorageService',
    '$location',
    '$route',
    '$window',
    '$q',
    '$filter',
    '$timeout',
    function($scope, $rootScope, $http, PageData, ProductService, ProductIO, TenantIO, UserService, StorageService, $location, $route, $window, $q, $filter, $timeout) {

    // Global parameters.
    $scope.EMode = {
        eCreate : 'create',
        eEdit : 'edit',
        eEditCreate: 'editCreate', // In edit page but product didn't loaded.
        eDuplicate: 'duplicate'
    };
    $scope.mode = $scope.EMode.eCreate;

    $scope.formData = {
        id1: null,
        name: null,
        description: null,
        status: null,
        path: null,
        externalId: null,
        brand: null,
        retailer: null,
        retailerPrice: null,
        retailerProductUrl: null,
        zindex: null,
        isScalable: null,
        hasPocket: null,
        contentType: null,
        categoryIds: null,
        pocketMaterial: null,
        pocketMaterials: [],
        currentPocketMaterial: null,
        files: [],
        newKey1: null,
        productType: null,
        version: null,
        postProcessingStatus: null,
        isStyledProduct: false,
        is3DProduct: false,
        xDimension: 1,
        yDimension : 1,
        style: null,
        material:null,
        creationTime: null,
        modifiedTime: null,
        lastTimeProcessed: null,
        creationTimeOutput: null,
        modifiedTimeOutput: null,
        lastTimeProcessedOutput: null,
        creationTimeOutputFormat: null,
        modifiedTimeOutputFormat: null,
        lastTimeProcessedOutputFormat: null,
        label: null,
        svgFile: null,
        familyIds: null,
        defaultHeight: null,
        dwgFile: null,
        couponType: null,
        isContentTypeWallpaper: false,
        unitDimensionWidth: 0,
        unitDimensionHeight: 0
    };

    $scope.loading = true;
    $scope.curtainText = 'loading';

    // Init from URL parameters.
    var productId = $location.search().guid || '';
    var branchName = $location.search().branch || '';
    $scope.formData.id1 = productId;
    $scope.branch = productId ? branchName : '';

    //variations
    $scope.variations = [];
    $scope.contentTypeInfoMap = null;

    $scope.urlFor3DViewer = robinConsts.URLFor3DViewer;

    // languages
    var languageKey = $rootScope.tenantId + "_upload_product_language";
    $scope.languages = robinConsts.languages;
    $scope.language = $window.sessionStorage[languageKey] ? JSON.parse($window.sessionStorage[languageKey]) : robinConsts.languages[0];

    var Product= new ProductService.Product($scope.language.code, $scope.branch);

    $scope.user = UserService.get();
    $scope.userRole = $scope.user.role.name;
    $scope.userRoles = $rootScope.RoleEnum;
    $scope.isManufacturerRole = null;

    $scope.productTypeDescriptionText = "";
    $scope.statusDescriptionText = "";
    $scope.skuDescriptionText = "";

    $scope.sizeTypeFilters = null;
    $scope.sizeTypeFiltersMap = null;

    var updateProductStatus = null;

    var didntConfirmYet = true;

    $scope.showHasPocket = false;
    $scope.showPocketMaterial = false;

    $scope.$watch("formData.status", function(){$scope.changeStatusDescription();});
    $scope.$watch("formData.productType", function(){$scope.changeProductTypeDescription();});
    $scope.$watch("formData.creationTime", function(){$scope.makeCreationTime();});
    $scope.$watch("formData.modifiedTime", function(){$scope.makeModifiedTime();});
    $scope.$watch("formData.lastTimeProcessed", function(){$scope.makePostProcessingTime();});
    $scope.$watch("formData.brand", function(){$scope.initPocketMaterialList();});
    $scope.$watch("formData.hasPocket", function(){$scope.updatePocketMaterialUI();});
    $scope.$watch("showHasPocket", function(){$scope.updatePocketMaterialUI();});
    $scope.$watch("formData.pocketMaterial", function(){$scope.onPocketMaterialChanged();});
    $scope.$watch("formData.currentPocketMaterial", function(){$scope.onPocketMaterialOptionChanged();});

    function hideMaterialForNonEzhome() {
      if ($scope.tenantId === 'ezhome') {
      } else {
        $("#materialDiv").hide();
      }
    }

    function setFieldsDefaultValues() {

        // is scalable
        $.each($scope.pageData.isScalableValues, function(index, isScalableValue) {
            if (isScalableValue.name == "false")
                $scope.formData.isScalable = isScalableValue;
        });

        // has pocket
        $.each($scope.pageData.hasPocketValues, function(index, hasPocketValue) {
            if (hasPocketValue.name == "false")
                $scope.formData.hasPocket = hasPocketValue;
        });

        // zindex
        $.each($scope.pageData.zIndexValues, function(index, zIndexValue) {
            if (zIndexValue.name == 200)
                $scope.formData.zindex = zIndexValue;
        });

        // product type
        $.each($scope.pageData.productTypeValues, function(index, productTypeValue) {
            if (productTypeValue.name == "3D") {
                $scope.formData.productType = productTypeValue;
                $scope.formData.is3DProduct = true;
            }
        });

        //mesh reduce
        $.each($scope.pageData.meshReduceValues, function(index, meshReduceValue) {
            if (meshReduceValue.name == "true") {
                $scope.formData.meshReduce = meshReduceValue;
            }
        });

        $scope.formData.isStyledProduct = false;

        // status
        $scope.isManufacturerRole = false;

        if($scope.userRole == $scope.userRoles.MANUFACTURER){
            $.each($scope.pageData.statuses, function(index, status) {
                if (status.id == 3) // private default value
                    $scope.formData.status = status; // init default value
            });

            $scope.isManufacturerRole = true;
        }
        else if ($scope.userRole == $scope.userRoles.TENANT_ADMIN) {
            $.each($scope.pageData.statuses, function(index, status) {
                if (status.id == 3) // private default value
                    $scope.formData.status = status; // init default value
            });
        }
        else {
            $.each($scope.pageData.statuses, function(index, status) {
                if (status.id == 1) // active default value
                    $scope.formData.status = status; // init default value
            });
        }

        // for manufacturer role
        if($scope.isManufacturerRole){
            // only ProductTypes of 3D, Wallpaper, FloorTiles are allowed
            var allowedProductTypes = ["3D", "Wallpapers", "FloorTiles"];
            $scope.pageData.productTypeValues = PageData.pageData.productTypeValues.filter(function(item) {
                return allowedProductTypes.indexOf(item.name)!=-1;
            });
        }
        else{
            // for products which product type is Colors will be upload by other means, so hide it
            $scope.pageData.productTypeValues = PageData.pageData.productTypeValues.filter(function(item) {
                return item.name != "Colors";
            });
        }
    }

    /*
     * categories tree
     */

    $scope.cTreeSelector = '.categories-tree';
    //$scope.cTreeObject = null;
    $scope.cTree = null;
      //$scope.cTreeRoot = null;

    $scope.fTreeSelector = '.families-tree';
    $scope.fTree = null;

    $scope.ctypeTreeSelector = '.contenttype-tree';
    $scope.ctypeTree = null;

    $scope.ptypeSelector = '.couponType';
    $scope.pkTypeTree = null;

    // add extra tree attributes to raw data
    $scope.initTreeDataAttrs = function(nodesList) {
        $(nodesList).each(function(index, node) {
            node.count = 0; // how many products are tagged with this category
            node.name = node.title; // save for later use
            if (node.children && node.children.length > 0) {
                // set parent nodes as non-checkbox nodes
                node.isParent = true; // mark for later use
                //node.hideCheckbox = true;
                node.expand = false;
                //node.unselectable = true;
                $scope.initTreeDataAttrs(node.children);
            }
        });
    };

    // init dynatree with data
    $scope.initTree = function(rootSelector, data) {
        $.ui.dynatree.nodedatadefaults["icon"] = false; // turn off icons by default
        $(rootSelector).dynatree({
            imagePath: " ",
            //persist: true,
            checkbox: true,
            selectMode: 3,
            clickFolderMode: 1,
            children: data,
            onSelect: function(flag, node){
            	$scope.showSizeFilterInfo(flag, node);
            }
        });

        // init scope vars
        $scope.cTreeObject = $(rootSelector);
        $scope.cTree = $(rootSelector).dynatree("getTree");
        $scope.cTreeRoot = $(rootSelector).dynatree("getRoot");

    };

    $scope.initFamilyTree = function (rootSelector, data) {
      $.ui.dynatree.nodedatadefaults["icon"] = false; // turn off icons by default
      $(rootSelector).dynatree({
        //persist: true,
        checkbox: true,
        selectMode: 3,
        clickFolderMode: 1,
        children: data
      });

      // init scope vars
      $scope.fTreeObject = $(rootSelector);
      $scope.fTree = $(rootSelector).dynatree("getTree");
      $scope.fTreeRoot = $(rootSelector).dynatree("getRoot");

    };

    $scope.initContentTypeInfoMap = function() {
        return $http({
            method: 'GET',
            url: '/appversion/app/resources/contenttypeinfomap.json'
        }).success(function(data) {
            $scope.contentTypeInfoMap = data;
        }).error(function(data, status, headers, config) {
            robinDialog.alert('Content type information map download failed!'); // TODO: improve this error handling
        });
    };

    $scope.initCouponType = function (rootSelector, data) {
        $.ui.dynatree.nodedatadefaults["icon"] = false; // turn off icons by default
        $(rootSelector).dynatree({
          //persist: true,
          checkbox: true,
          selectMode: 3,
          clickFolderMode: 1,
          children: data
        });

        // init scope vars
        $scope.pkTreeObject = $(rootSelector);
        $scope.pkTypeTree = $(rootSelector).dynatree("getTree");
        $scope.pkTreeRoot = $(rootSelector).dynatree("getRoot");
    };

    $scope.initCtypeTree = function (rootSelector, data) {
      $.ui.dynatree.nodedatadefaults["icon"] = false; // turn off icons by default
      $(rootSelector).dynatree({
        //persist: true,
        checkbox: true,
        classNames: {checkbox: "dynatree-radio"},
        selectMode: 1, // single select
        clickFolderMode: 1,
        children: data,
        onSelect: function(flag, node) {
            if (flag === true) {
                var contentType = null;
                $.each($scope.pageData.contentTypeValues, function(index, contentTypeValue) {
                    if (contentTypeValue.id == node.data.contentTypeValueId) {
                        contentType  = contentTypeValue;
                    }
                });

                if (contentType) {
                    $scope.formData.contentType = contentType;
                    $scope.updateContentTypeInfo();
                } else {
                    if (!$scope.$$phase) $scope.ctypeTree.selectKey(node.data.key, false);
                }
            } else {
                $scope.formData.contentType = null;
                $scope.updateContentTypeInfo();
            }

            if (!$scope.$$phase) {
                $scope.$apply();
            }
        }
      });

      // init scope vars
      $scope.ctypeTreeObject = $(rootSelector);
      $scope.ctypeTree = $(rootSelector).dynatree("getTree");
      $scope.ctypeTree.reload();
      $scope.ctypeTreeRoot = $(rootSelector).dynatree("getRoot");
      $scope.ctypeTreeRoot.sortChildren(null, true);

    };

    function collectCategoryByAttr(custAttr, categories, results) {
        var length = categories.length;
        for (var i = 0; i < length; i++) {
            var category = categories[i];
            if (category.custAttr === custAttr) {
                results.push(category.id);
                return;
            }
            // Check children.
            if (category.children && category.children.length > 0) {
                collectCategoryByAttr(custAttr, category.children, results);
            }
        }
    }

    $scope.updatePocketMaterialUI = function () {
        var hasPocket = $scope.formData.hasPocket && $scope.formData.hasPocket.name == "true";
        if (!$scope.formData.pocketMaterials ||
            $scope.formData.pocketMaterials.length == 0 ||
            !$scope.formData.brand ||
            !$scope.showHasPocket ||
            hasPocket) {
            $scope.showPocketMaterial = false;
        } else {
            $scope.showPocketMaterial = true;
        }
        if (!$scope.$$phase) $scope.$apply();
    }

    $scope.initPocketMaterialList = function (defaultPocketMaterial) {
        if (!PageData.treeData) return;
        $scope.formData.pocketMaterials = $scope.formData.pocketMaterials || [];
        var results = [];
        collectCategoryByAttr(CategoryAttr.PocketMaterial, PageData.treeData, results);

        if ($scope.formData.brand && results.length > 0) {
            var brandId = $scope.formData.brand.id;
            var param = {
                offset: 0,
                limit: 100,
                status: 1,
                categoryIds: results,
                brandIds: [brandId],
                branch: $scope.branch
            }
            ProductIO.searchProducts(param).success(function(data) {
                $scope.formData.pocketMaterials.length = 0;
                $scope.formData.pocketMaterials = data.products || [];
                // Set current.
                $scope.onPocketMaterialChanged();
                $scope.updatePocketMaterialUI();
            }).error(function(data, status, headers, config) {
                $scope.formData.pocketMaterials.length = 0;
                $scope.updatePocketMaterialUI();
            });
        } else {
            $scope.formData.pocketMaterials.length = 0;
            $scope.updatePocketMaterialUI();
        }
    };

    $scope.onPocketMaterialOptionChanged = function () {
        if (!$scope.formData || !$scope.formData.currentPocketMaterial) return;
        $scope.formData.pocketMaterial = $scope.formData.currentPocketMaterial.id;
        if (!$scope.$$phase) $scope.$apply();
    };

    $scope.onPocketMaterialChanged = function () {
        if (!$scope.formData || !$scope.formData.pocketMaterial) return;
        if ($scope.formData || $scope.formData.pocketMaterials) {
            var current = $scope.formData.pocketMaterials.find(function(material) {
                return material.id === $scope.formData.pocketMaterial;
            })
            $scope.formData.currentPocketMaterial = current;
        }
        if (!$scope.$$phase) $scope.$apply();
    };

    function startWith(text, prefix) {
        if (!text) {
            return false;
        }

        if (text.length < prefix.length) {
            return false;
        }

        return text.substring(0, prefix.length) === prefix;
    }

    $scope.showSizeFilterInfo = function(flag, node){
        $scope.sizeTypeFilters = [];
        $scope.sizeTypeFiltersMap = {};
        $scope.listSizeTypes(node.tree.tnRoot);
        for(var i=0;i<$scope.sizeTypeFilters.length;i++){
            var id = $scope.sizeTypeFilters[i].id;
            var selectedValueId = null;

            if($scope.formData.sizeTypeAttributes && $scope.formData.sizeTypeAttributes.length>0){
                for(var j=0;j<$scope.formData.sizeTypeAttributes.length;j++){
                    if(id === $scope.formData.sizeTypeAttributes[j].id ){
                        if($scope.formData.sizeTypeAttributes[j].valuesIds.length>0){
                            selectedValueId = $scope.formData.sizeTypeAttributes[j].valuesIds[0];
                        }
                    }
                }
            }

            for(var j=0;j<$scope.sizeTypeFilters[i].values.length;j++){
                var valueOption = $scope.sizeTypeFilters[i].values[j];
                $scope.sizeTypeFilters[i].values[j].checked = false;

                if(selectedValueId && (valueOption.id === selectedValueId)){
                    $scope.sizeTypeFilters[i].values[j].checked = true;
                }
            }
        }
        if(!$rootScope.$$phase){
            $scope.$digest();
        }
    }

    $scope.showSizeType = function(){
        if($scope.sizeTypeFilters && $scope.sizeTypeFilters.length>0){
            return true;
        }
        else{
            return false;
        }
    }

    $scope.listSizeTypes = function(node){
        if( node.bSelected ){//|| ($scope.formData && $scope.formData.categoryIds && $scope.formData.categoryIds.contains(node.data.id) )){
            if(node.data.attributes){
                var obj = {};
                obj.name = node.data.name;
                obj.attributes = node.data.attributes;

                var attributes = node.data.attributes;
                for(var i=0;i<attributes.length;i++){
                    var id = attributes[i].id;
                    if(!$scope.sizeTypeFiltersMap.hasOwnProperty(id)){
                        $scope.sizeTypeFilters.push(attributes[i]);
                        $scope.sizeTypeFiltersMap[id] = attributes[i];
                    }
                }
            }
        }

        if(node.childList && node.childList.length>0){
            for(var i=0;i<node.childList.length;i++){
                $scope.listSizeTypes(node.childList[i]);
            }
        }
    }


    /*
    zindex, default height and dwg/svg will be decided by content type.
    So after content type changed, we need update content type info.
    */
    $scope.updateContentTypeInfo = function() {
        var zindexInfo = '';
        var svgFileInfo = '';
        var defaultZindexValue = null;
        var defaultZindex = '200';
        $.each($scope.pageData.zIndexValues, function(index, zIndexValue) {
            if (zIndexValue.name == 200)
                defaultZindexValue = zIndexValue;
        });

        if ($scope.contentTypeInfoMap && $scope.formData.contentType && $scope.contentTypeInfoMap[$scope.formData.contentType.name]) {
            var contentTypeName = $scope.formData.contentType.name;
            var contentTypeInfo = $scope.contentTypeInfoMap[contentTypeName];

            if (startWith(contentTypeName, "door") ||
                startWith(contentTypeName, "window") ||
                contentTypeName === "build element/wall opening") {
                $scope.showHasPocket = true;
            } else {
                $scope.showHasPocket = false;
            }

            if (startWith(contentTypeName, "material") && contentTypeName.includes("wallpaper")) {
                $scope.formData.isContentTypeWallpaper = true;
            } else {
                $scope.formData.isContentTypeWallpaper = false;
            }

            // update zindex by content type
            if (contentTypeInfo.zindex) {
                $.each($scope.pageData.zIndexValues, function(index, zIndexValue) {
                    if (zIndexValue.name == contentTypeInfo.zindex)
                        $scope.formData.zindex = zIndexValue;
                });
            } else {
                $scope.formData.zindex = defaultZindexValue;
            }


            $scope.formData.defaultHeight = contentTypeInfo.defaultHeight

            if (contentTypeInfo.symbol.indexOf(".svg") === contentTypeInfo.symbol.length - 4) {
                $scope.formData.dwgFile = '';
                if ($scope.pageData.svgFileValues) {
                    for (var i = 0; i < $scope.pageData.svgFileValues.length; ++i) {
                        if ($scope.pageData.svgFileValues[i].name == contentTypeInfo.symbol) {
                            $scope.formData.svgFile = $scope.pageData.svgFileValues[i];
                            svgFileInfo = "svgFile: " +contentTypeInfo.symbol;
                            break;
                        }
                    }
                }
            } else {
                $scope.formData.dwgFile = contentTypeInfo.symbol;
                svgFileInfo = '';
            }

            zindexInfo = "z-index : " + (contentTypeInfo.zindex ? contentTypeInfo.zindex : defaultZindex);
        } else {
            // no content type info..
            $scope.formData.defaultHeight = null;
            $scope.formData.svgFile = null;
            $scope.formData.dwgFile = null;
            zindexInfo = '';
            $scope.showHasPocket = false;
            $scope.formData.isContentTypeWallpaper = false;
        }

        // show information on UI
        $('#zindexinfo').text(zindexInfo);
        $('#svgfileinfo').text(svgFileInfo);
        $('#contenttypeinfo').text("content type: " + ($scope.formData.contentType ? $scope.formData.contentType.name : ''));
    };

    /*
     * action handlers
     */
    $scope.makeCreationTime = function() {
        var moment1 = moment($scope.formData.creationTime);
        $scope.formData.creationTimeOutput = moment1.fromNow();
        $scope.formData.creationTimeOutputFormat = moment1.format();
    };

    $scope.makeModifiedTime = function() {
        var moment1 = moment($scope.formData.modifiedTime);
        $scope.formData.modifiedTimeOutput = moment1.fromNow();
        $scope.formData.modifiedTimeOutputFormat = moment1.format();
    };

    $scope.makePostProcessingTime = function() {

        // if its not working try this:
        // moment($scope.formData.lastTimeProcessed).add(2, 'hours');

        var moment1 = moment($scope.formData.lastTimeProcessed);
        $scope.formData.lastTimeProcessedOutput = moment1.fromNow();
        $scope.formData.lastTimeProcessedOutputFormat = moment1.format();
    };

    $scope.showPostProcessingLog = function(){
        robinDialog.alert($scope.formData.postProcessingLog);
    }

    $scope.onProductTypeChanged = function(productTypeDescription, skuDescription, isStyledProduct, is3DProduct) {
        $scope.productTypeDescriptionText = productTypeDescription;
        $scope.skuDescriptionText = skuDescription;
        $scope.formData.isStyledProduct = isStyledProduct;
        $scope.formData.is3DProduct = is3DProduct;
    };

    $scope.changeProductTypeDescription = function() {
        if ($scope.formData.productType != null) {
            switch ($scope.formData.productType.name) {
                case "3D":
                    var productTypeDescription = "";
                    if($scope.userRole == $scope.userRoles.MANUFACTURER){
                        productTypeDescription = " [model.max.zip + iso] or [model.max.zip + iso + png + obj]";
                    }else {
                        productTypeDescription = " [iso + obj + png]";
                    }
                    $scope.onProductTypeChanged(productTypeDescription, "", false, true);
                    break;
                case "FloorTiles":
                case "Wallpapers":
                    $scope.onProductTypeChanged(" [iso + jpg]", "", true, false);
                    break;
                case "Assembly":
                    var skuDescriptionText = "uploadproduct_assembly_sku_tip";
                    $scope.onProductTypeChanged(" [iso + scene]", skuDescriptionText, false, false);
                    break;
                case "Profile":
                    $scope.onProductTypeChanged(" [normal + obj + iso]", "", false, false);
                    break;
                case "2D Texture":
                    $scope.onProductTypeChanged(" [png]", "", false, false);
                    break;
                default :
                    $scope.onProductTypeChanged("", "", false, false);
                    break;
            }
        }
    };

    $scope.changeStatusDescription = function() {
        //var desc = $('#statusDescription');
        if ($scope.formData.status != null) {
            switch ($scope.formData.status.description) {
                case "Active":
                    $scope.statusDescriptionText = "uploadproduct_status_active";
                    break;
                case "Inactive":
                    $scope.statusDescriptionText = "uploadproduct_status_inactive";
                    break;
                case "Private":
                    $scope.statusDescriptionText = "uploadproduct_status_private";
                    break;
                case "Deleted":
                    $scope.statusDescriptionText = "uploadproduct_status_deleted";
                    break;
            }
        }
    };

    $scope.updateSelectedBrandCategory = function() {
        // find the id of 'Brands' node
        var brandsCategoryId = null;
        $scope.cTree.visit(function(node) {
            if (node.data.title == 'Brands') {
                brandsCategoryId = node.data.id;
            }
        }, false);
        // update selected brand
        var selectedBrandTitle = $scope.formData.brand.name;
        $scope.cTree.visit(function(node) {
            if (node.data.parentId == brandsCategoryId) {
                node.select(node.data.title.toLowerCase() == selectedBrandTitle.toLowerCase());
            }
        }, false);
    };

    $scope.updateSelectedRetailer = function() {
        // non empty selection
        if ($scope.formData.retailer) {
            $scope.showRetailerDetail = true;
        } else {
            $scope.showRetailerDetail = false;
        }
    }

    $scope.validateUrl = function(url) {
        return url.match(/(^http:\/\/)|(^www)/) != null;
    };

    $scope.goToRetailerProductUrl = function() {
        if ($scope.formData.retailerProductUrl && $scope.validateUrl($scope.formData.retailerProductUrl))
            window.open($scope.formData.retailerProductUrl);
        else
            robinDialog.alert($filter('translate')('uploadproduct_invalid_url'))
    };

    $scope.isPostProcessingStatus = function() {
        return !($scope.formData.postProcessingStatus == -1 || $scope.formData.postProcessingStatus == null);
    };

    $scope.hasPostProcessingLog = function() {
        return !($scope.formData.postProcessingLog == "" || $scope.formData.postProcessingLog == null);
    }

    $scope.loadProductByGuid = function(firstRequest) {
        if(!$scope.formData.id1 || $scope.formData.id1 == "") {
            robinDialog.alert($filter('translate')('uploadproduct_product_id_required'));
            return;
        }

        $scope.curtainText = 'loading';
        $scope.loading = true;

        Product.getById($scope.formData.id1)
        .then(function(data) {
            $scope.loading = false;
            $scope.formData.id1 = data.id;
            $scope.formData.name = data.name;
            $scope.formData.label = data.label;
            $scope.formData.dimension = data.dimension;
            $scope.formData.path = data.path;
            $scope.formData.externalId = data.externalId;
            $scope.formData.version = data.version;
            $scope.formData.postProcessingStatus = data.postProcessingStatus;
            $scope.formData.postProcessingLog = data.postProcessingLog;
            $scope.formData.creationTime = data.requestTime.creationTime;
            $scope.formData.modifiedTime = data.requestTime.modifiedTime;
            $scope.formData.lastTimeProcessed = data.lastTimeProcessed;
            $scope.formData.description = data.description;
            $scope.formData.retailerProductUrl = data.retailerProductUrl;
            $scope.formData.retailerPrice = data.retailerPrice;
            $scope.formData.categoryIds = data.categoryIds;
            $scope.formData.familyIds = $.isEmptyObject(data.familyIds)?null:data.familyIds;
            $scope.variations.length = 0; //clear it
            $scope.formData.forceTopViewGenerate = false;
            $scope.formData.pocketMaterial = '';
            $scope.formData.sizeTypeAttributes = data.rawAttributes;

            if (data.attributes.variations) {
              var all = data.attributes.variations.free;
              if (all) {
                for (i = 0; i < all.length; i++) $scope.variations.push(JSON.parse(all[i])); //add variations
              }
            }

            if (data.attributes.dimension) {
                $scope.formData.dimension = data.attributes.dimension;
            }

            // status
            for (var i = 0; i < $scope.pageData.statuses.length; ++i) {
                if ($scope.pageData.statuses[i].id == data.status) {
                    $scope.formData.status = $scope.pageData.statuses[i];
                    break;
                }
            }

            // brand
            for (i = 0; i < $scope.pageData.brands.length; ++i) {
                if ($scope.pageData.brands[i].id == data.brandId) {
                    $scope.formData.brand = $scope.pageData.brands[i];
                    break;
                }
            }

            // retailers
            for (i = 0; i < $scope.pageData.retailers.length; ++i) {
                if (data.retailer) {
                    if ($scope.pageData.retailers[i].id == data.retailer.id) {
                        $scope.formData.retailer = $scope.pageData.retailers[i];
                        if (data.retailer.price) {
                            $scope.formData.retailerPrice = parseFloat(data.retailer.price).toFixed(2);
                        }
                        $scope.formData.retailerProductUrl = data.retailer.url;
                        $scope.showRetailerDetail = true;
                        break;
                    }
                }
            }

            //styles
            if (!($.isEmptyObject(data.attributes) || $.isEmptyObject(data.attributes.style))) {// check if style exists
              for (i = 0; i < $scope.pageData.styleValues.length; ++i) {
                if ($scope.pageData.styleValues[i].id == data.attributes.style) {
                  $scope.formData.style = $scope.pageData.styleValues[i];
                  break;
                }
              }
            }


            //materials if have
            if (!($.isEmptyObject(data.attributes) || $.isEmptyObject(data.attributes.material))) {// check if material exists
              for (i = 0; i < $scope.pageData.materialValues.length; ++i) {
                if ($scope.pageData.materialValues[i].id == data.attributes.material) {
                    $scope.formData.material = $scope.pageData.materialValues[i];
                    break;
                }
              }
            }

            // is scalable
            for (i = 0; i < $scope.pageData.isScalableValues.length; ++i) {
                if ($scope.pageData.isScalableValues[i].id == data.attributes.isScalable) {
                    $scope.formData.isScalable = $scope.pageData.isScalableValues[i];
                    break;
                }
            }

            // has pocket
            if (!($.isEmptyObject(data.attributes) || $.isEmptyObject(data.attributes.hasPocket))) {// check if hasPocket exists
                for (i = 0; i < $scope.pageData.hasPocketValues.length; ++i) {
                    if ($scope.pageData.hasPocketValues[i].id == data.attributes.hasPocket) {
                        $scope.formData.hasPocket = $scope.pageData.hasPocketValues[i];
                        break;
                    }
                }
            }

            // pocket material
            if (!($.isEmptyObject(data.attributes) || $.isEmptyObject(data.attributes.pocketMaterial))) {// check if hasPocket exists
                $scope.formData.pocketMaterial = data.attributes.pocketMaterial;
            }

            /* // tricky method to update zIndex after timeOut promise
            // zindex
            for (i = 0; i < $scope.pageData.zIndexValues.length; ++i) {
                if ($scope.pageData.zIndexValues[i].id == data.attributes.zindex) {
                    $scope.formData.zindex = $scope.pageData.zIndexValues[i];
                    break;
                }
            }*/

            // content type
            // Init it to default, in case the product does not have this value.
            if (data.attributes.contentType) {
                for (i = 0; i < $scope.pageData.contentTypeValues.length; ++i) {
                    if ($scope.pageData.contentTypeValues[i].id == data.attributes.contentType) {
                        $scope.formData.contentType = $scope.pageData.contentTypeValues[i];
                        break;
                    }
                }
            }

            /* // tricky method to update defaultHeight and dwgFile after timeOut promise
            if (data.attributes.defaultHeight) {
                $scope.formData.defaultHeight = data.attributes.defaultHeight;
            }

            if (data.attributes.dwgFile) {
                $scope.formData.dwgFile = data.attributes.dwgFile;
            }
            */
            // product type
            for (i = 0; i < $scope.pageData.productTypeValues.length; ++i) {
                if ($scope.pageData.productTypeValues[i].id == data.attributes.productType) {
                    $scope.formData.productType = $scope.pageData.productTypeValues[i];
                    break;
                }
            }

            if ($scope.formData.productType != null &&
                ($scope.formData.productType.name == "FloorTiles" || $scope.formData.productType.name == "Wallpapers")) {
                $scope.formData.isStyledProduct = true;
            } else {
                $scope.formData.isStyledProduct = false;
            }

            if ($scope.formData.productType != null && $scope.formData.productType.name == "3D") {
                $scope.formData.is3DProduct = true;
            } else {
                $scope.formData.is3DProduct = false;
            }

            //mesh reduce
            if ($scope.formData.is3DProduct) {
                //set default value
                $.each($scope.pageData.meshReduceValues, function(index, meshReduce) {
                    if (meshReduce.name == "false") {
                        $scope.formData.meshReduce = meshReduce;
                    }
                });
                $.each($scope.pageData.meshReduceValues, function(index, meshReduce) {
                    if (meshReduce.id == data.attributes.meshReduce) {
                        $scope.formData.meshReduce = meshReduce;
                    }
                })
            }

            //merge groups with categries
                if(data.groups){
                    for (var i = 0; i < data.groups.length; i++) {
                        data.categoryIds.push(data.groups[i])
                    }
                }

            // categories
            $('.categories-tree').dynatree("getRoot").visit(function(node) {
                var nd = node.data;
                if (!$scope.$$phase) $scope.cTree.selectKey(nd.key, false);
                for (var i = 0; i < data.categoryIds.length; ++i) {
                    if (nd.id == data.categoryIds[i]) {
                        if (!$scope.$$phase) $scope.cTree.selectKey(nd.key, true);
                    }
                }
            });
           // families
            $('.families-tree').dynatree("getRoot").visit(function (node) {
              var nd = node.data;
              if (!$scope.$$phase) $scope.fTree.selectKey(nd.key, false);
              if (!$.isEmptyObject(data.familyIds)) {
                for (var i = 0; i < data.familyIds.length; ++i) {
                  if (nd.id == data.familyIds[i])
                    if (!$scope.$$phase) $scope.fTree.selectKey(nd.key, true);
                }
              }

            });

            // files
            $scope.formData.files = [];
            for (i = 0; i < data.files.length; ++i) {
                $scope.formData.files.push({
                    name: data.files[i].name,
                    uploadByUser: data.files[i].uploadByUser,
                    metaData: data.files[i].metaData,
                    serverUrl: data.files[i].serverName,
                    storageUrl: data.files[i].storageUrl
                });
            }

            // counpon types
            if (data.attributes.couponType) {
                $scope.formData.couponType = [];
                for (i = 0; i < $scope.pageData.couponTypeValues.length; ++i) {
                    for (j = 0; j < data.attributes.couponTypeValues.length; ++j) {
                        if (data.attributes.couponTypeValues[j] === $scope.pageData.couponTypeValues[i].id) {
                            $scope.formData.couponType.push($scope.pageData.couponTypeValues[i]);
                        }
                    }
                }
            }

            // tileSize
            if (data.attributes.tileSize) {
                // load new tileSize, convert string to number
                $scope.formData.xDimension = Number(data.attributes.tileSize.x);
                $scope.formData.yDimension = Number(data.attributes.tileSize.y);
            }

            // unitSize
            if (data.attributes.unitSize) {
                $scope.formData.unitDimensionWidth = Number(data.attributes.unitSize.width);
                $scope.formData.unitDimensionHeight = Number(data.attributes.unitSize.height);
            }

            // size type attributes
            $scope.showSizeFilterInfo(null,$scope.cTree.tnRoot);

            $scope.formData.previewImage = $.grep(data.files, function(file, index) {
                return !file.metaData || file.metaData == "iso";
            })[0].storageUrl;

            if (firstRequest) {
                $timeout(function() {
                    $scope.$digest();
                    updateProductStatus = setInterval(updateProductStatusUntilSuccess, 30000);
                }, 1);
            }

            $timeout(function() {
                $scope.updateCouponTypeTreeSelection();
                $scope.updateContentTypeInfo();
                $scope.updateCtypeTreeSelection();
                $scope.updateCategoriesSelection();
                $scope.updateFamiliesSelection();
            }).then(function() {
                if (data.attributes.dwgFile) {
                    $scope.formData.dwgFile = data.attributes.dwgFile;
                }
                if (data.attributes.defaultHeight){
                    $scope.formData.defaultHeight = data.attributes.defaultHeight;
                }
                for (i = 0; i < $scope.pageData.zIndexValues.length; ++i) {
                    if ($scope.pageData.zIndexValues[i].id == data.attributes.zindex) {
                        $scope.formData.zindex = $scope.pageData.zIndexValues[i];
                        break;
                    }
                }
            });

            $scope.loading = false;

            // Go to edit mode.
            $scope.mode = $scope.EMode.eEdit;

        }, function(error) {
            robinDialog.alert('server error - ' + error.data); // TODO: improve this error handling
            $scope.loading = false;
        });
    };

    function updateProductStatusUntilSuccess() {
        var done = false;
        var firstIdStatusChecking = $scope.formData.id1;
        var numOfTries = 100;
        // while(!done) {

        ProductIO.getProduct($scope.formData.id1, $scope.language.code, $scope.branch).success(function(data) {
            $scope.formData.postProcessingStatus = data.postProcessingStatus;
            $scope.formData.creationTime = data.creationTime;
            $scope.formData.modifiedTime = data.modifiedTime;
            $scope.formData.lastTimeProcessed = data.lastTimeProcessed;
            $scope.formData.version = data.version;

        }).error(function(data, status, headers, config) {
            done = true;
        });

        if (($scope.formData.postProcessingStatus == 1) ||
            ($scope.formData.id1 != firstIdStatusChecking) ||
            (numOfTries-- == 0)) {
            clearInterval(updateProductStatus);
        }

        if(!$scope.$$phase) {
            $scope.$digest();
        }
    }

    function validation() {

        var isValid = true;
        var message = '';

        if ($scope.formData.name === null || $scope.formData.name === '') {
            isValid = false;
            message += $filter('translate')('uploadproduct_name_not_empty') + '<br />';
        }

        // change that we dont need to check description if empty, if it is we put .
        /*if ($scope.formData.description == null) {
         isValid = false;
         message += 'Description - must not be empty' + '<br />';
         }*/

        if ($scope.formData.contentType == null) {
            isValid = false;
            message += $filter('translate')('uploadproduct_content_type_not_empty') + '<br />';
        }

        // check unitSize for material/wallpaper
        if ($scope.formData.contentType !== null) {
            var contentTypeName = $scope.formData.contentType.name;
            if (startWith(contentTypeName, "material") && contentTypeName.includes("wallpaper")) {
                if (!document.getElementById("unit_dimension_width").validity.valid) {
                    isValid = false;
                    message += $filter('translate')('uploadproduct_unit_width_invalid') + '<br />';
                }
                if (!document.getElementById("unit_dimension_height").validity.valid) {
                    isValid = false;
                    message += $filter('translate')('uploadproduct_unit_height_invalid') + '<br />';
                }
            }
        }

        /*
        if ($scope.formData.style == null) {
            isValid = false;
            message += 'Styles - must not be empty.' + '<br />';
        }*/

        if ($scope.formData.productType.name === '3D' && $scope.formData.isScalable == null) {
            isValid = false;
            message += $filter('translate')('uploadproduct_scalable_not_empty') + '<br />';
        }

        if ($scope.formData.brand == null) {
            isValid = false;
            message += $filter('translate')('uploadproduct_brand_not_empty') + '<br />';
        }

        if ($scope.formData.status == "Deleted" && didntConfirmYet) {
            isValid = false;
            var msg = $filter('translate')('delete_confirm');
            $scope.openConfirmationDialog('uploadproduct_delete_product', msg + ' "' + $scope.formData.name + '"?',
                function() {
                    didntConfirmYet = false;
                    $scope.closeCurrentDialog();
                    $scope.onSave();
                });
        }

        if ($scope.cTree.getSelectedNodes(true).length == 0) {
            isValid = false;
            message += $filter('translate')('uploadproduct_categories_at_least') + '<br />';
        }

        if ($scope.formData.productType == null) {
            isValid = false;
            message += $filter('translate')('uploadproduct_product_type_not_empty') + '<br />';
        } else {
            switch ($scope.formData.productType.name) {

                case "3D":
                    var hasIso = false;
                    var hasObj = false;
                    var hasTxtr = false;
                    var hasMaxFile = false;
                    for (var i = 0; i < $scope.formData.files.length; i++) {
                        switch ($scope.formData.files[i].metaData) {
                            case 'iso':
                            {
                                if (hasIso) {
                                    isValid = false;
                                    message += $filter('translate')('uploadproduct_file_type_iso') + '<br />';
                                }
                                hasIso = true;
                            }
                            break;
                            case 'obj':
                            {
                                if (hasObj) {
                                    isValid = false;
                                    message += $filter('translate')('uploadproduct_file_type_obj') + '<br />';
                                }
                                hasObj = true;
                            }
                            break;
                            case 'txtr':
                            {
                                if (hasTxtr) {
                                    isValid = false;
                                    message += $filter('translate')('uploadproduct_file_type_txtr') + '<br />';
                                }
                                hasTxtr = true;
                            }
                            break;
                            case '3ds':
                            {
                                if (hasMaxFile) {
                                    isValid = false;
                                    message += $filter('translate')('uploadproduct_file_type_3ds') + '<br />';
                                }
                                hasMaxFile = true;
                            }
                            break;
                        }
                    }
                    if($scope.userRole == $scope.userRoles.MANUFACTURER){
                        if(hasObj) {
                            if (hasIso && hasMaxFile && hasTxtr){
                                // ok
                            }
                            else {
                                isValid = false;
                                message += $filter('translate')('uploadproduct_product_type_3d') + '<br />';
                            }
                        }
                        else if (!(hasIso && hasMaxFile)) {
                            isValid = false;
                            message += $filter('translate')('uploadproduct_product_type_3d') + '<br />';
                        }
                    } else {
                        if (!(hasIso && hasObj && hasTxtr)) {
                            isValid = false;
                            message += $filter('translate')('uploadproduct_product_type_3d_obj') + '<br />';
                        }
                    }
                    break;

                case "FloorTiles":
                case "Wallpapers":
                    // check dimension
                    if (!document.getElementById("x_dimension").validity.valid) {
                        isValid = false;
                        message += $filter('translate')('uploadproduct_product_dimension_x') + '<br />';
                    }
                    if (!document.getElementById("y_dimension").validity.valid) {
                        isValid = false;
                        message += $filter('translate')('uploadproduct_product_dimension_y') + '<br />';
                    }
                    var hasIso = false;
                    var hasJpg = false;
                    for (var i = 0; i < $scope.formData.files.length; i++) {
                        switch ($scope.formData.files[i].metaData) {
                            case 'iso':
                            {
                                if (hasIso) {
                                    isValid = false;
                                    message += $filter('translate')('uploadproduct_file_type_iso') + '<br />';
                                }
                                hasIso = true;
                            }
                                break;
                            case 'wallfloor':
                            {
                                if (hasJpg) {
                                    isValid = false;
                                    message += $filter('translate')('uploadproduct_file_type_jpg') + '<br />';
                                }
                                hasJpg = true;
                            }
                                break;
                        }
                    }

                    if (!(hasIso && hasJpg)) {
                        isValid = false;
                        message += $filter('translate')('uploadproduct_product_type_wallpaper') + '<br />';
                    }
                    break;

                case "Assembly":
                    // check there's one iso file and one scene file
                    var hasIso = false,
                        hasScene = false;
                    $.each($scope.formData.files, function(index, file) {
                        switch (file.metaData) {
                            case 'iso':
                                if (hasIso) {
                                    isValid = false;
                                    message += $filter('translate')('uploadproduct_file_type_iso') + '<br />';
                                }
                                hasIso = true;
                                break;
                            case 'scene':
                                if (hasScene) {
                                    isValid = false;
                                    message += $filter('translate')('uploadproduct_file_type_scene') + '<br />';
                                }
                                hasScene = true;
                                break;
                        }
                    });
                    if (!(hasIso && hasScene)) {
                        isValid = false;
                        message += $filter('translate')('uploadproduct_product_type_assembly') + '<br />';
                    }
                    break;

                case "Profile":
                    var hasNormal = false;
                    var hasObj = false;
                    var hasIso = false;
                    for (var i = 0; i < $scope.formData.files.length; i++) {
                        switch ($scope.formData.files[i].metaData) {
                            case 'normal':
                            {
                                if (hasNormal) {
                                    isValid = false;
                                    message += $filter('translate')('uploadproduct_file_type_normal') + '<br />';
                                }
                                hasNormal = true;
                            }
                            break;
                            case 'obj':
                            {
                                if (hasObj) {
                                    isValid = false;
                                    message += $filter('translate')('uploadproduct_file_type_obj') + '<br />';
                                }
                                hasObj = true;
                            }
                            break;
                            case 'iso':
                            {
                                if (hasIso) {
                                    isValid = false;
                                    message += $filter('translate')('uploadproduct_file_type_iso') + '<br />';
                                }
                                hasIso = true;
                            }
                                break;
                        }
                    }
                    if (!(hasNormal && hasObj && hasIso)) {
                        isValid = false;
                        message += $filter('translate')('uploadproduct_product_type_profile') + '<br />';
                    }
                    break;
            }

            // check that the files extension is corresponding to the file type
            var msg = validateFiles(true);
            if (msg != "") {
                message += msg;
                isValid = false;
            }
        }
        message != "" ? robinDialog.alert(message) : message;

        return isValid;
    }

    function validateFiles(allowEmptyMetadata) {
        var message = "";
        for (var i = 0; i < $scope.formData.files.length; ++i) {
            var fileExtension = $scope.formData.files[i].name.substring($scope.formData.files[i].name.lastIndexOf('.') + 1, $scope.formData.files[i].name.length);
            fileExtension = fileExtension.toLowerCase();
            switch ($scope.formData.files[i].metaData) {
                case 'iso':
                    if (fileExtension != 'jpg' && fileExtension != 'jpeg') {
                        message += $filter('translate')('uploadproduct_iso_extension') + '<br />';
                    }
                    break;
                case 'wallfloor':
                    if (fileExtension != 'jpg' && fileExtension != 'jpeg') {
                        message += $filter('translate')('uploadproduct_wallpaper_extension') + '<br />';
                    }
                    break;
                case 'obj':
                    if (fileExtension != 'obj') {
                        message += $filter('translate')('uploadproduct_obj_extension') + '<br />';
                    }
                    break;
                case 'txtr':
                    if (fileExtension != 'png') {
                        message += $filter('translate')('uploadproduct_texture_extension') + '<br />';
                    }
                    break;
                case 'thumbnail':
                    if (fileExtension != 'jpg' && fileExtension != 'jpeg') {
                        message += $filter('translate')('uploadproduct_thumbnail_extension') + '<br />';
                    }
                    break;
                case 'normal':
                    if (fileExtension != 'png') {
                        message += $filter('translate')('uploadproduct_material_normal_extension') + '<br />';
                    }
                    break;
                default:
                    if (!allowEmptyMetadata) message += $filter('translate')('uploadproduct_choose_file_type') + '<br />';
            }
        }
        return message
    }

    function clearForm() {
        /*$scope.formData = {
         name: null,
         description: null,
         status: null,
         path: null,
         externalId: null,
         brand: null,
         retailerId: null,
         zindex: null,
         categoryIds: null,
         files: [],
         newKey1: null,
         productType: null
         };*/

        $scope.formData = {};
        $scope.formData.files = [];

        $('.categories-tree').dynatree("getRoot").visit(function(node) {
            var nd = node.data;
            // set the node's selection mode
            if (!$scope.$$phase) $scope.cTree.selectKey(nd.key, false);
        });
    }

    $scope.onDuplicateBtnClicked = function() {
        $scope.mode = $scope.EMode.eDuplicate;
        $scope.formData.files = [];
        $scope.formData.id1 = null;
        $scope.formData.postProcessingStatus = null;
    }

    $scope.onCreateProductBtnClicked = function() {
        $scope.openConfirmationDialog(('uploadproduct_create_mode'), ('uploadproduct_confirm_to_create_mode'),
            function() {
                if($location.url()==$location.path()) {
                    clearForm();
                    $scope.mode = $scope.EMode.eCreate;
                    $scope.variations.length = 0;
                    setFieldsDefaultValues();
                } else {
                    $timeout(function() {
                        $location.url($location.path());
                    }, 500);
                }
                $scope.closeCurrentDialog();
            });
    }

    $scope.onEditProductBtnClicked = function() {
        // When click on edit button the status is eEditCreate
        // until target product data retrieved from back-end.
        $scope.mode = $scope.EMode.eEditCreate;
    }

    function moveToUpdateModeAfterCreate(guid) {
        clearForm();
        $scope.formData.id1 = guid;
        $timeout(function() {
            $scope.loadProductByGuid(true);
        }, 500);
    }

    function showSuccessMessage(message) {
        $scope.successMessage = message;
        $('.alert .close').on('click', function(e) {
            $(this).parent().hide();
        });
        $('#success-alert').show();
        $("html, body").animate({scrollTop: 0}, "slow");
        try {
            $scope.$digest();
        } catch (err) {
            //kill this until will be fixed
            console.log(err.stack);
        }
    }

    $scope.onSave = function() {
        if (!validation()) return;
        didntConfirmYet = true;
        $scope.curtainText = 'uploadproduct_saving_product';
        $scope.loading = true;
        /*var selectedCategoriesIds = [];

         $.each($scope.cTree.getSelectedNodes(true), function(index, node) {
         selectedCategoriesIds.push(node.data.id);
         });*/

        var formData = {};
        angular.copy($scope.formData, formData);

        formData.categoryIds = $.map($scope.cTree.getSelectedNodes(), function(node) {
            return node.data.id;
        });

        formData.familyIds = $.map($scope.fTree.getSelectedNodes(), function (node) {
          return node.data.id;
        });

        formData.couponType = $.map($scope.pkTypeTree.getSelectedNodes(), function (node) {
          return node.data.id;
        });

        if (!$scope.showPocketMaterial) {
            formData.pocketMaterial = null;
        }

        if (!$scope.showHasPocket) {
            formData.hasPocket = null;
        }

        var sizeTypeAttributes = [];
        if($scope.sizeTypeFilters){
            $.each($scope.sizeTypeFilters, function(index, filter) {
                var filterId = filter.id;
                var selectedValueId = $("input[name='"+filterId+"']:checked").val();
                if(selectedValueId !== undefined){
                    var sizeTypeAttr = {};
                    sizeTypeAttr.id = filterId;
                    sizeTypeAttr.valueId = selectedValueId;
                    sizeTypeAttributes.push(sizeTypeAttr);
                }
            });
            formData.sizeTypeAttributes = sizeTypeAttributes;
        }

        if ($scope.mode === $scope.EMode.eCreate || $scope.mode === $scope.EMode.eDuplicate) {
            //postData.files = $scope.formData.files;
            Product.create(formData, $scope.pageData.attributeDB).success(function(data) {
                //robinDialog.alert("Done saving product!<br />Guid is - " + data);
                $scope.loading = false;
                var guid = data.substring(1, data.length - 1);
                //moveToUpdateModeAfterCreate(guid);
                //showSuccessMessage("Done saving product!\nGuid - " + guid);
                robinDialog.alert("Saved successfully!");
                var url = '/' + $rootScope.tenantId +'/upload-product' + '?guid=' + guid;
                url += ('&mode=' + $scope.EMode.eEdit);
                url += ('&branch=' + $scope.branch);
                url += ('&lang=' + $scope.language.code);

                $location.url(url);
            }).error(function(data, status, headers, config) {
                robinDialog.alert(data);
                $scope.loading = false;
            });
        } else if ($scope.mode === $scope.EMode.eEdit) {
            Product.patch(formData, $scope.pageData.attributeDB).success(function(data) {
                $scope.loading = false;
                moveToUpdateModeAfterCreate(formData.id1);
                showSuccessMessage("Done updating product!\nGuid - " + formData.id1);
            }).error(function(data, status, headers, config) {
                robinDialog.alert(data);
                $scope.loading = false;
            });
        }
    };

    $scope.downloadFile = function(fileUrl) {
        var link = document.createElement("a");
        link.href = fileUrl;
        link.click();
        //window.open(fileUrl,'mywindow','width=400,height=200')
    };

    /*
     * files handlers
     */

    $scope.currentModalSelector = null; // saves current modal

    $scope.openUploadFileDialog = function() {
        $scope.currentModalSelector = '#upload-file-modal';
        $($scope.currentModalSelector).on('hidden.bs.modal', function () {
            if($scope.xhr && $scope.xhr.readyState != 4){
                $scope.xhr.abort();
            }
            $scope.setLoading(false);
            $scope.closeCurrentDialog();
        })

        $($scope.currentModalSelector).modal('show');
        $scope.resetUploadFileDialog();
    };

    $scope.closeCurrentDialog = function() {
        $($scope.currentModalSelector).modal('hide');
        $scope.xhr = null;
    };

    $scope.resetUploadFileDialog = function() {
        $('#file-upload-form').get(0).reset();
        $('.progress-upload-file').hide();
    };

    $scope.submitFileForm = function(file1) {

        if ($scope.mode === $scope.EMode.eEdit) {
            var uploadUrl = '/mw/upload/' + $rootScope.tenantId + '?' + 'name=' + file1.name + '&key=' + $scope.formData.id1 + '&haveGuid=true' + '&lang=' + $scope.language.code;
        } else {
            var key = $scope.formData.newKey1;
            var uploadUrl = '/mw/upload/' + $rootScope.tenantId + '?' + 'name=' + file1.name + (key != null ? '&key=' + key : '') + '&lang=' + $scope.language.code;
        }

        $scope.setLoading(true);
        var progressSelector = '.progress-upload-file';
        $scope.xhr = robinCommon.submitFilesForm(
            '#file-upload-form', // form selector
            progressSelector,
            // url for sending the form
            uploadUrl,
            // on success
            function(data) {
                $scope.formData.newKey1 = data.newKey;
                $scope.formData.forceTopViewGenerate = true;
                $scope.formData.files.push({
                    name: data.realFileName, // real file name
                    metaData: '', // maybe in the future to check the extension of the file and give the user suggestion
                    uploadByUser: true,
                    serverName: data.fileUrl,
                    storageUrl: data.storageUrl
                });

                $scope.refreshImages();
                $scope.setLoading(false);

                $scope.$digest();
                $scope.closeCurrentDialog();
            },
            // on error
            function(data) {
                $(progressSelector).hide();
                $scope.formData.newKey1 = null;

                if(data.statusText == 'abort'){
                    robinDialog.alert('File upload is aborted.');
                }
                else {
                    robinDialog.alert('Unable to upload file, error: ' + data.statusText);
                }
                $scope.setLoading(false);

                $scope.closeCurrentDialog();
            },
            // on before send
            function() {
                $(progressSelector).show();
            },
            // on during upload
            function(e) {
                /*if (e.lengthComputable) {
                    $(progressSelector).attr({
                        value: e.loaded,
                        max: e.total
                    });
                }*/
            }
        );

    };

    $scope.removeFile = function(fileIndex) {
        $scope.formData.files.splice(fileIndex, 1);
    };

    $scope.setLoading = function(toggle) {
        $scope.loading = toggle;
        $scope.$digest();
    };

    $scope.refreshImages = function() {
        $scope.extTimestamp = '?' + (new Date().getTime());
        $scope.$digest();
    };

    $scope.openConfirmationDialog = function(title, message, onConfirm, okText, cancelText) {
        $scope.currentModalSelector = '#confirmation-modal';
        $scope.confirmationPopup = {
            title: title,
            message: message,
            onConfirm: onConfirm,
            okText: okText ? okText : 'ok',
            cancelText: cancelText ? cancelText : 'cancel'
        };
        $($scope.currentModalSelector).modal('show');
    };

    $scope.onLanguageChange = function() {
        $window.sessionStorage[languageKey] = JSON.stringify($scope.language);
        $route.reload();
    };

    $scope.onBranchChange = function() {
        $location.search({'branch': $scope.branch, 'guid': $scope.formData.id1});
        $route.reload();
    };

    $scope.updateCategoriesSelection = function() {

        var mapCategoryProductsCount = $scope.createMapCategoryProductsCount();
        //tree.renderInvisibleNodes();

        $scope.updateTreeSelection(mapCategoryProductsCount);
        $scope.updateTreeUi(mapCategoryProductsCount);

    };

    $scope.updateFamiliesSelection = function () {
      if (!$.isEmptyObject($scope.formData.familyIds)) {
        var mapFamilieProductsCount = $scope.createMapFamilyProductsCount();
        //tree.renderInvisibleNodes();

        $scope.updateFamilyTreeSelection(mapFamilieProductsCount);
        $scope.updateFamilyTreeUi(mapFamilieProductsCount);
      }
    };

    // returns a map from category id to num of selected products with that category
    $scope.createMapCategoryProductsCount = function() {
        var mapCategoryProductsCount = {};
        $.each($scope.formData.categoryIds, function(index, catId) {
            var catCount = mapCategoryProductsCount[catId];
            mapCategoryProductsCount[catId] = catCount ? catCount+1 : 1;
        });

        return mapCategoryProductsCount;
    };

      // returns a map from category id to num of selected products with that category
    $scope.createMapFamilyProductsCount = function () {
      var mapFamilyProductsCount = {};
      $.each($scope.formData.familyIds, function (index, catId) {
        var catCount = mapFamilyProductsCount[catId];
        mapFamilyProductsCount[catId] = catCount ? catCount + 1 : 1;
      });

      return mapFamilyProductsCount;
    };

    // for each tree node, set its selection state, according to the selected products
    $scope.updateTreeSelection = function(mapCategoryProductsCount) {
        $scope.cTreeRoot.visit(function(node) {
            var nd = node.data;
            // set the node's selection mode
            if (!$scope.$$phase) $scope.cTree.selectKey(nd.key, nd.id in mapCategoryProductsCount);
        });
    };

    $scope.updateFamilyTreeSelection = function (mapFamilyProductsCount) {
      $scope.fTreeRoot.visit(function (node) {
        var nd = node.data;
        // set the node's selection mode
        if (!$scope.$$phase) $scope.fTree.selectKey(nd.key, nd.id in mapFamilyProductsCount);
      });
    };

    $scope.updateCtypeTreeSelection = function () {
        if (!$scope.formData.contentType) return;
        var cType = $scope.formData.contentType;
        $scope.ctypeTreeRoot.visit(function (node) {
            var nd = node.data;
            // set the node's selection mode
            if (!$scope.$$phase)  $scope.ctypeTree.selectKey(nd.key, nd.contentTypeValueId === cType.id);
        });
    };

    $scope.updateCouponTypeTreeSelection = function() {
        if (!$scope.formData.couponType) return;
        // Clear and set selection status.
        $scope.pkTreeRoot.visit(function (node) {
            var nd = node.data;
            $scope.pkTypeTree.selectKey(nd.key, false);
            // set the node's selection mode
            for (i = 0; i < $scope.formData.couponType.length; ++i) {
                var isSelected = (nd.fullName === $scope.formData.couponType[i].name);
                if (isSelected) {
                    $scope.pkTypeTree.selectKey(nd.key, true);
                }
            }
          });
    };

    // for each tree node, set the title, style, and icon
    $scope.updateTreeUi = function(mapCategoryProductsCount) {
        $scope.cTreeRoot.visit(function(node) {
            var nd = node.data;
            // how many products have this node
            nd.count = nd.id in mapCategoryProductsCount ? mapCategoryProductsCount[nd.id] : 0;
            // set the node's title
            //nd.title = nd.count ? nd.name + ' (' + nd.count + ')' : nd.name;
            // set the node's icon
            nd.addClass = '';
            if ((nd.isParent && (0 < nd.count)) ||
                (!nd.isParent && 0 < nd.count)) {
                nd.addClass = 'dynatree-partsel';
            }
        });
        $scope.cTree.redraw();
    };

    $scope.updateFamilyTreeUi = function (mapFamilyProductsCount) {
      $scope.fTreeRoot.visit(function (node) {
        var nd = node.data;
        // how many products have this node
        nd.count = nd.id in mapFamilyProductsCount ? mapFamilyProductsCount[nd.id] : 0;
        // set the node's title
        //nd.title = nd.count ? nd.name + ' (' + nd.count + ')' : nd.name;
        // set the node's icon
        nd.addClass = '';
        if ((nd.isParent && (0 < nd.count)) ||
            (!nd.isParent && 0 < nd.count)) {
          nd.addClass = 'dynatree-partsel';
        }
      });
      $scope.fTree.redraw();
    };

    $scope.sendToTopview = function() {
        $scope.loading = true;
        var patchProducts = [
            {
                id: $scope.formData.id1,
                files: $scope.formData.files
            }
        ];

        ProductIO.patchProducts(patchProducts, $scope.language.code, $scope.branch).success(function(data) {
            updateProductStatusUntilSuccess();
            $timeout(function() {
                    updateProductStatus = setInterval(updateProductStatusUntilSuccess, 30000);
                }, 1);
            $scope.loading = false;
            robinDialog.alert('sent successfully');
        }).error(function(data, status, headers, config) {
            $scope.loading = false;
            robinDialog.alert('server error, please try again.');
        });
    };

    $scope.onEditVariationBtnClicked = function() {
        if (!$scope.formData.id1) return; // Inaccessible.
        var url = '/#/' + $rootScope.tenantId +'/variation' + '?guid=' + $scope.formData.id1;
        var mode = $scope.variations.length > 0 ? 'edit' : 'create';
        url += ('&mode=' + mode);
        $window.open(url, '_blank');
    };

    /*
     * c'tor
     */
    (function() {
        Product.languageCode = $scope.language.code;
        Product.tenantId = $rootScope.tenantId;

        $q.all([
            $scope.initContentTypeInfoMap(), // get content type
            TenantIO.getTenantBranch(), // get tenant branch
            PageData.populateData($scope.language.code, $scope.branch) // init page data
        ]).then(function(results){
            var branchData = results[1].children;
            $scope.availableBranches = branchData ? branchData.slice(0) : [];
            if ($scope.availableBranches.length > 0) {
                $scope.availableBranches.unshift('');
            }
            $scope.pageData = PageData.pageData;
            $scope.initTreeDataAttrs(PageData.treeData); // attach needed attrs to raw data
            $scope.initTreeDataAttrs(PageData.familyTreeData);
            $scope.initTreeDataAttrs(PageData.ctypeTreeData);
            $scope.initFamilyTree($scope.fTreeSelector, PageData.familyTreeData);
            $scope.initCtypeTree($scope.ctypeTreeSelector, PageData.ctypeTreeData);
            $scope.initCouponType($scope.ptypeSelector, PageData.pktypeTreeData);
            $scope.initTree($scope.cTreeSelector, PageData.treeData);

            setFieldsDefaultValues();

            hideMaterialForNonEzhome();

            // populate tenant branch
            if ($scope.formData.id1 && $scope.formData.id1 != "") {
                moveToUpdateModeAfterCreate($scope.formData.id1);
            }
            else {
                $scope.loading = false;
            }
        });

        $rootScope.$on('$translateChangeSuccess', function (event, data) {
            $scope.loading = true;
            PageData.populateData($scope.language.code, $scope.branch).then(function(){
                $scope.initCtypeTree($scope.ctypeTreeSelector, PageData.ctypeTreeData);
                $scope.loading = false;
            });
        });
    })();
}]);
