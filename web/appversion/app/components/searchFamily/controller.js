robinApp.controller('CtrlSearchFamily',
    ['$scope', '$filter', 'FamilyIO',
    function ($scope, $filter, FamilyIO) {

    // globals
    $scope.s3IconPathCategory = robinConsts.s3IconPathCategory;
    $scope.extTimestamp = '';

    $scope.loading = false;
    $scope.statuses = [
        {id: 0, description: "Deleted"},
        {id: 1, description: "Active"},
        {id: 2, description: "Hidden"},
        {id: 3, description: "Private"}
    ];

    $scope.totalFamiliesCount = null;

    // search params
    $scope.initSearchParams = function() {
        $scope.searchParams = {
            freeText: null,
            familyGuid: null,
            status: $scope.statuses[1]
        };
    };

    // basic search params
    $scope.familiesPerPage = {
        selected: 10,
        options: [10, 20, 50, 100]
    };
    $scope.currentPage = 0;
    $scope.onItemsOnPageChanged = function() {
        $scope.currentPage = 0;
        $scope.refreshSearchResults(false);
    };

    $scope.populateData = function() {
        $scope.refreshSearchResults(true);
    };

    $scope.onSearchButtonClicked = function() {
        $scope.selectedFamily = null;
        $scope.refreshSearchResults(true);
    };

    $scope.familyTree = undefined;
    $scope.families = [];
    $scope.selectedFamily = null;
    $scope.refreshSearchResults = function(syncTree) {

        var searchFilter = function (node) {
            var familyGuid = $scope.searchParams.familyGuid;
            var freeText = $scope.searchParams.freeText;
            var status = $scope.searchParams.status.id;

            familyGuid = familyGuid && familyGuid.trim();
            freeText = freeText && freeText.trim();

            if (familyGuid) {
                if (node.id == familyGuid) {
                    if (status != -1) {
                        return node.status == status;
                    }
                    return true;
                }
                return false;
            } else if (freeText){
                if (-1 != node.title.toLowerCase().indexOf(freeText.toLowerCase())) {
                    if (status != -1) {
                        return node.status == status;
                    }
                    return true;
                }
                return false;
            } else {
                if (status != -1) {
                    return node.status == status;
                }
                return true;
            }
        };

        var findNode= function (root, filter) {
            if ($scope.searchParams.familyGuid) {
                return TreeVisitor.findFirst(root, filter);
            } else {
                return TreeVisitor.findBranch(root, filter);
            }
        };

        var searchFamilies = function () {
            var data = $scope.familyTree;
            if (!data) {
                robinDialog.alert('No family tree.');
            } else {
                var targets = findNode(data, searchFilter);
                $scope.totalFamiliesCount = targets.length;

                var offset = $scope.currentPage * $scope.familiesPerPage.selected;
                var limit = $scope.familiesPerPage.selected;
                $scope.families = targets.slice(offset, offset + limit) || [];

                if ($scope.families.length < 1) {
                    robinDialog.alert('no families found, please revise your search.');
                }
            }
        }

        if (syncTree || !$scope.familyTree) {
            $scope.loading = true;
            FamilyIO.getFamilyTree().then(
                function (res) {
                    $scope.loading = false;
                    $scope.familyTree = res.data;
                    searchFamilies();
                },
                function (res) {
                    robinDialog.alert('server error, please try again.'); // TODO: improve this error handling
                    $scope.loading = false;
                }
            );
        } else {
            searchFamilies();
        }
    };

    $scope.onFamilySelected = function(familyIndex) {
        if (!$scope.loading) {
            $scope.selectedFamilyIndex = familyIndex;
            $scope.selectedFamily = angular.copy($scope.families[familyIndex]);
        }
    };

    // pagination
    $scope.refreshPagination = function(totalFamilies) {
        if ($scope.families.length > 0) {
            var totalPages = Math.ceil(totalFamilies / $scope.familiesPerPage.selected);
            var twbsParams = {
                totalPages: totalPages,
                visiblePages: Math.min(5, totalPages),
                //startPage: 1,
                onPageClick: function (event, page) {
                    $scope.currentPage = page - 1;
                    $scope.refreshSearchResults(false);
                }
            };
            $('#families-pagination').empty();
            $('#families-pagination').removeData("twbs-pagination");
            $('#families-pagination').unbind("page");
            $('#families-pagination').twbsPagination(twbsParams);
        }
    };

    $scope.openDeleteDialog = function (family) {
        var onDeleteConfirmed = function () {
            $scope.closeCurrentDialog();
            $scope.deleteFamily(family.id);
        };
        $scope.openConfirmationDialog('Delete Family', 'Are you sure you want to delete "' + family.title + '"?', onDeleteConfirmed);
    };

    // called after 'ok' is clicked at 'delete family' popup
    $scope.deleteFamily = function (id) {
        $scope.loading = true;

        FamilyIO.deleteFamily(id).then(function (res) {
            // Remove the item from family list
            for (var i = $scope.families.length - 1; i >= 0; i = i - 1) {
                if ($scope.families[i].id == id) {
                    $scope.families.splice(i, 1)
                }
            }

            // message to user
            robinDialog.alert('Family has been deleted.');

            $scope.loading = false;
        }, function (res) {
            robinDialog.alert('server error, please try again.');
            $scope.loading = false;
        });
    };

    $scope.openConfirmationDialog = function(title, message, onConfirm, okText, cancelText) {
        $scope.currentModalSelector = '#confirmation-modal';
        $scope.confirmationPopup = {
            title: title,
            message: message,
            onConfirm: onConfirm,
            okText: okText ? okText : 'Ok',
            cancelText: cancelText ? cancelText : 'Cancel'
        };
        $($scope.currentModalSelector).modal('show');
    };

    $scope.closeCurrentDialog = function() {
        $($scope.currentModalSelector).modal('hide');
    };

    // c'tor
    $scope.initSearchParams();
    $scope.populateData();

}]);
