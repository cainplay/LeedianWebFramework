robinApp.controller('CtrlTenantEdit', ['$scope', '$http', '$rootScope', '$filter', function ($scope, $http, $rootScope, $filter) {
    $scope.tenant = {}

    $scope.s3IconPathTenant = robinConsts.s3IconPathTenant;
    $scope.cIconUrl = null;

    $scope.extTimestamp = '';

    // set when do saving
    $scope.saving = false;
    $scope.tenantChanged = false;
    $scope.iconChanged = false;

    $scope.msg = "tenantedit_message";
    $scope.actionTip = null;

    $scope.resetForm = function() {
        $('#edit-tenant-form').get(0).reset();
        $('.progress-upload-icon').hide();
    };

    $scope.checkName = function(data) {
        // name changed
        $scope.tenantChanged = true
    }

    $scope.onEditTenantSubmitted = function() {
        var url = $scope.tenant.iconUrl
        if ($scope.cIconUrl) {
            // user uploaded a new url
            url = $scope.cIconUrl
        }
        var postData = {
            name: $scope.tenant.name,
            tenantId: $rootScope.tenantId,
            iconUrl: url
        }

        $scope.actionTip = $filter('translate')("tenantedit_saving") + $filter('translate')($scope.msg);
        $scope.saving = true;
        $http({
            method: 'PUT',
            url: '/mw/migrate/tenant/' + $rootScope.tenantId,
            headers: {
                'Content-Type': 'application/json'
            },
            data: postData
        }).success(function(data, status, headers, config) {
            $scope.cIconUrl = null
            robinDialog.alert('Tenant has been updated.');
            // reset status
            $scope.saving = false;
            $scope.tenantChanged = false
            $scope.resetForm();
            // if icon is changed, reload
            if ($scope.iconChanged) {
                $scope.tenant.iconUrl = null;
                $scope.extTimestamp = '?' + (new Date().getTime());
                $scope.loadTenant()
            }
            $scope.iconChanged = false
        }).error(function(data, status, headers, config) {
            robinDialog.alert('server error, please try again.');
            $scope.saving = false;
        });
    };

    /*
     * called after selecting an icon to upload.
     * server will upload the icon to a temp location, and will return the location
     */
    $scope.submitIconForm = function(input) {
        $scope.actionTip = $filter('translate')("tenantedit_updating") + $filter('translate')($scope.msg);
        $scope.saving = true;
        $scope.$digest();
        var progressSelector = '.progress-upload-icon';
        robinCommon.submitFilesForm(
            '#edit-tenant-form', // form selector
            progressSelector,
            // url for sending the form
            '/mw/upload/icon/' + $rootScope.tenantId + '/tenant',
            // on success
            function(data) {
                $(progressSelector).hide();
                $scope.cIconUrl = data.tempKey;
                $scope.saving = false;
                $scope.tenantChanged = true
                $scope.iconChanged = true
                $scope.$digest();
            },
            // on error
            function(data) {
                $(progressSelector).hide();
                robinDialog.alert('Unable to upload icon, error: ' + data.statusText);
                $scope.saving = false;
            },
            // on before send
            function() {
                $(progressSelector).show();
            },
            // on during upload
            function(e) {
                if (e.lengthComputable){
                    $('progress').attr({
                        value: e.loaded,
                        max: e.total
                    });
                }
            }
        );
    };

    $scope.loadTenant = function() {
        // load tenant data
        $http({
            method: 'GET',
            url: '/mw/migrate/tenant/' + $rootScope.tenantId,
            headers: {
                'Content-Type': 'application/json'
            }
        }).success(function(data, status) {
            $scope.tenant = data
        }).error(function(data, status, headers, config) {
            robinDialog.alert('server error, please try again.');
        });
    }

    $scope.resetForm();
    $scope.loadTenant();
}]);

