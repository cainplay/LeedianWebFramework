robinApp.controller('CtrlSignup', [
    "$scope", "$rootScope", "AuthService",
    function ($scope, $rootScope, AuthService) {
    $scope.errorMessage = null;
    $scope.posting = false;
    $scope.formData = {
        email: null,
        password1: null,
        password2: null
    };

    $scope.onSubmit = function () {
        if ($scope.formData.password1 != $scope.formData.password2) {
            // show error
            $scope.errorMessage = "Password does not match! Please correct.";
            return;
        }
        $scope.errorMessage = null;
        $scope.posting = true;

        var credentials = {
            email : $scope.formData.email,
            password : $scope.formData.password1
        };
        AuthService.signup(credentials).success(function (data) {
            // return data contains "er" code
            // -1 mean success
            $scope.posting = false;
            if (data.er == -1) {
                // robinDialog.showConfirmation(null, 'Successful sign up - please contact your admin to assign a role for you.', 'OK', null, function() {
                //     robinDialog.dismissDialog();
                //     $rootScope.redirectToLogin(true);
                // }, true);
                $rootScope.redirectToLogin(true);
            } else {
                // data.erMessage contains error
                robinDialog.alert(data.erMessage);
            }
        }).error(function (data, status, headers, config) {
            $scope.posting = false;
            robinDialog.alert('Server error, please try again.');
        });
    };

    $scope.resetPassword = function (token) {
        if ($scope.formData.password1 != $scope.formData.password2) {
            // show error
            $scope.errorMessage = "Password does not match.";
            return;
        }
        var postData = {
            token: token,
            password: $scope.formData.password1
        };
        $scope.errorMessage = null;
        $scope.posting = true;
        $http({
            method: 'POST',
            url: '/mw/password/reset',
            headers: {
                'Content-Type': 'application/json'
            },
            data: postData
        }).success(function (data) {
            // return data contains "er" code
            // -1 mean success
            $scope.posting = false;
            if (data.er == -1) {
                robinDialog.alert("Password reset successful.\nPlease login with new password.");
            } else {
                // data.erMessage contains error
                robinDialog.alert(data.erMessage);
            }
        }).error(function (data, status, headers, config) {
            $scope.posting = false;
            robinDialog.alert('Server error, please try again.');
        });
    };
}]);
