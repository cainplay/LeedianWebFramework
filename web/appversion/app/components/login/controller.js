robinApp.controller('CtrlLogin',
    ["$rootScope", '$scope', '$http', "AuthService", "$location",
    function($rootScope, $scope, $http, AuthService, $location) {
    $scope.providerLoginUrl = "";
    $scope.providerLoginError = null;

    $scope.loading = false;
    $scope.formData = {
        email : null,
        password : null,
        rememberMe : false
    };

    $scope.onSubmit = function() {
        $scope.loading = true;

        var credentials = {
            email : $scope.formData.email,
            password : $scope.formData.password,
            rememberMe : $scope.formData.rememberMe
        };
        AuthService.login(credentials).then(
            function(res) {
                // return data contains "er" code
                // -1 mean success
                $scope.loading = false;

                if (res.data.er != -1) {
                    robinDialog.alert(res.data.erMessage);
                }
            },
            function(res) {
                $scope.loading = false;

                robinDialog.alert('Server error, please try again.');
            }
        );
    };

    $scope.forgotPassword = function() {
        $scope.loading = true;

        var url = '/mw/password/' + $scope.formData.email;
        $http({
            method : 'GET',
            url : url
        }).then(function(data) {
            // return data contains "er" code
            // -1 mean success
            $scope.loading = false;
            if (data.er == -1) {
                robinDialog.alert("Reset email sent, please check!");
            } else {
                // data.erMessage contains error
                robinDialog.alert(data.erMessage);
            }
        }).error(function(data, status, headers, config) {
            $scope.loading = false;
            robinDialog.alert('Server error, please try again.');
        });
    }
}]);
