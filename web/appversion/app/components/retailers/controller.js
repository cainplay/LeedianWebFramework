robinApp.controller('CtrlRetailers', ['$scope', 'RetailerIO', function ($scope, RetailerIO) {

    // declaring vars used by angular
    $scope.isPageDataLoaded = false;
    $scope.loading = false;
    $scope.editing = null;
    $scope.retailers = null;
    $scope.selectedRetailerIndex = null;
    $scope.selectedRetailer = null;
    $scope.editedRetailer = {};
    $scope.title = "";
    $scope.isDeleting = false;

    var editingMode = { New: 1, Existing: 2 };

    $scope.initRetailersList = function() {
        RetailerIO.getRetailers().success(function(data, status, headers, config) {
            $scope.retailers = [];
            $.each(data, function(index, retailer) {
                $scope.retailers.push({
                    id: retailer.id,
                    name: retailer.name,
                    url: retailer.url
                });
            });
            $scope.sortRetailersList();
            $scope.isPageDataLoaded = true;
        }).error(function(data, status, headers, config) {
            robinDialog.alert('Server error, please try again.');
        });
    };

    function sortByName(a, b){
      var aName = a.name.toLowerCase();
      var bName = b.name.toLowerCase();
      return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
    }

    $scope.sortRetailersList = function() {
        $scope.retailers.sort(sortByName)
    };

    /*
     * selection handlers
     */

    $scope.onRetailerSelected = function(index, scroll) {
        $scope.selectedRetailerIndex = index;
        $scope.selectedRetailer = $scope.retailers[index];
        if (scroll)
            $("html, body").animate({ scrollTop: 0 }, "slow");
    };

    $scope.clearSelectedRetailer = function() {
        $scope.selectedRetailerIndex = null;
        $scope.selectedRetailer = null;
    };

    /*
     * editing action handlers
     */

    $scope.editNewRetailer = function() {
        $scope.editedRetailer = {};
        $scope.editing = editingMode.New;
    };

    function copyRetailer(source, target) {
        target.id = source.id;
        target.name = source.name;
        target.url = source.url;
    }

    $scope.editRetailer = function() {
        copyRetailer($scope.selectedRetailer, $scope.editedRetailer);
        $scope.editing = editingMode.Existing;
    };

    $scope.saveEditedRetailer = function() {
        if ($scope.editedRetailer.name) {
            var isNew = $scope.editing == editingMode.New;
            $scope.loading = true;

            var request = isNew ? RetailerIO.createRetailer($scope.editedRetailer) :
                RetailerIO.updateRetailer($scope.editedRetailer, $scope.editedRetailer.id);

            request.success(function(data, status, headers, config) {
                if (isNew) {
                    $scope.retailers.push(data);
                    $scope.sortRetailersList();
/*
                    $.each($scope.retailers, function(index, retailer) {
                        if (retailer.id = data.id)
                            $scope.onRetailerSelected(index);
                    });
*/
                } else {
                    copyRetailer($scope.editedRetailer, $scope.selectedRetailer);
                }
                $scope.loading = false;
                robinDialog.alert('Retailer "' + $scope.editedRetailer.name + '" has been saved.');
                $scope.cancelEditRetailer();
            }).error(function(data, status, headers, config) {
                $scope.loading = false;
                robinDialog.alert('Server error, please try again.');
            });
        } else {
            robinDialog.alert('Please provide retailer name.');
        }
    };

    $scope.cancelEditRetailer = function() {
        $scope.editedRetailer = {};
        $scope.editing = null;
    };

    $scope.deleteSelectedRetailer = function() {
        var selIndex = $scope.selectedRetailerIndex;
        robinDialog.showConfirmation(
            null, 'Are you sure you want to delete "' + $scope.selectedRetailer.name + '"?', 'Delete', 'Cancel',
            function() {
                robinDialog.dismissDialog();
                $scope.isDeleting = true;
                RetailerIO.deleteRetailer($scope.retailers[selIndex].id).success(function(data, status, headers, config) {
                    $scope.isDeleting = false;
                    //robinDialog.alert('Retailer "' + $scope.selectedRetailer.name + '" has been deleted.');
                    $scope.retailers.splice(selIndex, 1);
                    $scope.clearSelectedRetailer();
                    //$scope.$digest();
                }).error(function(data, status, headers, config) {
                    $scope.isDeleting = false;
                    robinDialog.alert('Server error, please try again.');
                });
            }
        );
    };

    /*
     * c'tor
     */
    $scope.initRetailersList();

}]);
