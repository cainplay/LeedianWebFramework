robinApp.controller('CtrlAttributes', ['$rootScope', '$scope', '$http', 'AttributeIO',
    function ($rootScope, $scope, $http, AttributeIO) {

    // globals
    $scope.loading = true;

    // attributes list
    $scope.attributes = null;
    $scope.selectedAttribute = null;
    $scope.newOrUpdate = false;

    // sorting parameters
    $scope.predicate = 'name';
    $scope.reverse = false;

    $scope.formData = {
        passwordUser: null
    };

    var CONST_ATTRIBUTE_GENERATED_ID = 'will be generated on save';

    $scope.getAllAttributes = function() {
        $scope.loading = true;
        AttributeIO.getAttributes().success(function(data) {
            if (data.length == 0) {
                robinDialog.alert('no attributes found.');
            } else {
                $scope.attributes = data;
                // convert references for form easier for angular
                /*$.each($scope.products, function(index, product) {
                    product.vReferences = [];
                    $.each(product.references, function(name, value) {
                        product.vReferences.push({"name": name, "value": value});
                    });
                });*/
            }
            $scope.loading = false;
        }).error(function(data, status, headers, config) {
            robinDialog.alert('server error, please try again.'); // TODO: improve this error handling
            $scope.loading = false;
        });
    };

    $scope.showPassword = function(formAttributes, formAttributeValues) {
      return formAttributes.$dirty || formAttributeValues.$dirty
    };

    $scope.onAttributeSelected = function(formAttributes, formAttributeValues, attributeIndex) {
        if (!$scope.loading) {
            $scope.selectedAttributeIndex = attributeIndex;
            $scope.selectedAttribute = angular.copy($scope.attributes[attributeIndex]);
            formAttributeValues.$setPristine();

            if(!$scope.isNewAttribute(attributeIndex)) {
                $scope.removeAttribute(formAttributes)
            }
        }
    };

    $scope.isNewAttribute = function(attrOffset) {
      return attrOffset == $scope.attributes.length - 1 && $scope.newOrUpdate
    };

    $scope.isNewAttributeId = function(attrId) {
        return attrId == $scope.attributes[$scope.attributes.length - 1].id && $scope.newOrUpdate
    };

    // click handlers

    $scope.removeAttributeValue = function(form, refIndex) {
        $scope.selectedAttribute.valuesIds.splice(refIndex, 1);
        form.$setDirty(true);
    };

    $scope.addAttributeValue = function(form) {
        $scope.selectedAttribute.valuesIds.push({
            name: '',
            id: CONST_ATTRIBUTE_GENERATED_ID
        });
        form.$setDirty(true);
    };


    $scope.removeAttribute = function(form) {
        if ($scope.newOrUpdate) {
            $scope.newOrUpdate = false;
            $scope.attributes.splice($scope.attributes.length - 1, 1);
            form.$setPristine();
        }
    };

    $scope.addAttribute = function(form) {
        $scope.newOrUpdate = true;
        $scope.attributes.push({
            id: '',
            name: ''
        });
        $scope.selectedAttributeIndex = $scope.attributes.length - 1;
        $scope.attributes[$scope.selectedAttributeIndex].valuesIds = [];
        $scope.selectedAttribute = angular.copy($scope.attributes[$scope.selectedAttributeIndex]);
        form.$setDirty(true);
    };


    $scope.saveAttributeValues = function(form) {
        if (form.$valid) {
            $scope.loading = true;

            /*for(var i = 0; i < $scope.selectedAttribute.valuesIds.length; ++i) {
                if ($scope.selectedAttribute.valuesIds[i].id == CONST_ATTRIBUTE_GENERATED_ID)
                    delete $scope.selectedAttribute.valuesIds[i].id;
            }*/
            
            var attributeType = "";
            if($scope.selectedAttribute.attributeType !== null){
            	attributeType = $scope.selectedAttribute.attributeType;
            }

            data = {
                attributeName: $scope.selectedAttribute.name,
                attributeType: attributeType,
                values: $scope.selectedAttribute.valuesIds
            };

            for(var i = 0; i < data.values.length; ++i) {
                if (data.values[i].id == CONST_ATTRIBUTE_GENERATED_ID)
                    delete data.values[i].id;
            }

            AttributeIO.updatesValues(data, $scope.selectedAttribute.id, $scope.formData.passwordUser).success(function (data) {
                robinDialog.alert('Updated successfully.');
                $scope.attributes[$scope.selectedAttributeIndex].valuesIds = angular.copy($scope.selectedAttribute.valuesIds);
                form.$setPristine();
                $scope.loading = false;
            }).error(function (data, status, headers, config) {
                if (status == 401) robinDialog.alert(data);
                else robinDialog.alert('server error, please try again.');
                $scope.loading = false;
                form.$setDirty(true);
                $scope.$digest();
            });

        } else {
            robinDialog.alert('invalid form fields.');
        }
    };

    $scope.saveAttributes = function(form) {
        if (form.$valid) {
            $scope.loading = true;

            var newAttribute = $scope.attributes[$scope.attributes.length - 1];
            var newAttributeType = form["isSizeTypeAttribute"].$modelValue ? "sizeType" : "";
            data = {
                attributeName: newAttribute.name,
                attributeType: newAttributeType
            };

            AttributeIO.createAttribute(data, newAttribute.id, $scope.formData.passwordUser).success(function (data) {
                robinDialog.alert('Updated successfully.');
                $scope.attributes[$scope.attributes.length - 1].id = data.substring(1, data.length - 1);
                $scope.attributes[$scope.attributes.length - 1].attributeType = newAttributeType;
                $scope.selectedAttribute.attributeType = newAttributeType;
                form.$setPristine();
                $scope.newOrUpdate = false;
                $scope.loading = false;
            }).error(function (data, status, headers, config) {
                if (status == 401) robinDialog.alert(data);
                else robinDialog.alert('server error, please try again.');
                $scope.loading = false;
                form.$setDirty(true);
                $scope.$digest();
            });

        } else {
            robinDialog.alert('invalid form fields.');
        }
    };

    $scope.cancelChanges = function(form) {
        $scope.selectedAttribute = angular.copy($scope.attributes[$scope.selectedAttributeIndex]);
        form.$setPristine();
    };

    // c'tor
    $scope.getAllAttributes();

}]);