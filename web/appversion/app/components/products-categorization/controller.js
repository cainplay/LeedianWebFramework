robinApp.controller('CtrlProductsCategorization',[
    '$rootScope',
    '$scope',
    '$window',
    '$route',
    '$q',
    '$location',
    'ProductIO',
    'FamilyIO',
    'TenantIO',
    'StorageService',
    'LocaleService',
    function ($rootScope, $scope, $window, $route, $q, $location, ProductIO, FamilyIO, TenantIO, StorageService, LocaleService) {

    // globals
    $scope.loading = true;

    $scope.editModes = {
        Categories: 0,
        Families: 1,
        Attributes: 2,
        Status: 3,
        Retailer: 4
    };
    $scope.currentEditMode = $scope.editModes.Categories;
    $scope.availableBranches = [];
    $scope.currentAttribute = null;
    $scope.currentAttributeValue = null;

    $scope.currentStatus = null;
    $scope.currentRetailer = null;

    $scope.showBranch = false;

    // Init search parameters.
    $scope.searchParams = {
        freeText: null,
        category: null,
        brand: null,
        branch: null
    };

    // Get branch from parameter.
    $scope.searchParams.branch = $location.search().branch || '';

    /*
     * products search
     */

    // basic search params
    $scope.productsPerPage = {
        selected: 10,
        options: [10, 20, 50, 100]
    };
    $scope.currentPage = 0;
    // items on page functionality
    $scope.onItemsOnPageChanged = function() {
        $scope.currentPage = 0;
        $scope.refreshSearchResults(true);
    };

    // init page data
    $scope.availableCategories = null;
    $scope.availableBrands = null;

    $scope.populateData = function() {
        $q.all([
            populateCategorizationData(),
            populateTenantTree(),
            populateFamilyTree()
        ]).then(function(results) {
            $scope.loading = false;
        }, function(error) {
            $scope.loading = false;
            robinDialog.alert('server error, please try again.');
        });
    };

    var populateCategorizationData = function() {
        var initDataValues = function(data) {
            // populate categories
            $scope.availableCategories = data.categories;
            $scope.availableCategories.unshift({id: '', displayName: ''})
            $scope.searchParams.category = $scope.availableCategories[0];
            // populate brands
            $scope.availableBrands = data.brands.slice(0);
            $scope.availableBrands.unshift({id: '', name: ''});
            $scope.availableBrands.sort(LocaleService.localeStringSort(undefined, function(item) {return item.name;}));
            $scope.searchParams.brand = $scope.availableBrands[0];
            // populate categories tree
            var treeData = data.categoriesTree;
            initTreeDataAttrs(treeData); // attach needed attrs to raw data
            initTree($scope.cTreeSelector, treeData);
            // attributes
            $scope.availableAttributes = data.attributes;
            $scope.availableAttributes.sort(LocaleService.localeStringSort(undefined, function(item) {return item.name;}));
            // statuses
            $scope.availableStatuses = data.statuses;
            // retailers
            $scope.availableRetailers = data.retailers.slice(0);
            $scope.availableRetailers.sort(LocaleService.localeStringSort(undefined, function(item) {return item.name;}));

            $scope.brands = data.brands;
            $scope.retailers = data.retailers;
            $scope.categoryTree = data.categoriesTree;
        }

        // Create cache key. The key is tenant id + branch name.
        return ProductIO.getProductCategorizationData($scope.searchParams.branch).then(function(data) {
            initDataValues(data);
            return data;
        });
    };

    var populateTenantTree = function() {
        return TenantIO.getTenantBranch().then(function(treeData) {
            if (treeData.children && treeData.children.length > 0) {
                $scope.availableBranches = treeData.children.slice(0);
                $scope.availableBranches.unshift('');
                $scope.availableBranches.sort(LocaleService.localeStringSort());
                $scope.showBranch = true;
            }
            return treeData;
        });
    };

    var populateFamilyTree = function() {
        return FamilyIO.getFamilyTree().then(function(data) {
            initFTreeDataAttrs(data); // attach needed attrs to raw data
            initFTree($scope.fTreeSelector, data);
            return data;
        });
    };

    // products list
    $scope.products = [];
    $scope.selectedProductsCount = 0;

    var getBrand = function(product){
        if(product.brands.length <= 0)
            return "";
        for (i = 0; i < $scope.brands.length; ++i) {
            if ($scope.brands[i].id == product.brands[0]) {
                return $scope.brands[i].name;
            }
        }
        return "";
    };

    var getRetailer = function(product) {
        if(!product.retailers)
            return "";
        if(product.retailers.length <= 0)
            return "";
        for(i=0; i < $scope.retailers.length; i++){
            if ($scope.retailers[i].id == product.retailers[0].id){
                return $scope.retailers[i].name;
            }
        }
        return "";
    };

    var getRetailerPrice = function(product){
        if(!product.retailers)
            return "";
        if(product.retailers.length <= 0)
            return "";
        if(!product.retailers[0].price)
            return "";
        return parseFloat(product.retailers[0].price).toFixed(2);
    };

    var getStyle = function(product){
        if (!product.attributes)
            return "";
        var styleStrings = "";
        product.attributes.forEach(function(attr){
            if(attr.name === "Styles"){
                if(attr.valuesIds2 && attr.valuesIds2.length > 0){
                    styleStrings = attr.valuesIds2[0].name;
                }
            }
        });
        return styleStrings;
    };

    var getScalable = function(product){
        if (!product.attributes)
            return "";
        var isScalableStrings = "";
        product.attributes.forEach(function(attr){
            if(attr.name === "isScalable"){
                if(attr.valuesIds2 && attr.valuesIds2.length > 0){
                    isScalableStrings = attr.valuesIds2[0].name;
                }
            }
        });
        return isScalableStrings;
    };

    var getCouponType = function(product){
        if (!product.attributes)
            return "";
         var couponStrings = "";
         product.attributes.forEach(function(attr){
             if(attr.name === "couponType"){
                 if(attr.valuesIds2 && attr.valuesIds2.length > 0){
                     couponStrings = attr.valuesIds2[0].name;
                 }
             }
         });
         return couponStrings;
    };

    var getDefaultHeight = function(product) {
        if (!product.attributes)
            return "";
        var defaultHeightStrings = "";
        product.attributes.forEach(function(attr){
            if(attr.name === "defaultHeight"){
                if(attr.free && attr.free.length > 0){
                    defaultHeightStrings = attr.free[0];
                }
            }
        });
        return defaultHeightStrings;
    };

    var getCategoryName = function(node, parents, id) {
        var parentsStr = parents + "/" + node.title;
        var categoryName = "";
        if(node.id === id){
            categoryName = parentsStr;
            return categoryName;
        }
        else if(node.children.length > 0){
            node.children.forEach(function(childNode){
                var tempStr = getCategoryName(childNode, parentsStr, id);
                if(tempStr !== "")
                    categoryName = tempStr;
            });
        }
        return categoryName;
    };

    var getCategory = function(product) {
        if (product.categoryIds.length == 0)
            return "";
        var categoryStrings = "";
        $scope.categoryTree.forEach(function(node){
            var tempStr = getCategoryName(node, "", product.categoryIds[0]);
            if(tempStr !== "")
                categoryStrings = tempStr;
        });
        return categoryStrings;
    };

    $scope.refreshSearchResults = function(isRefreshPagination) {
        $scope.loading = true;
        var categoryIds = $scope.searchParams.category.id ? [$scope.searchParams.category.id] : undefined;
        var brandIds = $scope.searchParams.brand.id ? [$scope.searchParams.brand.id] : undefined;
        var param = {
            offset: $scope.currentPage * $scope.productsPerPage.selected,
            limit: $scope.productsPerPage.selected,
            status: -1,
            categoryIds: categoryIds,
            brandIds: brandIds,
            freeText: $scope.searchParams.freeText,
            branch: $scope.searchParams.branch
        }

        ProductIO.searchProducts(param).success(function(data) {
            $scope.products = data.products;
            $scope.totalProductsCount = data.total;
            $scope.selectedProductsCount = 0;
            $scope.initProducts();
            if (isRefreshPagination)
                $scope.refreshPagination($scope.totalProductsCount);
            $.each($scope.products, function(index, product) {
                product.postStatus = "";
                product.SKU = "";
                product.brand = getBrand(product);
                product.retailer = getRetailer(product);
                product.retailerPrice = getRetailerPrice(product);
                product.style = getStyle(product);
                product.isScalable = getScalable(product);
                product.couponType = getCouponType(product);
                product.defaultHeight = getDefaultHeight(product);
                product.category = getCategory(product);
                $.each(product.references, function(name, value) {
                    if(name == "others"){
                        if(value.externalId !== undefined)
                            product.SKU = value.externalId;
                        switch(value.postProcessingStatus){
                        case "1":
                            product.postStatus = "Success";
                            break;
                        case "2":
                            product.postStatus = "Failure";
                            break;
                        default:
                            break;
                        }
                    }
                });
            });
            $scope.toggleTree(false);
            $scope.toggleFTree(false);
            //$scope.updateCategoriesSelection();
            $scope.onCancelButtonClicked();
            $scope.loading = false;
        }).error(function(data, status, headers, config) {
            robinDialog.alert('server error, please try again.'); // TODO: improve this error handling
            $scope.loading = false;
        });
    };

    // search button
    $scope.onSearchButtonClicked = function() {
        $scope.currentPage = 0;
        $scope.refreshSearchResults(true);
    };
    var getContentType = function(product) {
        if (!product.attributes) return "";
        var contentTypes = product.attributes.filter(function(attr) {if (attr.name === "ContentType") return true;});
        var typeStrings = "";
        contentTypes.forEach(function(ctype) {
            if (ctype.valuesIds2 && ctype.valuesIds2.length > 0) {
                typeStrings = ctype.valuesIds2[0].name;
            }
        });
        return typeStrings;
    };
    // init products ui data
    $scope.initProducts = function() {
        $.each($scope.products, function(index, product) {
            product.typeStrings = getContentType(product);
            product.updatedCategoryIds = product.categoryIds.slice();
            product.updatedFamilyIds = product.familyIds ? product.familyIds.slice() : [];
            product.isSelected = false;
        });
    };

    // sets a product as selected, called both from the ui click handler and upon 'toggle all' action
    $scope.onProductChecked = function(product, check) {
        if (!$scope.loading) {
            product.isSelected = check ? check.isOn : !product.isSelected;
            $scope.selectedProductsCount += product.isSelected ? 1 : -1;
            // enable / disable tree according to selection
            $scope.toggleTree($scope.selectedProductsCount);
            $scope.toggleFTree($scope.selectedProductsCount);

            $scope.updateCategoriesSelection();
            $scope.updateFamiliesSelection();
            $scope.updateAttributesSelection();
        }
    };

    $scope.attributePressed = function() {
        $scope.updateAttributesSelection();
    };

    // all products select/deselect
    $scope.onToggleAllProductsSelection = function(toggle) {
        var check = {isOn: toggle};
        $.each($scope.products, function(index, product) {
            if (product.isSelected != toggle)
                $scope.onProductChecked(product, check);
        });
    };

    // pagination
    $scope.refreshPagination = function(totalProducts) {
        if ($scope.products.length > 0) {
            var totalPages = Math.ceil(totalProducts / $scope.productsPerPage.selected);
            var twbsParams = {
                totalPages: totalPages,
                visiblePages: Math.min(5, totalPages),
                //startPage: 1,
                onPageClick: function(event, page) {
                    $scope.currentPage = page - 1;
                    $scope.refreshSearchResults(false);
                }
            };
            $('#products-pagination').empty();
            $('#products-pagination').removeData("twbs-pagination");
            $('#products-pagination').unbind("page");
            $('#products-pagination').twbsPagination(twbsParams);
        }
    };

    /*
     * categories tree
     */

    $scope.cTreeSelector = '.categories-tree';
    $scope.cTreeObject = null;
    $scope.cTree = null;
    $scope.cTreeRoot = null;
    $scope.isTreeDirty = false;

    // add extra tree attributes to raw data
    function initTreeDataAttrs(nodesList) {
        $(nodesList).each(function(index, node) {
            node.count = 0; // how many products are tagged with this category
            node.name = node.title; // save for later use
            if (node.children && node.children.length > 0) {
                //node.isFolder = true;
                node.isParent = true; // mark for later use
                //node.hideCheckbox = true;
                node.expand = false;
                //node.unselectable = true;
                initTreeDataAttrs(node.children);
            }
        });
    }

    // init dynatree with data
    function initTree(rootSelector, data) {

        $(rootSelector).dynatree({
            imagePath: " ",
            onSelect: function(flag, node) {
                if (this.phase == 'userEvent') { // prevent actions from external events
                    $scope.updateSelectedProductsCategories(node.data.id, node.bSelected);
                    $scope.isTreeDirty = true;
                    $scope.$digest();
                }
            },
            //persist: true,
            checkbox: true,
            selectMode: 3,
            clickFolderMode: 1,
            children: data
        });

        // init scope vars
        $scope.cTreeObject = $(rootSelector);
        $scope.cTree = $(rootSelector).dynatree("getTree");
        $scope.cTreeRoot = $(rootSelector).dynatree("getRoot");

        // disable tree
        $scope.cTree.disable(); // TODO: refactor this mechanism

    }

    $scope.toggleTree = function(enable) {
        $scope.cTreeObject.dynatree(enable > 0 ? "enable" : "disable");
    };

    /*
     * families tree
     */

    $scope.fTreeSelector = '.families-tree';
    $scope.fTreeObject = null;
    $scope.fTree = null;
    $scope.fTreeRoot = null;
    $scope.isFTreeDirty = false;

    // add extra tree attributes to raw data
    function initFTreeDataAttrs(nodesList) {
        $(nodesList).each(function(index, node) {
            node.count = 0; // how many products are tagged with this category
            node.name = node.title; // save for later use
            if (node.children && node.children.length > 0) {
                //node.isFolder = true;
                node.isParent = true; // mark for later use
                //node.hideCheckbox = true;
                node.expand = false;
                //node.unselectable = true;
                initFTreeDataAttrs(node.children);
            }
        });
    }

    // init dynatree with data
    function initFTree(rootSelector, data) {

        $(rootSelector).dynatree({
            onSelect: function(flag, node) {
                if (this.phase == 'userEvent') { // prevent actions from external events
                    $scope.updateSelectedProductsFamilies(node.data.id, node.bSelected);
                    $scope.isFTreeDirty = true;
                    $scope.$digest();
                }
            },
            //persist: true,
            checkbox: true,
            selectMode: 3,
            clickFolderMode: 1,
            children: data
        });

        // init scope vars
        $scope.fTreeObject = $(rootSelector);
        $scope.fTree = $(rootSelector).dynatree("getTree");
        $scope.fTreeRoot = $(rootSelector).dynatree("getRoot");

        // disable tree
        $scope.fTree.disable(); // TODO: refactor this mechanism

    }

    $scope.toggleFTree = function(enable) {
        $scope.fTreeObject.dynatree(enable > 0 ? "enable" : "disable");
    };


    /*
     * action buttons
     */
    $scope.onSaveButtonClicked = function() {

        $scope.loading = true;

        switch ($scope.currentEditMode) {
            case $scope.editModes.Categories:
                $scope.onSaveButtonClickedCategories();
                break;
            case $scope.editModes.Families:
                $scope.onSaveButtonClickedFamilies();
                break;
            case $scope.editModes.Attributes:
                $scope.onSaveButtonClickedAttributes();
                break;
            case $scope.editModes.Status:
                $scope.onSaveButtonClickedStatuses();
                break;
            case $scope.editModes.Retailer:
                $scope.onSaveButtonClickedRetailers();
                break;
        }

    };

    $scope.onSaveButtonClickedCategories = function() {
        // prepare data to send
        var updatedProducts = {};
        $.each($scope.products, function(index, product) {
            if (product.isSelected) {
                updatedProducts[product.id] = product.updatedCategoryIds;
            }
        });

        ProductIO.updateProductCategories(updatedProducts, $scope.searchParams.branch).success(function(data) {
            // set updated categories as categories
            $.each($scope.products, function(index, product) {
                if (product.isSelected) {
                    product.categoryIds = product.updatedCategoryIds.slice();
                }
            });
            robinDialog.alert('Products categories has been updated successfully.');
            $scope.isTreeDirty = false;
            $scope.loading = false;
        }).error(function(data, status, headers, config) {
            robinDialog.alert('server error, please try again.'); // TODO: improve this error handling
            $scope.loading = false;
        });
    };

    $scope.onSaveButtonClickedFamilies = function() {
        // prepare data to send
        var updatedProducts = {};
        $.each($scope.products, function(index, product) {
            if (product.isSelected) {
                updatedProducts[product.id] = product.updatedFamilyIds;
            }
        });

       // call middleware
       ProductIO.updateProductFamilies(updatedProducts).success(function(data) {
           // set updated families as families
           $.each($scope.products, function(index, product) {
               if (product.isSelected) {
                   product.familyIds = product.updatedFamilyIds ? product.updatedFamilyIds.slice() : [];
               }
           });
           robinDialog.alert('Products families has been updated successfully.');
           $scope.isTreeDirty = false;
           $scope.loading = false;
       }).error(function(data, status, headers, config) {
           robinDialog.alert('server error, please try again.'); // TODO: improve this error handling
           $scope.loading = false;
       });
    };

    $scope.onSaveButtonClickedAttributes = function() {

        var patchProducts = [];

        $.each($scope.products, function(index, product) {
            if (product.isSelected) {

                var updatedAttributes = [];
                var found = false;

                for (var i = 0; i < product.attributes.length; ++i) {
                    if (product.attributes[i].id == $scope.currentAttribute.id) {
                        found = true;
                        updatedAttributes.push({
                            id: product.attributes[i].id,
                            valuesIds: [$scope.currentAttributeValue.id],
                            free: product.attributes[i].free
                        });
                    } else {
                        updatedAttributes.push({
                            id: product.attributes[i].id,
                            valuesIds: product.attributes[i].valuesIds,
                            free: product.attributes[i].free
                        });
                    }
                }

                if (!found) {
                    updatedAttributes.push({
                        id: $scope.currentAttribute.id,
                        valuesIds: [$scope.currentAttributeValue.id]
                    });
                }

                patchProducts.push(
                    {
                        id: product.id,
                        attributes: updatedAttributes
                    }
                );
            }
        });

        ProductIO.patchProducts(patchProducts, undefined, $scope.searchParams.branch).success(function(data) {
            // TODO: need to add attributes to the products
            $.each($scope.products, function(index, product) {
                if (product.isSelected) {
                    for (var i = 0; i < patchProducts.length; ++i) {
                        if (patchProducts[i].id == product.id) {
                            product.attributes = patchProducts[i].attributes
                        }
                    }
                }
            });
            robinDialog.alert('Products attributes has been updated successfully.');
            $scope.loading = false;
        }).error(function(data, status, headers, config) {
            robinDialog.alert('server error, please try again.'); // TODO: improve this error handling
            $scope.loading = false;
        });
    };

    $scope.onSaveButtonClickedStatuses = function() {

        var selectedStatus = $scope.currentStatus.description;
        var selectedStatusId = $scope.currentStatus.id;

        var patchProducts = [];
        $.each($scope.products, function(index, product) {
            if (product.isSelected && product.status != selectedStatus) {
                patchProducts.push(
                    {
                        id: product.id,
                        status: selectedStatusId
                    }
                );
            }
        });

        ProductIO.patchProducts(patchProducts, undefined, $scope.searchParams.branch).success(function(data) {
            // update products in the ui
            $.each($scope.products, function(index, product) {
                if (product.isSelected && product.status != selectedStatus) {
                    product.status = selectedStatus;
                }
            });

            robinDialog.alert('Products status has been updated successfully.');
            $scope.loading = false;

        }).error(function(data, status, headers, config) {
            robinDialog.alert('server error, please try again.'); // TODO: improve this error handling
            $scope.loading = false;
        });

    };

    $scope.onSaveButtonClickedRetailers = function () {
        var selectedRetailer = $scope.currentRetailer;

        var patchProducts = [];
        $.each($scope.products, function (index, product) {
            if (product.isSelected) {
                var updatedRetailers = [];
                updatedRetailers.push(selectedRetailer);
                patchProducts.push(
                    {
                        id: product.id,
                        retailers: updatedRetailers
                    }
                );
            }
        });

        ProductIO.patchProducts(patchProducts, undefined, $scope.searchParams.branch).success(function (data) {
            robinDialog.alert('Products retailer has been updated successfully.');
            $scope.loading = false;

        }).error(function (data, status, headers, config) {
            robinDialog.alert('server error, please try again.'); // TODO: improve this error handling
            $scope.loading = false;
        });
    }

    $scope.onCancelButtonClicked = function() {
        // revert categories selection
        $.each($scope.products, function(index, product) {
            if (product.isSelected) {
                product.updatedCategoryIds = product.categoryIds.slice();
                product.updatedFamilyIds = product.familyIds ? product.familyIds.slice() : [];
            }
        });
        // revert tree selection
        $scope.updateCategoriesSelection();
        $scope.updateFamiliesSelection();
        $scope.isTreeDirty = false;

        $scope.updateAttributesSelection();

        //$scope.refreshSearchResults();
    };

    /*
     * sync products list and categories tree
     */

    $scope.updateCategoriesSelection = function() {

        var mapCategoryProductsCount = $scope.createMapCategoryProductsCount();
        //tree.renderInvisibleNodes();

        $scope.updateTreeSelection(mapCategoryProductsCount);
        $scope.updateTreeUi(mapCategoryProductsCount);

    };

    /*
     * sync products list and families tree
     */

    $scope.updateFamiliesSelection = function() {

        var mapFamilyProductsCount = $scope.createMapFamilyProductsCount();
        //tree.renderInvisibleNodes();

        $scope.updateFTreeSelection(mapFamilyProductsCount);
        $scope.updateFTreeUi(mapFamilyProductsCount);

    };

    $scope.updateAttributesSelection = function() {

        var agreedValue = "";
        var agreedValueSet = false;

        $.each($scope.products, function(index, product) {
            if (product.isSelected) {
                if (!agreedValueSet) {
                    agreedValueSet = true;
                    agreedValue = getAttributeValueOfProduct(product, $scope.currentAttribute);
                }
                else {
                    if (agreedValue != getAttributeValueOfProduct(product, $scope.currentAttribute))
                        agreedValue = "";
                }
            }
        });

        if (agreedValue != "") {

            for (var i = 0; i < $scope.currentAttribute.valuesIds.length; ++i) {
                if ($scope.currentAttribute.valuesIds[i].id == agreedValue)
                    $scope.currentAttributeValue = $scope.currentAttribute.valuesIds[i];
            }
        } else {
            $scope.currentAttributeValue = null;
        }

    };

    $scope.isSaveDisabled = function() {
        return $scope.loading || $scope.selectedProductsCount == 0 ||
            ($scope.currentEditMode == $scope.editModes.Categories && !$scope.isTreeDirty) ||
            ($scope.currentEditMode == $scope.editModes.Status && $scope.currentStatus == null)
    };

    function getAttributeValueOfProduct(product, attribute) {

        var attributeValue = "";

        if (attribute != null) {
            for (var i = 0; i < product.attributes.length; ++i) {
                if (product.attributes[i].id == attribute.id) {
                    attributeValue = product.attributes[i].valuesIds[0];
                }
            }
        }

        return attributeValue;
    }

    // for a selected category, add it or remove it from the products' categories list
    $scope.updateSelectedProductsCategories = function(selectedCategoryId, isChecked) {
        $.each($scope.products, function(index, product) {
            if (product.isSelected) {
                if (isChecked) {
                    if ($.inArray(selectedCategoryId, product.updatedCategoryIds) < 0) {
                        product.updatedCategoryIds.push(selectedCategoryId);
                    }
                } else {
                    product.updatedCategoryIds.splice($.inArray(selectedCategoryId, product.updatedCategoryIds), 1);
                }
            }
        });
        $scope.updateTreeUi($scope.createMapCategoryProductsCount());
    };

    // returns a map from category id to num of selected products with that category
    $scope.createMapCategoryProductsCount = function() {
        var mapCategoryProductsCount = {};
        $.each($scope.products, function(index, product) {
            if (product.isSelected) {
                $.each(product.updatedCategoryIds, function(index, catId) {
                    var catCount = mapCategoryProductsCount[catId];
                    mapCategoryProductsCount[catId] = catCount ? catCount + 1 : 1;
                });
            }
        });
        return mapCategoryProductsCount;
    };

    // for each tree node, set its selection state, according to the selected products
    $scope.updateTreeSelection = function(mapCategoryProductsCount) {
        $scope.cTreeRoot.visit(function(node) {
            var nd = node.data;
            // set the node's selection mode
            $scope.cTree.selectKey(nd.key, mapCategoryProductsCount[nd.id] == $scope.selectedProductsCount);
        });
    };

    // for each tree node, set the title, style, and icon
    $scope.updateTreeUi = function(mapCategoryProductsCount) {
        $scope.cTreeRoot.visit(function(node) {
            var nd = node.data;
            // how many products have this node
            nd.count = mapCategoryProductsCount[nd.id];
            // set the node's title
            nd.title = nd.count ? nd.name + ' (' + nd.count + '/' + $scope.selectedProductsCount + ')' : nd.name;
            // set the node's icon
            nd.addClass = '';
            if ((nd.isParent && (0 < nd.count)) ||
                (!nd.isParent && (0 < nd.count && nd.count < $scope.selectedProductsCount))) {
                nd.addClass = 'dynatree-partsel';
            }
        });
        $scope.cTree.redraw();
    };


    // for a selected family, add it or remove it from the products' family list
    $scope.updateSelectedProductsFamilies = function(selectedFamilyId, isChecked) {
        $.each($scope.products, function(index, product) {
            if (product.isSelected) {
                if (isChecked) {
                    if ($.inArray(selectedFamilyId, product.updatedFamilyIds) < 0) {
                        product.updatedFamilyIds.push(selectedFamilyId);
                    }
                } else {
                    product.updatedFamilyIds.splice($.inArray(selectedFamilyId, product.updatedFamilyIds), 1);
                }
            }
        });
        $scope.updateFTreeUi($scope.createMapFamilyProductsCount());
    };

    // returns a map from category id to num of selected products with that category
    $scope.createMapFamilyProductsCount = function() {
        var mapFamilyProductsCount = {};
        $.each($scope.products, function(index, product) {
            if (product.isSelected) {
                $.each(product.updatedFamilyIds, function(index, famId) {
                    var famCount = mapFamilyProductsCount[famId];
                    mapFamilyProductsCount[famId] = famCount ? famCount + 1 : 1;
                });
            }
        });
        return mapFamilyProductsCount;
    };

    // for each tree node, set its selection state, according to the selected products
    $scope.updateFTreeSelection = function(mapFamilyProductsCount) {
        $scope.fTreeRoot.visit(function(node) {
            var nd = node.data;
            // set the node's selection mode
            $scope.fTree.selectKey(nd.key, mapFamilyProductsCount[nd.id] == $scope.selectedProductsCount);
        });
    };

    // for each tree node, set the title, style, and icon
    $scope.updateFTreeUi = function(mapFamilyProductsCount) {
        $scope.fTreeRoot.visit(function(node) {
            var nd = node.data;
            // how many products have this node
            nd.count = mapFamilyProductsCount[nd.id];
            // set the node's title
            nd.title = nd.count ? nd.name + ' (' + nd.count + '/' + $scope.selectedProductsCount + ')' : nd.name;
            // set the node's icon
            nd.addClass = '';
            if ((nd.isParent && (0 < nd.count)) ||
                (!nd.isParent && (0 < nd.count && nd.count < $scope.selectedProductsCount))) {
                nd.addClass = 'dynatree-partsel';
            }
        });
        $scope.fTree.redraw();
    };

    $scope.checkIfOneProductSelected = function() {
        $.each($scope.products, function(index, product) {
            if (product.isSelected) {
                product.categoryIds = product.updatedCategoryIds.slice();
                product.familyIds = product.updatedFamilyIds.slice();
            }
        });

        return $scope.selectedProductsCount == 1;
    };

    $scope.changeMode = function(mode) {
        $scope.currentEditMode = mode;
    };

    $scope.onBranchChange = function() {
        $location.search({'branch': $scope.searchParams.branch});
        $route.reload();
    };

    /*
     * c'tor
     */
    $scope.populateData();
    //$scope.refreshSearchResults(true);

    $scope.$watch("currentAttribute", function() {
        $scope.updateAttributesSelection();
    });

}]);
