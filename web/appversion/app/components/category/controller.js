robinApp.controller('CtrlCategories', [
    '$rootScope',
    '$scope',
    '$window',
    '$route',
    '$filter',
    '$location',
    'TenantIO',
    'AttributeIO',
    'CategoryIO',
    'StorageService',
    function ($rootScope, $scope, $window, $route, $filter, $location, TenantIO, AttributeIO, CategoryIO, StorageService) {

    // globals
    $scope.s3IconPathCategory = robinConsts.s3IconPathCategory;

    // declaring vars used by angular
    $scope.loading = true;
    $scope.activeCategory = null;
    $scope.editCategoryPopup = null;
    $scope.confirmationPopup = null;
    $scope.extTimestamp = '';
    $scope.refreshImage = false;
    $scope.haveTranslations = false;
    $scope.uploadingCsv = false;

    $scope.languages = robinConsts.languages;

    var languageCodeKey = $rootScope.tenantId + "_category_lang_code";
    var defaultLangCode= $window.sessionStorage[languageCodeKey] || $scope.languages[0].code;
    $scope.language =  $scope.languages[0];
    $.each($scope.languages, function(index, l) {
        if (l.code == defaultLangCode)
            $scope.language = l;
    });

    // Get branch from parameter.
    $scope.tenantBranch = $location.search().branch || '';

    $scope.statuses = [
        {id: 0, description: "Deleted"},
        {id: 1, description: "Active"},
        {id: 2, description: "Hidden"},
        {id: 3, description: "Private"},
        {id: 4, description: "Group"}
    ];

    $scope.translations = null;
    $scope.sizeTypes = null;
    $scope.sizeTypesMap = null;
    $scope.attributeIds = [];
    $scope.sizeTypeAttributeName = "sizeType";

    /*
     * functions handling categories tree
     */

    $scope.cTreeSelector = '.tree-container';
    $scope.cTreeObject = null;
    $scope.cTree = null;
    $scope.cTreeRoot = null;
    $scope.cSizeTypeSelector = ".all-type-size";

    $scope.getTenantBranch = function() {
        TenantIO.getTenantBranch().then(function(data) {
            $scope.updateTenantTree(data);
        }, function() {
            robinDialog.alert('server error, please try again.');
        });
    };

    // get tree data from server and init dynatree with that data
    $scope.refreshCategoriesTree = function() {
        CategoryIO.getCategoryTree($scope.language.code, $scope.tenantBranch).success(function(data) {
            $scope.updateTreeData(data);
            $scope.loading = false;
        }).error(function(data, status, headers, config) {
            robinDialog.alert('server error, please try again.');
            $scope.loading = false;
        });
    };

    $scope.updateTreeData = function(treeData) {
        $scope.initTreeDataAttrs(treeData); // attach needed attrs to raw data
        $scope.initTree($scope.cTreeSelector, treeData);
    };

    $scope.updateTenantTree = function(treeData) {
        var list = treeData.children;
        if (treeData.children.length > 0) {
            list = [''].concat(treeData.children);
            $('#branchOptions').css('display', 'block');
        }
        $scope.tenantBranches = list;
    };

    // add extra tree attributes to raw data
    $scope.initTreeDataAttrs = function(nodesList) {
        $(nodesList).each(function(index, node) {
            if (node.children && node.children.length > 0) {
                //node.isFolder = true;
                //node.expand = true;
                $scope.initTreeDataAttrs(node.children);
            }
        });
    };

    // init dynatree with data
    $scope.initTree = function(rootSelector, data) {

        $(rootSelector).dynatree({
            imagePath: " ",
            onActivate: function(node) {
                $scope.activeCategory = node.data;
                $scope.$digest();
            },
            //persist: true,
            dnd: {
                preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
                onDragStart: function(node) {
                    // enable / disable the drag and drop
                    return $scope.editTreeOrder.isEditable; // TODO: remove this dependency
                },
                onDragEnter: function(node, sourceNode) {
                    // Prevent dropping a parent below another parent (only sort nodes under the same parent)
                    if(node.parent !== sourceNode.parent){
                        return true;
                    }
                    //return ['before', 'after']; // Don't allow dropping *over* a node (would create a child)
                    return true;
                },
                onDrop: function(node, sourceNode, hitMode, ui, draggable) {
                    sourceNode.move(node, hitMode);
                    // TODO: expand node here
                }
            },
            clickFolderMode: 1,
            children: data
        });

        // init scope vars
        $scope.cTreeObject = $(rootSelector);
        $scope.cTree = $(rootSelector).dynatree("getTree");
        $scope.cTreeRoot = $(rootSelector).dynatree("getRoot");

    };

    /*
     * tree updates
     */

    $scope.addCategoryNode = function(parentNode, id, title, logo, icon, thumbnail, status, custAttr, attributeIds) {
        var node = parentNode ? parentNode : $scope.cTreeRoot;
        var attributes = [];
        if(attributeIds && attributeIds.length>0){
	        for(var i=0;i<attributeIds.length;i++){
	        	if($scope.sizeTypesMap.hasOwnProperty(attributeIds[i])){
	        		attributes.push($scope.sizeTypesMap[attributeIds[i]]);
	        	}
	        }
        }
        var childNode = node.addChild({
            id: id,
            title: title,
            logo: logo,
            icon: icon,
            thumbnail: thumbnail,
            status: status,
            custAttr: custAttr,
            parentId: node.data.id,
            attributes: attributes
        });
        node.expand(true);
    };

    $scope.updateCategoryNode = function(node, title, logo, icon, thumbnail, status, custAttr,attributeIds) {
        node.data.title = title;
        node.data.logo = logo;
        node.data.icon = icon;
        node.data.thumbnail = thumbnail;
        node.data.status = status;
        node.data.custAttr = custAttr;
        
        var attributes = [];
        if(attributeIds && attributeIds.length>0){
	        for(var i=0;i<attributeIds.length;i++){
	        	if($scope.sizeTypesMap.hasOwnProperty(attributeIds[i])){
	        		attributes.push($scope.sizeTypesMap[attributeIds[i]]);
	        	}
	        }
        }
        node.data.attributes = attributes;
        node.render();
        $scope.refreshImages();
    };
    
    $scope.updateCategoryNodeSizeType = function(node, attributeIds){
    	var attributes = [];
        if(attributeIds && attributeIds.length>0){
	        for(var i=0;i<attributeIds.length;i++){
	        	if($scope.sizeTypesMap.hasOwnProperty(attributeIds[i])){
	        		attributes.push($scope.sizeTypesMap[attributeIds[i]]);
	        	}
	        }
        }
        node.data.attributes = attributes;
        node.render();
    }

    $scope.removeCategoryNode = function(node) {
        node.remove();
        $scope.activeCategory = null;
    };

    /*
     * functions handling popups
     */

    $scope.currentModalSelector = null; // saves current modal

    // open the edit popup, and save data for use after 'ok' is clicked
    // if category is provided, then open 'edit' dialog, otherwise open 'add' dialog
    $scope.openEditDialog = function(category) {

        $scope.currentModalSelector = '#edit-category-modal';
        $scope.resetForm();
        var curNode = $scope.cTreeObject.dynatree("getActiveNode");

        var onSaveChanges = function() {
            if (category) {
                $scope.editCategory(curNode);
            } else {
                $scope.addCategory(curNode);
            }
        };

        // save data on the popup for later use
        $scope.editCategoryPopup = {
            popupTitle: (category ? 'category_edit_category' : 'category_add_category'),
            popupSaveText: category ? 'category_save_changes' : 'category_add_category',
            cId: category ? category.id : null,
            cParentId: category ? category.parentId : null,
            cTitle: category ? category.title : null,
            cCustAttr: category ? category.custAttr : null,
            cLogoUrl: category ? category.logo : null,
            cIconUrl: category ? category.icon : null,
            cThumbnailUrl: category ? category.thumbnail : null,
            cStatus: category ? $scope.getCateogryFullDetailsById(category.status) : $scope.statuses[1],
            onConfirm: onSaveChanges,
            cMode: category? true:false
        };
        
        $scope.getFullTypeSizes(category);
        $scope.makeLogoUrls();

        $($scope.currentModalSelector).modal('show');
        $($scope.cSizeTypeSelector).hide();
        
    };
    
    $scope.initSizeTypeFilters = function(){
        AttributeIO.getAttributes($scope.sizeTypeAttributeName).success(function(data) {
            $scope.sizeTypes = [];
            $scope.sizeTypesMap = {};
            for( var i=0;i<data.length;i++){
	            var obj = {};
	            obj.name = data[i].name;
	            obj.id = data[i].id;
	            obj.free = [];
	            obj.attributeType = $scope.sizeTypeAttributeName;
	            obj.values = [];
	            if( data[i].valuesIds ){
		            for( var j=0;j<data[i].valuesIds.length;j++){
		            	var v = {};
		            	v.id = data[i].valuesIds[j].id;
		            	v.value = data[i].valuesIds[j].name;
		            	obj.values.push(v);
		            }
	            }
	            $scope.sizeTypesMap[obj.id] = obj;
	            $scope.sizeTypes.push(obj);
            }      
        }).error(function(data, status, headers, config) {
            robinDialog.alert('server error, please try again.');
        });
    }
    
    $scope.updateFilterCheckStatus = function(){
    	var checkboxes = $("#allTypeSizes input[type='checkbox']");
    	var displaySizeType = "";
    	$scope.attributeIds = [];
    	for(i=0;i<checkboxes.length;i++){
    		if(checkboxes[i].checked){
    			displaySizeType += checkboxes[i].value + " ";
    			$scope.attributeIds.push(checkboxes[i].id);
    		}
		}
		$scope.sizeType = displaySizeType;
    }
    
    $scope.getFullTypeSizes = function(category){
    	$("#allTypeSizes input[type='checkbox']").prop("checked",false);
    	$scope.attributeIds = [];
    	if(category){
    		var attributes = category.attributes;
    		var displaySizeType = "";
    		if(attributes){
	    		for(i=0;i<attributes.length;i++){
	    			displaySizeType += attributes[i].name + " ";
	    			$("#"+attributes[i].id).prop("checked",true);
	    			$scope.attributeIds.push(attributes[i].id);
	    		}
    		}
    		$scope.sizeType = displaySizeType;
    	}
    	else{
    		$scope.sizeType = "";
    	}
    }
    
    $scope.selectSizeTypes = function(){
    	if( $($scope.cSizeTypeSelector).css("display") === "none" ){
    		$($scope.cSizeTypeSelector).show();
		}
    	else{
    		$($scope.cSizeTypeSelector).hide();
    	}
    }

    $scope.getCateogryFullDetailsById = function(categoryId) {
        for(var i = 0; i < $scope.statuses.length; ++i) {
            if (categoryId == $scope.statuses[i].id)
                return $scope.statuses[i]
        }

        return null;
    };

    $scope.$watch("editCategoryPopup.cId", function(){$scope.makeLogoUrls();});

    $scope.makeLogoUrls = function() {
        if ($scope.editCategoryPopup != null && $scope.editCategoryPopup.cId != null) {
            $scope.editCategoryPopup.cLogoUrl2x = $scope.editCategoryPopup.cId + '@2x.png';
            $scope.editCategoryPopup.cLogoUrl3x = $scope.editCategoryPopup.cId + '@3x.png';
            $scope.editCategoryPopup.cLogoUrlsvg = $scope.editCategoryPopup.cId + '.svg';

            $(".extendedImage").attr("style", "display: inline;");
            $(".extendedImage").attr("onerror", 'this.style.display = "none"');
            /*$(".extendedImage").success(function () {
                $(this).show();
            });*/
        }
    };

    $scope.isImageLoaded = function(image) {
      switch (image) {
          case 0:
              return $("#Image2x")[0].getAttribute("style") == "display: none;";
      }
    };

    $scope.resetForm = function() {
        $('#file-logo-upload-form').get(0).reset();
        $('#file-icon-upload-form').get(0).reset();
        $('#file-thumbnail-upload-form').get(0).reset();
        $('.progress-upload-icon').hide();
        $('.progress-upload-icon2').hide();
    };

    $scope.openDeleteDialog = function(category) {
        var onDeleteConfirmed = function() {
            $scope.deleteCategory($scope.cTreeObject.dynatree("getActiveNode"), category.id);
        };
        var msg = $filter('translate')('delete_confirm');
        $scope.openConfirmationDialog('category_delete_category', msg + ' "' + category.title + '"?', onDeleteConfirmed);
    };

    $scope.openConfirmationDialog = function(title, message, onConfirm, okText, cancelText) {
        $scope.currentModalSelector = '#confirmation-modal';
        $scope.confirmationPopup = {
            title: title,
            message: message,
            onConfirm: onConfirm,
            okText: okText ? okText : 'ok',
            cancelText: cancelText ? cancelText : 'cancel'
        };
        $($scope.currentModalSelector).modal('show');
    };

    $scope.closeCurrentDialog = function() {
        $($scope.currentModalSelector).modal('hide');
    };

    /*
     * action handlers
     */

    $scope.onLanguageChange = function() {
        $window.sessionStorage[languageCodeKey] = $scope.language.code;
        $route.reload();
    };

    $scope.onBranchChange = function() {
        $location.search({'branch': $scope.tenantBranch});
        $route.reload();
    };

    /*
     * called after selecting an icon to upload.
     * server will upload the icon to a temp location, and will return the location
     */
    $scope.submitIconForm = function(formId, progressSelector) {
        var params = [];

        var subtype;
        if (formId == 'file-logo-upload-form') {
            subtype = ""; // lagecy code: The logo is used by mobile and named as 'category-id' by default.
        } else if (formId == 'file-icon-upload-form') {
            subtype = "icon";
        } else if (formId == 'file-thumbnail-upload-form') {
            subtype = "thumbnail";
        }
        if (subtype) {
            params.push("subtype=" + subtype)
        }

        var key = $scope.editCategoryPopup.cId;
        if (key) {
            params.push("key=" + key)
        }

        query = params.join("&")

        $scope.editCategoryPopup.fileTooBig = false;
        $scope.setLoading(true);
        var url = '/mw/upload/icon/' + $rootScope.tenantId + '/category';
        if (query != "") {
            url = url + '?' + query;
        }
        robinCommon.submitFilesForm(
            '#' + formId, // form selector
            progressSelector,
            // url for sending the form
            url,
            // on success
            function(data) {
                if (formId === 'file-logo-upload-form') {
                    // TODO: should replace the key with URL
                    $scope.editCategoryPopup.cLogoUrl = data.tempKey;
                } else if (formId === 'file-icon-upload-form') {
                    $scope.editCategoryPopup.cIconUrl = data.iconUrl;
                } else if (formId === 'file-thumbnail-upload-form') {
                    $scope.editCategoryPopup.cThumbnailUrl = data.iconUrl;
                }
                $scope.refreshImages(true);
                $(progressSelector).hide();
                $scope.setLoading(false);
            },
            // on error
            function(data) {
                if (data.status == 400) {
                    $scope.editCategoryPopup.fileTooBig = true;
                } else {
                    robinDialog.alert('Unable to upload icon, error: ' + data.statusText);
                }
                $(progressSelector).hide();
                $scope.setLoading(false);
            },
            // on before send
            function() {
                $(progressSelector).show();
            },
            // on during upload
            function(e) {
                if (e.lengthComputable){
                    $('progress').attr({
                        value: e.loaded,
                        max: e.total
                    });
                }
            }
        );

    };

    $scope.showTranslationModal = function() {
        $scope.currentModalSelector = '#translations-modal';
        $($scope.currentModalSelector).modal('show');
    };

    $scope.showTranslationInfoModal = function() {
        $scope.currentModalSelector = '#translations-info-modal';
        $($scope.currentModalSelector).modal('show');
    };

    $scope.submitCsvTranslationsForm = function(iconType) {
        $scope.uploadingCsv = true;
        formSelected = '#file-upload-form-csv';
        $scope.setLoading(true);

        var progressSelector = '.progress-upload-icon2';
        robinCommon.submitFilesForm(
            formSelected, // form selector
            progressSelector,
            // url for sending the form
            '/mw/category/csv/' + $rootScope.tenantId,
            // on success
            function(data) {

                $scope.translations = data;

                $.map($scope.translations, function(a) {
                    if(a.id == "-1") {
                        a.langs.en_US = a.langs.erMsg;
                        a.langs.zh_CN = a.langs.input;
                        return a;
                    }
                    else return a
                });

                $scope.currentModalSelector = '#translations-modal';
                $($scope.currentModalSelector).modal('show');
                $scope.haveTranslations = true;
                $scope.uploadingCsv = false;

                $(progressSelector).hide();
                $scope.setLoading(false);
            },
            // on error
            function(data) {
                if (data.status == 400) {

                } else {
                    robinDialog.alert('Unable to upload icon, error: ' + data.statusText);
                }
                $(progressSelector).hide();
                $scope.setLoading(false);
                $scope.uploadingCsv = false;
            },
            // on before send
            function() {
                $(progressSelector).show();
                $scope.uploadingCsv = true;
            },
            // on during upload
            function(e) {
                if (e.lengthComputable){
                    $('progress').attr({
                        value: e.loaded,
                        max: e.total
                    });
                }
            }
        );
    };

    $scope.submitExtendedIconForm = function(iconType) {

        var iconName = "";
        var formSelected = "";
        switch (iconType) {
            case 0: {
                iconName = $scope.editCategoryPopup.cLogoUrl2x;
                formSelected = '#file-upload-form2';
            } break;
            case 1: {
                iconName = $scope.editCategoryPopup.cLogoUrl3x;
                formSelected = '#file-upload-form3';
            } break;
            case 2: {
                iconName = $scope.editCategoryPopup.cLogoUrlsvg;
                formSelected = '#file-upload-form4';
            } break;
        }


        $scope.setLoading(true);
        var progressSelector = '.progress-upload-icon2';
        robinCommon.submitFilesForm(
            formSelected, // form selector
            progressSelector,
            // url for sending the form
            '/mw/upload/icon/' + $rootScope.tenantId + '/category?key=' + iconName,
            // on success
            function(data) {
                refreshExtendedIcons();
                $(progressSelector).hide();
                $scope.setLoading(false);
            },
            // on error
            function(data) {
                if (data.status == 400) {
                    $scope.editCategoryPopup.fileTooBig = true;
                } else {
                    robinDialog.alert('Unable to upload icon, error: ' + data.statusText);
                }
                $(progressSelector).hide();
                $scope.setLoading(false);
            },
            // on before send
            function() {
                $(progressSelector).show();
            },
            // on during upload
            function(e) {
                if (e.lengthComputable){
                    $('progress').attr({
                        value: e.loaded,
                        max: e.total
                    });
                }
            }
        );

    };

    function refreshExtendedIcons() {
        $("#Image2x").attr("src", $scope.s3IconPathCategory + $scope.editCategoryPopup.cLogoUrl2x + '?' + (new Date().getTime()));
        $("#Image3x").attr("src", $scope.s3IconPathCategory + $scope.editCategoryPopup.cLogoUrl3x + '?' + (new Date().getTime()));
        $("#ImageSvg").attr("src", $scope.s3IconPathCategory + $scope.editCategoryPopup.cLogoUrlsvg + '?' + (new Date().getTime()));

        $(".extendedImage").attr("style", "display: inline;");
        $(".extendedImage").attr("onerror", 'this.style.display = "none"');
    }

    $scope.isCategoryIconExists = function() {
      return $scope.editCategoryPopup != null && $scope.editCategoryPopup.cLogoUrl != null && $scope.editCategoryPopup.cLogoUrl != "";
    };

    $scope.clearLogo = function() {
        $scope.editCategoryPopup.cLogoUrl = null;
    };

    $scope.clearIcon = function() {
        $scope.editCategoryPopup.cIconUrl = null;
    };

    $scope.clearThumbnail = function() {
        $scope.editCategoryPopup.cThumbnailUrl = null;
    };

    // called after 'ok' is clicked at 'add category' popup
    $scope.addCategory = function(parentNode) {

        var ecp = $scope.editCategoryPopup;

        if (ecp.cTitle) {

            // clear errors
            ecp.titleRequired = false;

            // prepare data for sending
            var postData = {
                name: ecp.cTitle,
                logo: ecp.cLogoUrl,
                icon: ecp.cIconUrl,
                thumbnail: ecp.cThumbnailUrl,
                status: ecp.cStatus.id,
                custAttr: ecp.cCustAttr,
                parentId: parentNode ? parentNode.data.id : null,
                attributeIds: $scope.attributeIds
            };

            $scope.loading = true;
            CategoryIO.createCategory(postData, $scope.language.code, $scope.tenantBranch).success(function(data, status, headers, config) {
                // update tree (the 'data' returned is the key of the category just created)
                $scope.addCategoryNode(parentNode, data, ecp.cTitle, ecp.cLogoUrl, ecp.cIconUrl, ecp.cThumbnailUrl, ecp.cStatus.id, ecp.cCustAttr, $scope.attributeIds);
                // message the user
                robinDialog.alert('Category has been added.');
                // close dialog
                $scope.closeCurrentDialog();
                $scope.loading = false;
            }).error(function(data, status, headers, config) {
                robinDialog.alert('server error, please try again.');
                $scope.loading = false;
            });

        } else {
            // set error
            ecp.titleRequired = true;
        }

    };

    // called after 'ok' is clicked at 'edit category' popup
    $scope.editCategory = function(node) {

        var ecp = $scope.editCategoryPopup;

        var postData = {
            id: ecp.cId,
            name: ecp.cTitle,
            logo: ecp.cLogoUrl,
            icon: ecp.cIconUrl,
            thumbnail: ecp.cThumbnailUrl,
            status: ecp.cStatus.id,
            custAttr: ecp.cCustAttr,
            parentId: ecp.cParentId,
            attributeIds: $scope.attributeIds
        };

        $scope.loading = true;
        CategoryIO.updateCategory(postData, ecp.cId, $scope.language.code, $scope.tenantBranch).success(function(data, status, headers, config) {
            // update tree (the 'data' returned is the key of the category, which is also its logo content)
            $scope.updateCategoryNode(node, ecp.cTitle, ecp.cLogoUrl ? data : null, ecp.cIconUrl, ecp.cThumbnailUrl, ecp.cStatus.id, ecp.cCustAttr, $scope.attributeIds);
            // apply the size type attributes to subitem if apply checkbox is checked
            $scope.applySizeTypesToSubitems(node);
            // message the user
            robinDialog.alert('Category has been updated.');
            // finish the flow
            $scope.closeCurrentDialog();
            $scope.loading = false;
        }).error(function(data, status, headers, config) {
            robinDialog.alert('server error, please try again.');
            $scope.loading = false;
        });

    };
    
    $scope.applySizeTypesToSubitems = function(node){
    	if(!$("#applyItem").prop("checked") ){
    		return;
    	}
    	/*
    	var attributes = [];
    	attributeIds = $scope.attributeIds;
        if(attributeIds && attributeIds.length>0){
	        for(var i=0;i<attributeIds.length;i++){
	        	if($scope.sizeTypesMap.hasOwnProperty(attributeIds[i])){
	        		attributes.push($scope.sizeTypesMap[attributeIds[i]]);
	        	}
	        }
        }*/
        
    	if(node.childList && node.childList.length>0){
    		for(var i=0;i<node.childList.length;i++){
    			var id = node.childList[i].data.id;
    			var name = node.childList[i].data.title;
    	 		
    			var postData = {
    		        id: id,
    		        name: name,
    		        attributeIds: $scope.attributeIds
    		    };

    		    $scope.loading = true;
                CategoryIO.updateCategory(postData, id, $scope.language.code, $scope.tenantBranch).success(function(data, status, headers, config) {
    		            // update tree (the 'data' returned is the key of the category, which is also its logo content)
    		        
    		    }).error(function(data, status, headers, config) {
    		        robinDialog.alert('server error, please try again.');
    		        $scope.loading = false;
    		    });
    		}
    		for(var i=0;i<node.childList.length;i++){
    			var childNode = node.childList[i];	
    			$scope.updateCategoryNodeSizeType(childNode, $scope.attributeIds);
    		}
    	}
    }

    // called after 'ok' is clicked at 'delete category' popup
    $scope.deleteCategory = function(node, id) {
        $scope.loading = true;
        CategoryIO.deleteCategory(id).success(function(data, status, headers, config) {
            // update tree
            $scope.removeCategoryNode(node);
            // message the user
            robinDialog.alert('Category has been deleted.');
            // finish the flow
            $scope.closeCurrentDialog();
            $scope.loading = false;
        }).error(function(data, status, headers, config) {
            robinDialog.alert('server error, please try again.');
            $scope.loading = false;
        });
    };

    /*
     * functions handling tree order editing
     */

    $scope.editTreeOrder = {

        isEditable: false,

        onEditOrder: function() {
            // save current tree data
            this.curTreeData = $scope.cTree.toDict();
            // enter edit mode
            this.isEditable = true;
        },

        onDoneEditingOrder: function() {
            // save tree order
            var that = this;
            $scope.loading = true;

            var buildCatTree = function(nodes) {
                var catTree = [];
                $.each(nodes, function(index, node) {
                    catTree.push({
                        id: node.id,
                        children: node.children ? buildCatTree(node.children) : []
                    });
                });
                return catTree;
            };
            var tree = buildCatTree($scope.cTree.toDict());
            CategoryIO.updateCategoryTree(tree, $scope.language.code, $scope.tenantBranch).success(function(data, status, headers, config) {
                // exit edit mode
                that.isEditable = false;
                $scope.loading = false;
            }).error(function(data, status, headers, config) {
                robinDialog.alert('server error, please try again.'); // TODO: improve this error handling
                $scope.loading = false;
            });
        },

        onCancelEditOrder: function() {
            // revert tree structure
            $scope.updateTreeData(this.curTreeData);
            $scope.cTree.reload();
            this.isEditable = false;
        },

        sortBrandsSubTreeByTitle: function() {
            $scope.cTree.visit(function(node) {
                if (node.data.title == 'Brands') {
                    node.sortChildren(null, true);
                    return false;
                }
            }, false);
        }

    };

    $scope.getDescriptionByStatus = function(status) {

      switch (status) {
          case 0: return "Deleted";
          case 1: return "Active";
          case 2: return "Hidden";
          case 3: return "Private";
          case 4: return "Group";
          default : return "Unknown Status";
      }
    };

    /*
     * utils
     */

    $scope.setLoading = function(toggle) {
        $scope.loading = toggle;
        $scope.$digest();
    };

    $scope.refreshImages = function(digest) {
        $scope.extTimestamp = '?' + (new Date().getTime());
        if (digest)
            $scope.$digest();
    };

    $scope.isUploadingCsv = function() {
      return $scope.uploadingCsv;
    };

    $scope.initSizeTypeFilters();
    /*
     * c'tor
     */
    $scope.refreshCategoriesTree();
    /*$(".extendedImage").error(function () {
        $(this).hide();
    });*/

    $scope.getTenantBranch();

}]);
