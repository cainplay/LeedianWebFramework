robinApp.controller('CtrlEditVariation', [
    '$scope',
    '$rootScope',
    '$location',
    '$q',
    '$window',
    '$filter',
    '$route',
    'UserService',
    'ProductService',
    'AttributeIO',
    'TenantIO',
    'ProductIO',
    function($scope, $rootScope, $location, $q, $window, $filter, $route, UserService, ProductService, AttributeIO, TenantIO, ProductIO) {
        // Global parameters.
        $scope.EMode = {
            eCreate : 'create',
            eEdit : 'edit'
        };

        // TODO: Move status value to common place.
        $scope.statuses = [
            {id: 0, description: "Deleted"},
            {id: 1, description: "Active"},
            {id: 2, description: "Hidden"},
            {id: 3, description: "Private"}
        ];

        // TODO: Move fileTypes to common place.
        $scope.fileTypes = [
            {metaData: '', name: ''},
            {metaData: 'iso', name: 'ISO File'},
            {metaData: 'obj', name: 'Model Obj'},
            {metaData: 'txtr', name: 'Model Texture'},
            {metaData: 'normal', name: 'Material Normal'},
            {metaData: 'wallfloor', name: 'Wallpaper/Floor Jpg'},
            {metaData: '3ds', name: '3DStudioMax File'},
            {metaData: 'topView', name: 'TopView File'},
            {metaData: 'scene', name: 'Scene Graph'},
            {metaData: 'thumbnail', name: 'Thumbnail Image' }
        ];

        // Data model parameters.
        $scope.variations = [];
        $scope.currentVariationIndex = 0;
        $scope.availableBranches = [];
        $scope.variationTagValues = [];

        // UI bindings parameters.
        $scope.variationTag = null;
        $scope.retailer = null;
        $scope.variationName = null;
        $scope.path = null;
        $scope.externalId = null;
        $scope.files = [];
        $scope.tempKey = null;
        $scope.forceTopViewGenerate = false;
        $scope.status = $scope.statuses[1];
        $scope.previewImage = null;

        // URL parameters.
        $scope.productId = $location.search().guid || '';
        var branchName = $location.search().branch || '';
        $scope.branch = $scope.productId ? branchName : '';
        $scope.mode = $location.search().mode || $scope.EMode.eCreate;
        $scope.languages = robinConsts.languages;
        $scope.language = null;
        var langCode = $location.search().lang || 'en_US';
        if (langCode) {
            robinConsts.languages.forEach(function(item) {
                if ($scope.language) return;
                if (item.code === langCode) $scope.language = item;
            })
        } else {
            $scope.language = robinConsts.languages[0];  
        }

        // Loading status parameters.
        $scope.curtainText = 'loading';
        $scope.loading = true;

        // User related parameters.
        $scope.user = UserService.get();
        $scope.isManufacturerRole = ($scope.user.role.name === $rootScope.RoleEnum.MANUFACTURER);

        $scope.clear = function() {
            $scope.variationTag = null;
            $scope.retailer = null;
            $scope.variationName = null;
            $scope.path = null;
            $scope.externalId = null;
            $scope.files = [];
            $scope.tempKey = null;
            $scope.forceTopViewGenerate = false;
            $scope.status = $scope.statuses[1];
        }

        // Event handlers.
        $scope.onPrevBtnClicked = function() {
            if ($scope.currentVariationIndex == 0) $scope.currentVariationIndex = $scope.variations.length; //make it circular
            $scope.currentVariationIndex--;
            onCurrentChanged();
        }

        $scope.onNextBtnClicked = function() {
            if ($scope.currentVariationIndex == $scope.variations.length - 1) $scope.currentVariationIndex = -1; //if at the end go back to beginning
            $scope.currentVariationIndex++;
            onCurrentChanged();
        }

        $scope.onLanguageChanged = function() {
            $scope.reload({lang: $scope.language.code});
        }

        $scope.onBranchChanged = function() {
            $scope.reload({branch: $scope.branch});
        }

        $scope.onCreateVariationBtnClicked = function() {
            var url = '/#/' + $rootScope.tenantId +'/variation' + '?guid=' + $scope.productId;
            url += ('&mode=' + $scope.EMode.eCreate);
            if ($scope.branch) url += ('&branch=' + $scope.branch);
            if ($scope.language) url += ('&lang=' + $scope.language.code);
            $window.open(url, '_blank');
        }

        $scope.onGoToUrlClicked = function() {
            var url = $scope.retailer ? $scope.retailer.url : '';
            if (!url.match(/(^http:\/\/)|(^www)/)) {
                robinDialog.alert($filter('translate')('uploadproduct_invalid_url'));
                return;
            }
            $window.open(url);
        };

        $scope.onUploadFileClicked = function() {
            $scope.currentModalSelector = '#upload-file-modal';
            $($scope.currentModalSelector).on('hidden.bs.modal', function () {
                if($scope.xhr && $scope.xhr.readyState != 4){
                    $scope.xhr.abort();
                }
                $scope.setLoading(false);
                $scope.closeCurrentDialog();
            })

            $($scope.currentModalSelector).modal('show');
            $('#file-upload-form').get(0).reset();
            $('.progress-upload-file').hide();
        }

        // TODO: Move this method to a common place.
        $scope.openConfirmationDialog = function(title, message, onConfirm, okText, cancelText) {
            $scope.currentModalSelector = '#confirmation-modal';
            $scope.confirmationPopup = {
                title: title,
                message: message,
                onConfirm: onConfirm,
                okText: okText ? okText : 'ok',
                cancelText: cancelText ? cancelText : 'cancel'
            };
            $($scope.currentModalSelector).modal('show');
        };

        $scope.onDeleteBtnClicked = function() {
            $scope.openConfirmationDialog('uploadproduct_delete_variation', 'uploadproduct_delete_confirm',
                function() {
                    $scope.setLoading(true);
                    var currentVariation = $scope.variations[$scope.currentVariationIndex];
                    ProductIO.deleteVariation($scope.productId, currentVariation.id, $scope.language.code, $scope.branch).then(function(data) {
                        var isLastOne = ($scope.variations.length === 1);
                        var mode = isLastOne ? $scope.EMode.eCreate : $scope.EMode.eEdit;
                        $scope.reload({mode: mode});
                    }, function(error) {
                        robinDialog.alert(error.data);
                        $scope.setLoading(false);
                    });
                    $scope.closeCurrentDialog();
                }
            );
        };

        $scope.reload = function(option) {
            option = option || {};
            $location.search({
                'guid': option.guid || $scope.productId,
                'mode': option.mode || $scope.mode,
                'branch': option.branch || $scope.branch,
                'lang': option.lang || $scope.language.code
            });
            $route.reload();
        };

        $scope.onRemoveFileBtnClicked = function(fileIndex) {
            $scope.files.splice(fileIndex, 1);
        };

        $scope.onSaveBtnClicked = function() {
            // Validate parameters.
            if (!validateParameters())return;
            // Validate file meta type.
            if (!validateFiles($scope.files, true)) return;

            $scope.curtainText = 'uploadproduct_saving_variation';
            $scope.setLoading(true);

            var postData = {
                externalId: $scope.externalId,
                retailers: [$scope.retailer],
                status: $scope.status.id,
                name: $scope.variationName,
                path: $scope.path,
                files: $scope.files,
                variationTag: $scope.variationTag
            };

            if ($scope.mode === $scope.EMode.eCreate) {
                saveVariation(postData);
            } else {
                updateVariation(postData);
            }
        };

        var saveVariation = function(postData) {
            ProductIO.createVariation(postData, $scope.productId, $scope.language.code, $scope.branch).then(function(response) {
                $scope.setLoading(false);
                robinDialog.alertWithCallback("Create variation successfully!", function() {
                    $scope.reload({mode: $scope.EMode.eEdit});
                });
            }, function(error) {
                robinDialog.alert(error.data);
                $scope.setLoading(false);
            });
        };

        var updateVariation = function(postData) {
            var currentVariation = $scope.variations[$scope.currentVariationIndex];
            ProductIO.updateVariation(postData, $scope.productId, currentVariation.id, $scope.language.code, $scope.branch).success(function(data) {
                $scope.setLoading(false);
                robinDialog.alertWithCallback("Update variation successfully!", function() {
                    $scope.reload();
                });
            }, function(error) {
                robinDialog.alert(error.data);
                $scope.setLoading(false);
            });
        };

        $scope.submitFileForm = function(file) {

            var uploadUrl = '/mw/upload/' + $rootScope.tenantId + '?' + 'name=' + file.name + '&lang=' + $scope.language.code;
            if ($scope.tempKey) {
                uploadUrl += ('&key=' + $scope.tempKey);
            }

            $scope.setLoading(true);
            var progressSelector = '.progress-upload-file';
            $scope.xhr = robinCommon.submitFilesForm(
                '#file-upload-form', // form selector
                progressSelector,
                // url for sending the form
                uploadUrl,
                // on success
                function(data) {
                    $scope.tempKey = data.newKey;
                    $scope.forceTopViewGenerate = true;
                    $scope.files.push({
                        name: data.realFileName, // real file name
                        metaData: '', // maybe in the future to check the extension of the file and give the user suggestion
                        uploadByUser: true,
                        serverName: data.fileUrl,
                        storageUrl: data.storageUrl
                    });

                    $scope.setLoading(false);
                    $scope.closeCurrentDialog();
                },
                // on error
                function(data) {
                    $(progressSelector).hide();
                    $scope.tempKey = null; // Clear upload temp key.

                    if(data.statusText == 'abort'){
                        robinDialog.alert('File upload is aborted.');
                    }
                    else {
                        robinDialog.alert('Unable to upload file, error: ' + data.statusText);
                    }
                    $scope.setLoading(false);

                    $scope.closeCurrentDialog();
                },
                // on before send
                function() {
                    $(progressSelector).show();
                },
                // on during upload
                function(e) {
                    /*if (e.lengthComputable) {
                        $(progressSelector).attr({
                            value: e.loaded,
                            max: e.total
                        });
                    }*/
                }
            );
        };

        $scope.closeCurrentDialog = function() {
            $($scope.currentModalSelector).modal('hide');
            $scope.xhr = null;
        };

        $scope.setLoading = function(toggle) {
            $scope.loading = toggle;
            if(!$rootScope.$$phase){
                $scope.$digest();
            }
        };

        var onCurrentChanged = function () {
            // Clear current values.
            $scope.clear();

            var v = angular.copy($scope.variations[$scope.currentVariationIndex]);
            $scope.externalId = v.externalId;
            $scope.variationName = v.name;
            $scope.path = v.path;
            $scope.files = v.files;

            if (v.retailers) {
                $scope.retailer = v.retailers[0];
            } else if ($scope.productRetaier) {
                $scope.retailer = angular.copy($scope.productRetaier);
            }

            if (v.status !== undefined) {
                for (var i = 0; i < $scope.statuses.length; i++) {
                    if ($scope.statuses[i].id == v.status) {
                        $scope.status = $scope.statuses[i];
                        break;
                    }
                }
            }

            if(v.variationTag){
                for (var i = 0; i < $scope.variationTagValues.length; i++) {
                    if ($scope.variationTagValues[i].name == v.variationTag.name) {
                        $scope.variationTag = $scope.variationTagValues[i];
                        break;
                    }
                }
            }
        }

        // Utility methods. TODO: Move this to common place.
        var validateParameters = function() {
            var isValid = true;
            var message = '';
            if ($scope.variationName === null || $scope.variationName === '') {
                isValid = false;
                message += $filter('translate')('uploadproduct_name_not_empty') + '<br />';
            }
            // TODO: Add any other possible check here.
            if (message) robinDialog.alert(message);
            return isValid;
        }

        var validateFiles = function(files, allowEmptyMetadata) {
            var message = "";
            for (var i = 0; i < files.length; ++i) {
                var fileExtension = files[i].name.substring(files[i].name.lastIndexOf('.') + 1, files[i].name.length);
                fileExtension = fileExtension.toLowerCase();
                switch (files[i].metaData) {
                    case 'iso':
                        if (fileExtension != 'jpg' && fileExtension != 'jpeg') {
                            message += $filter('translate')('uploadproduct_iso_extension') + '<br />';
                        }
                        break;
                    case 'wallfloor':
                        if (fileExtension != 'jpg' && fileExtension != 'jpeg') {
                            message += $filter('translate')('uploadproduct_wallpaper_extension') + '<br />';
                        }
                        break;
                    case 'obj':
                        if (fileExtension != 'obj') {
                            message += $filter('translate')('uploadproduct_obj_extension') + '<br />';
                        }
                        break;
                    case 'txtr':
                        if (fileExtension != 'png') {
                            message += $filter('translate')('uploadproduct_texture_extension') + '<br />';
                        }
                        break;
                    case 'thumbnail':
                        if (fileExtension != 'jpg' && fileExtension != 'jpeg') {
                            message += $filter('translate')('uploadproduct_thumbnail_extension') + '<br />';
                        }
                        break;
                    case 'normal':
                        if (fileExtension != 'png') {
                            message += $filter('translate')('uploadproduct_material_normal_extension') + '<br />';
                        }
                        break;
                    default:
                        if (!allowEmptyMetadata) message += $filter('translate')('uploadproduct_choose_file_type') + '<br />';
                }
            }
            if (message) {
                robinDialog.alert(message);
                return false;
            }
            return true;
        }

        var initProductInfo = function(data) {
            // Collect variations.
            if (data.attributes.variations) {
                var all = data.attributes.variations.free;
                if (all) {
                    for (i = 0; i < all.length; i++) {
                        $scope.variations.push(JSON.parse(all[i]));
                    }
                }
            }
            $scope.retailer = angular.copy(data.retailer) || null;
            $scope.productRetaier = angular.copy(data.retailer) || null;
            $scope.path = data.path || null;
            $scope.externalId = data.externalId || null;

            // Update preview image.
            if(data.files && data.files.length > 0) {
                $scope.previewImage = $.grep(data.files, function(file, index) {
                    return !file.metaData || file.metaData == "iso";
                })[0].storageUrl;
                $("#dynamicPreviewImage img").attr("src", $scope.previewImage);
            }
        }

        var initVariationTag = function(data) {
            var attributes = data.data;
            if (!attributes) return;
            var variationAttr = undefined;
            attributes.forEach(function(item) {
                if (variationAttr) return;
                if (item.name === AttributeNames.VariationTags) {
                    variationAttr = item;
                }
            });

            if (variationAttr) {
                $scope.variationTagValues = variationAttr.valuesIds;
            }
        };

        var initBranchData = function(data) {
            var branchData = data.children;
            $scope.availableBranches = branchData ? branchData.slice(0) : []; 
            if ($scope.availableBranches.length > 0) {
                $scope.availableBranches.unshift('');
            }
        };

    
        // Initialize page.
        (function () {

            if(!$scope.productId) {
                robinDialog.alert($filter('translate')('uploadproduct_product_id_required'));
                return;
            }

            $scope.setLoading(true);
            var Product= new ProductService.Product($scope.language.code, $scope.branch);
            $q.all([
                Product.getById($scope.productId),
                TenantIO.getTenantBranch(),
                AttributeIO.getAttributes()
            ]).then(function(results){
                $scope.setLoading(false);

                initProductInfo(results[0]);
                initBranchData(results[1]);
                initVariationTag(results[2]);

                // Update UI according to current variation.
                if ($scope.mode === $scope.EMode.eEdit) {
                    onCurrentChanged();
                }
            }, function() {
                robinDialog.alert('server error, please try again.');
            })
        })();
    }]);

