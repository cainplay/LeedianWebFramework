robinApp.controller('CtrlSearch', [
    '$rootScope',
    '$scope',
    '$http',
    '$q',
    '$location',
    'TenantIO',
    'ProductIO',
    'BrandIO',
    'RetailerIO',
    'CategoryIO',
    'LocaleService',
    function ($rootScope, $scope, $http, $q, $location, TenantIO, ProductIO, BrandIO, RetailerIO, CategoryIO, LocaleService) {

    $scope.loading = true;

    $scope.availableCategories = [];
    $scope.availableBrands = [];
    $scope.availableStatuses = [];
    $scope.availableTypes = [];
    $scope.assetTypeId = "";
    $scope.totalProductsCount = null;

    // search params
    $scope.searchParams = {
        freeText: null,
        category: null,
        brand: null,
        status: null,
        productGuid: null,
        productPath: null,
        productSKU: null,
        branch: null,
        type: null
    };

    // Get branch from parameter.
    $scope.searchParams.branch = $location.search().branch || '';

    // basic search params
    $scope.productsPerPage = {
        selected: 10,
        options: [10, 20, 50, 100]
    };
    $scope.currentPage = 0;

    // items on page functionality
    $scope.onItemsOnPageChanged = function() {
        $scope.currentPage = 0;
        $scope.refreshSearchResults(true);
    };

    $scope.populateData = function() {

        var url = '/mw/products/search/data/' + $rootScope.tenantId;
        if ($scope.searchParams.branch) {
            url += ("?branch=" + $scope.searchParams.branch);
        }

        return $http({
            method: 'GET',
            url: url 
        }).success(function(data) {

            // populate categories
            $scope.availableCategories = data.categories;
            $scope.availableCategories.unshift({id: '', displayName: ''})
            $scope.searchParams.category = $scope.availableCategories[0];

            // populate brands
            $scope.availableBrands = data.brands.slice(0);
            $scope.availableBrands.unshift({id: '', name: ''});
            $scope.availableBrands.sort(LocaleService.localeStringSort(undefined, function(item) {return item.name;}));
            $scope.searchParams.brand = $scope.availableBrands[0];

            // populate statuses
            $scope.availableStatuses = data.statuses;
            $scope.availableStatuses.unshift({id: -1, description: ''});
            $scope.searchParams.status = $scope.availableStatuses[0];

            var attributes = data.attributes;
            var assetTypeAttr = attributes.filter(function(attr){if(attr.name == "AssetType") return true;});
            $scope.assetTypeId = assetTypeAttr[0].id;
            $scope.availableTypes = assetTypeAttr[0].valuesIds;
            $scope.availableTypes.unshift({id: '', name: ''});
            $scope.searchParams.type = $scope.availableTypes[0];


            $scope.loading = false;
        }).error(function(data, status, headers, config) {
            $scope.loading = false;
            robinDialog.alert('Unable to get search data from server, please try again.');
        });
    };

    $scope.onSearchButtonClicked = function() {
        $scope.selectedProduct = null;
        $scope.refreshSearchResults(true);
    };

    // products list
    $scope.products = [];
    $scope.selectedProduct = null;
    var getContentType = function(product) {
        if (!product.attributes) return "";
        var contentTypes = product.attributes.filter(function(attr) {if (attr.name === "ContentType") return true;});
        var typeStrings = "";
        contentTypes.forEach(function(ctype) {
            if (ctype.valuesIds2 && ctype.valuesIds2.length > 0) {
                typeStrings = ctype.valuesIds2[0].name;
            }
        });
        return typeStrings;
    };

    var getBrand = function(product){
        if(product.brands.length <= 0)
            return "";
        for (i = 0; i < $scope.brands.length; ++i) {
            if ($scope.brands[i].id == product.brands[0]) {
                return $scope.brands[i].title;
            }
        }
        return "";
    };

    var getRetailer = function(product) {
        if(!product.retailers)
            return "";
        if(product.retailers.length <= 0)
            return "";
        for(i=0; i < $scope.retailers.length; i++){
            if ($scope.retailers[i].id == product.retailers[0].id){
                return $scope.retailers[i].name;
            }
        }
        return "";
    };

    var getRetailerPrice = function(product){
        if(!product.retailers)
            return "";
        if(product.retailers.length <= 0)
            return "";
        if(!product.retailers[0].price)
            return "";
        return parseFloat(product.retailers[0].price).toFixed(2);
    };

    var getStyle = function(product){
        if (!product.attributes)
            return "";
        var styleStrings = "";
        product.attributes.forEach(function(attr){
            if(attr.name === "Styles"){
                if(attr.valuesIds2 && attr.valuesIds2.length > 0){
                    styleStrings = attr.valuesIds2[0].name;
                }
            }
        });
        return styleStrings;
    };

    var getScalable = function(product){
        if (!product.attributes)
            return "";
        var isScalableStrings = "";
        product.attributes.forEach(function(attr){
            if(attr.name === "isScalable"){
                if(attr.valuesIds2 && attr.valuesIds2.length > 0){
                    isScalableStrings = attr.valuesIds2[0].name;
                }
            }
        });
        return isScalableStrings;
    };

    var getCouponType = function(product){
        if (!product.attributes)
            return "";
         var couponStrings = "";
         product.attributes.forEach(function(attr){
             if(attr.name === "couponType"){
                 if(attr.valuesIds2 && attr.valuesIds2.length > 0){
                     couponStrings = attr.valuesIds2[0].name;
                 }
             }
         });
         return couponStrings;
    };

    var getDefaultHeight = function(product) {
        if (!product.attributes)
            return "";
        var defaultHeightStrings = "";
        product.attributes.forEach(function(attr){
            if(attr.name === "defaultHeight"){
                if(attr.free && attr.free.length > 0){
                    defaultHeightStrings = attr.free[0];
                }
            }
        });
        return defaultHeightStrings;
    };

    var getCategoryName = function(node, parents, id) {
        var parentsStr = parents + "/" + node.title;
        var categoryName = "";
        if(node.id === id){
            categoryName = parentsStr;
            return categoryName;
        }
        else if(node.children.length > 0){
            node.children.forEach(function(childNode){
                var tempStr = getCategoryName(childNode, parentsStr, id);
                if(tempStr !== "")
                    categoryName = tempStr;
            });
        }
        return categoryName;
    };

    var getCategory = function(product) {
        if (product.categoryIds.length == 0)
            return "";
        var categoryStrings = "";
        $scope.categoryTree.forEach(function(node){
            var tempStr = getCategoryName(node, "", product.categoryIds[0]);
            if(tempStr !== "")
                categoryStrings = tempStr;
        });
        return categoryStrings;
    };

    $scope.refreshSearchResults = function(isRefreshPagination) {
        $scope.loading = true;

        var categoryIds = $scope.searchParams.category.id ? [$scope.searchParams.category.id] : undefined;
        var brandIds = $scope.searchParams.brand.id ? [$scope.searchParams.brand.id] : undefined;
        var typeFilter = $scope.searchParams.type.id ? $scope.assetTypeId + "_" + $scope.searchParams.type.id : undefined;
        var param = {
            offset: ($scope.currentPage * $scope.productsPerPage.selected),
            limit: $scope.productsPerPage.selected,
            status: $scope.searchParams.status.id,
            freeText: $scope.searchParams.freeText,
            categoryIds: categoryIds,
            brandIds: brandIds,
            branch: $scope.searchParams.branch,
            guid: $scope.searchParams.productGuid,
            path: $scope.searchParams.productPath,
            sku: $scope.searchParams.productSKU,
            attributeIds: typeFilter
        }
        ProductIO.searchProducts(param).success(function(data) {
            if (data.products.length == 0) {
                robinDialog.alert('no products found, please revise your search.');
/*
            if this is uncommented, change the limit=50 for the server call
            } else if (data.products.length > 20) {
                robinDialog.alert('more than 20 products found, please refine your search.');
*/
            } else {
                $scope.totalProductsCount = data.total;
                $scope.products = data.products;
                // convert references for form easier for angular
                if (isRefreshPagination)
                    $scope.refreshPagination($scope.totalProductsCount);
                $.each($scope.products, function(index, product) {
                    product.vReferences = [];
                    product.typeStrings = getContentType(product);
                    product.postStatus = "";
                    product.SKU = "";
                    product.brand = getBrand(product);
                    product.retailer = getRetailer(product);
                    product.retailerPrice = getRetailerPrice(product);
                    product.style = getStyle(product);
                    product.isScalable = getScalable(product);
                    product.couponType = getCouponType(product);
                    product.defaultHeight = getDefaultHeight(product);
                    product.category = getCategory(product);
                    $.each(product.references, function(name, value) {
                        product.vReferences.push({"name": name, "value": value});
                        if(name == "others")
                        {
                          if(value.externalId !== undefined)
                            product.SKU = value.externalId;
                          switch(value.postProcessingStatus){
                            case "1":
                              product.postStatus = "Success";
                              break;
                            case "2":
                              product.postStatus = "Failure";
                              break;
                            default:
                              break;
                          }
                        }
                    });
                });

            }
        }).error(function(data, status, headers, config) {
            robinDialog.alert('server error, please try again.'); // TODO: improve this error handling
        })['finally'](function(){
            $scope.loading = false;
        });
    };

    $scope.onProductSelected = function(productIndex) {
        if (!$scope.loading) {
            $scope.selectedProductIndex = productIndex;
            $scope.selectedProduct = angular.copy($scope.products[productIndex]);
            $scope.getProductLogs($scope.selectedProduct);
        }
    };

    $scope.getProductLogs = function(product) {
        $http({
            method: 'GET',
            url: '/mw/product/log/' + $rootScope.tenantId + '/' + $scope.selectedProduct.id
        }).success(function(data) {
            $scope.selectedProduct.logs = data.logs;
            $scope.selectedProduct.isLogFetched = true;
        }).error(function(data, status, headers, config) {
            if (status != 401) // if the user is unauthorized, don't show the log.
                robinDialog.alert('server error, please try again.'); // TODO: improve this error handling
        });
    };

    // pagination
    $scope.refreshPagination = function(totalProducts) {
        if ($scope.products.length > 0) {
            var totalPages = Math.ceil(totalProducts / $scope.productsPerPage.selected);
            var twbsParams = {
                totalPages: totalPages,
                visiblePages: Math.min(5, totalPages),
                //startPage: 1,
                onPageClick: function (event, page) {
                    $scope.currentPage = page - 1;
                    $scope.refreshSearchResults(false);
                }
            };
            $('#products-pagination').empty();
            $('#products-pagination').removeData("twbs-pagination");
            $('#products-pagination').unbind("page");
            $('#products-pagination').twbsPagination(twbsParams);
        }
    };

    // references click handlers

    $scope.removeReference = function(form, refIndex) {
        $scope.selectedProduct.vReferences.splice(refIndex, 1);
        form.$setDirty(true);
    };

    $scope.addReference = function(form) {
        $scope.selectedProduct.vReferences.push({
            name: $scope.getAvailableRefName(),
            value: ''
        });
        form.$setDirty(true);
    };

    $scope.getAvailableRefName = function() {
        var selectedRefNames = $.map($scope.selectedProduct.vReferences, function(vRef) {return vRef.name});
        for (var i=0 ; i<$scope.refNames.length ; i++) {
            if ($.inArray($scope.refNames[i], selectedRefNames) < 0)
                return $scope.refNames[i];
        }
    };

    $scope.saveReferences = function(form) {
        if (form.$valid) {
            // check for duplicate ref names
            var selectedRefNames = $.map($scope.selectedProduct.vReferences, function(vRef) {return vRef.name});
            if (selectedRefNames.length == $.unique(selectedRefNames).length) {
                // create references data for sending to backend
                var refs = {};
                $.each($scope.selectedProduct.vReferences, function(index, ref) {
                    refs[ref.name] = ref.value;
                });
                $scope.loading = true;
                $http({
                    method: 'PUT',
                    url: '/mw/product/references/' + $rootScope.tenantId + '/' + $scope.selectedProduct.id,
                    data: refs
                }).success(function(data) {
                    robinDialog.alert('Product references has been updated successfully.');
                    $scope.products[$scope.selectedProductIndex].vReferences = angular.copy($scope.selectedProduct.vReferences);
                    form.$setPristine();
                    $scope.loading = false;
                }).error(function(data, status, headers, config) {
                    robinDialog.alert('server error, please try again.'); // TODO: improve this error handling
                    $scope.loading = false;
                });
            } else {
                robinDialog.alert('please select different reference names.');
            }
        } else {
            robinDialog.alert('invalid form fields.');
        }
    };

    $scope.cancelChanges = function(form) {
        $scope.selectedProduct = angular.copy($scope.products[$scope.selectedProductIndex]);
        form.$setPristine();
    };

    $scope.numOfVariations = function(product) {
        return product.numOfVariations != 0 ? product.numOfVariations : "";
    };

    $scope.onBranchChange = function() {
        $location.search({'branch': $scope.searchParams.branch});
    };

    // c'tor
    $q.all([
        TenantIO.getTenantBranch(), // get tenant branch
        $scope.populateData(),
        BrandIO.getBrands(),
        RetailerIO.getRetailers(),
        CategoryIO.getCategoryTree(undefined, $scope.searchParams.branch)
    ]).then(function(results){
        // populate tenant branch
        var tenantBranches = results[0];
        $scope.showBranch = false;
        if (tenantBranches && tenantBranches.children && tenantBranches.children.length > 0) {
            $scope.availableBranches = tenantBranches.children.slice(0);
            $scope.availableBranches.unshift('');
            $scope.showBranch = true;
        }
        $scope.brands = results[2].data;
        $scope.retailers = results[3].data;
        $scope.categoryTree = results[4].data;
        $scope.onSearchButtonClicked();
    });
}]);
