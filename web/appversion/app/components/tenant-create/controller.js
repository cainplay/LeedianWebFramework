robinApp.controller('CtrlCreateTenant', ["$rootScope","$scope","$http",function ($rootScope, $scope, $http) {

    // globals
    $scope.loading = false;
    $scope.formData = {
        newTenant: null,
        fromTenant: $rootScope.tenantId,
        password: null,
        migrateAllProducts: false
    };
    $scope.regexAlphaNumeric = /^[a-z0-9]+$/i;

    $scope.onCreateTenantSubmitted = function() {
        // validate new tenant id
        if ($scope.formData.newTenant.match($scope.regexAlphaNumeric)) {
            $scope.loading = true;
            $http({
                method: 'POST',
                url: '/mw/migrate/tenant/' + $scope.formData.fromTenant + '?newTenantId=' + $scope.formData.newTenant + '&password=' + $scope.formData.password
            }).success(function(data) {
                robinDialog.alert("Tenant '" + data + "' has been created, from tenant '" + $scope.formData.fromTenant + "'.");
                $scope.loading = false;
            }).error(function(data, status, headers, config) {
                if (status == 401)
                    robinDialog.alert(data);
                else
                    robinDialog.alert('server error, please try again.');
                $scope.loading = false;
            });
        } else {
            robinDialog.alert('New tenant ID must be alpha-numeric.');
        }
    }

}]);