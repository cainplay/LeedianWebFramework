robinApp.controller('CtrlAddPackageToTenant', ["$rootScope","$scope","$http",function ($rootScope, $scope, $http) {

    // globals
    $scope.loading = false;
    $scope.formData = {
        packageName: null,
        fromTenant: null,
        toTenant: null,
        password: null,
        guids: null
    };
    $scope.pageData = {
        packages: null
    };
    $scope.succeededCategory = "";
    $scope.failedCategory = "";
    $scope.succeededProducts = "";
    $scope.failedProducts = "";
    $scope.existingProducts = "";
    $scope.notFoundProducts = "";
    $scope.invalidGUIDs = "";


    function validateInput() {

        var valid = true;
        var msg = "";

        if ($scope.guids == "") {
            msg += "you should put guids of products to duplicate";
            valid = false;
        }

        if(msg != "") alert(msg);

        return valid;
    }

    function csvJSON(csv) {

        csv = csv.replace(" ", "");
        csv = csv.replace(/(\r\n|\n|\r)/gm, ",");

        var guids = csv.split(",");

        var result = {"guids": []};

        for(var i = 0; i < guids.length; i++) {
            if (guids[i] != "")
                result.guids.push(guids[i]);
        }

        return result; //JavaScript object
        //return JSON.stringify(result); //JSON
    }

    $scope.onAddPackageSubmitted = function() {

        if (!validateInput()) return;

        var guids = csvJSON($scope.formData.guids);

        $scope.succeededProducts = "";
        $scope.failedProducts = "";
        $scope.existingProducts = "";
        $scope.notFoundProducts = "";

        $scope.loading = true;
        $http({
            method: 'POST',
            url: '/mw/packages/duplicateProducts/' + $rootScope.tenantId +
                '?fromTenant='  + $scope.formData.fromTenant +
                '&toTenant=' + $scope.formData.toTenant +
                '&packageName=' + $scope.formData.packageName +
                '&password=' + $scope.formData.password,
            data: guids
        }).success(function (data) {

            //robinDialog.alert("Duplicated products '" + data + "'.");

            $scope.succeededCategory = "Added category - " + $scope.formData.packageName;

            if (data.succeededProducts.length != 0) {
                $scope.succeededProducts = "Succeeded products: " + data.succeededProducts.length + " - [";
                for (var i = 0; i < data.succeededProducts.length; ++i)
                    $scope.succeededProducts += data.succeededProducts[i] + ", ";
                $scope.succeededProducts = $scope.succeededProducts.substring(0, $scope.succeededProducts.length - 2);
                $scope.succeededProducts += "]";
            }

            if (data.failedProducts.length != 0) {
                $scope.failedProducts = "Failed products: " + data.failedProducts.length + " - [";
                for (i = 0; i < data.failedProducts.length; ++i)
                    $scope.failedProducts += data.failedProducts[i] + ", ";
                $scope.failedProducts = $scope.failedProducts.substring(0, $scope.failedProducts.length - 2);
                $scope.failedProducts += "]";
            }

            if (data.existingProducts.length != 0) {
                $scope.existingProducts = "Already exists products: " + data.existingProducts.length + " - [";
                for (i = 0; i < data.existingProducts.length; ++i)
                    $scope.existingProducts += data.existingProducts[i] + ", ";
                $scope.existingProducts = $scope.existingProducts.substring(0, $scope.existingProducts.length - 2);
                $scope.existingProducts += "]";
            }

            if (data.notFoundProducts.length != 0) {
                $scope.notFoundProducts = "Not found products: " + data.notFoundProducts.length + " - [";
                for (i = 0; i < data.notFoundProducts.length; ++i)
                    $scope.notFoundProducts += data.notFoundProducts[i] + ", ";
                $scope.notFoundProducts = $scope.notFoundProducts.substring(0, $scope.notFoundProducts.length - 2);
                $scope.notFoundProducts += "]";
            }

            $scope.loading = false;
        }).error(function (data, status, headers, config) {
            if (status == 401)
                robinDialog.alert(data);
            else if (status == 500)
                robinDialog.alert(data);
            else
                robinDialog.alert('server error, please try again.');
            $scope.loading = false;
        });

    };

    $scope.onSubmitCSV = function(csv) {
        //console.log(typeof(csv.files))
        $scope.formData.csvFile = csv.files[0]
    }

    $scope.onAddFileSubmitted = function() {
        var map = {}
        $scope.succeededCategory = "";
        $scope.failedCategory = "";
        $scope.succeededProducts = "";
        $scope.failedProducts = "";
        $scope.existingProducts = "";
        $scope.notFoundProducts = "";
        $scope.invalidGUIDs = "";
        Papa.parse($scope.formData.csvFile, {
            complete: function(results) {
                //console.log("Finished:", results.data);
                if (results.data.length > 2000) {
                    robinDialog.alert('Too many products in file, please check.');
                    return 
                }
                
                var invalidGUIDs = [];
                results.data.forEach(function(item) {
                    if (item[0] && $scope.isUUID(item[1])) {
                        map[item[0]]? map[item[0]].push(item[1]): map[item[0]] = [item[1]]
                    } else {
                        invalidGUIDs.push(item[1])
                    }
                })
                //console.log(map)
                if (invalidGUIDs.length > 0) {
                    $scope.invalidGUIDs = "Invalid GUIDs or categories: " + invalidGUIDs;
                    $scope.$digest();
                }
                if (Object.keys(map).length == 0) {
                    robinDialog.alert('No valid UUIDs or catgories found in file, please check.');
                    return
                }
                $scope.succeededProducts = "";
                $scope.failedProducts = "";
                $scope.existingProducts = "";
                $scope.notFoundProducts = "";
                
                var postData = {
                    prods: map
                }
                $scope.loading = true;
                $http({
                    method: 'POST',
                    url: '/mw/packages/duplicateProducts/batch/' + $rootScope.tenantId +
                        '?toTenant='  + $scope.formData.toTenant +
                        '&password=' + $scope.formData.password,
                    data: postData
                }).success(function (data) {

                    //robinDialog.alert("Duplicated products '" + data + "'.");

                    $scope.succeededCategory = "Added category - " + data.succeededCategories;

                    if (data.failedCategories.length != 0) {
                        $scope.failedCategory = "Failed category: " + data.failedCategories;
                    }
                    
                    $scope.succeededProducts = "Succeeded products: " + data.succeededProducts.length;

                    if (data.failedProducts.length != 0) {
                        $scope.failedProducts = "Failed products(assembly is not supported): " + data.failedProducts.length + " - [";
                        for (i = 0; i < data.failedProducts.length; ++i)
                            $scope.failedProducts += data.failedProducts[i] + ", ";
                        $scope.failedProducts = $scope.failedProducts.substring(0, $scope.failedProducts.length - 2);
                        $scope.failedProducts += "]";
                    }

                    if (data.existingProducts.length != 0) {
                        $scope.existingProducts = "Already exists products: " + data.existingProducts.length + " - [";
                        for (i = 0; i < data.existingProducts.length; ++i)
                            $scope.existingProducts += data.existingProducts[i] + ", ";
                        $scope.existingProducts = $scope.existingProducts.substring(0, $scope.existingProducts.length - 2);
                        $scope.existingProducts += "]";
                    }

                    if (data.notFoundProducts.length != 0) {
                        $scope.notFoundProducts = "Not found products: " + data.notFoundProducts.length + " - [";
                        for (i = 0; i < data.notFoundProducts.length; ++i)
                            $scope.notFoundProducts += data.notFoundProducts[i] + ", ";
                        $scope.notFoundProducts = $scope.notFoundProducts.substring(0, $scope.notFoundProducts.length - 2);
                        $scope.notFoundProducts += "]";
                    }

                    $scope.loading = false;
                }).error(function (data, status, headers, config) {
                    if (status == 401)
                        robinDialog.alert(data);
                    else if (status == 500)
                        robinDialog.alert(data);
                    else
                        robinDialog.alert('server error, please try again.');
                    $scope.loading = false;
                });
            }
        });
    }
    
    $scope.isUUID = function(v) {
        return /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i.test(v)
    }
 
    $scope.notEmpty = function(v) {
        return v != "";
    }

}]);