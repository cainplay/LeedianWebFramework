robinApp.controller('CtrlEditFamily',
    ['$rootScope', '$scope', '$location', '$filter', 'FamilyIO', 'ProductIO',
    function ($rootScope, $scope, $location, $filter, FamilyIO, ProductIO) {

    var $searchObject = $location.search();
    $scope.familyGuid = $searchObject["guid"]
    // $scope.familyGuid = '35810399-326e-4df2-ac5c-aab3cd3741a1';
    $scope.family = null;

    // Get the flat families id list in the root node
    var getFamilyIds = function (root) {
        var targets = TreeVisitor.findAll(root, function (node) {
            return true;
        });

        return targets.map(function (node) { return node.id; });
    };

    // globals
    $scope.s3IconPathCategory = robinConsts.s3IconPathCategory;
    $scope.extTimestamp = '';

    $scope.loading = 0;
    $scope.statuses = [
        {id: 0, description: "Deleted"},
        {id: 1, description: "Active"},
        {id: 2, description: "Hidden"},
        {id: 3, description: "Private"}
    ];

    $scope.populateData = function() {
        if (!$scope.familyGuid) return;

        $scope.resetForm();

        // find the family node with the same guid in the family tree
        var findNode = function (root, guid) {
            var targets = TreeVisitor.findFirst(root, function (node) {
                return node.id == guid;
            });

            if (targets.length > 0) {
                return targets[0];
            }

            return undefined;
        };

        $scope.loading = true;
        FamilyIO.getFamilyTree().then(
            function (res) {
                $scope.loading = false;

                $scope.rootFamilies = res.data;
                $scope.family = findNode($scope.rootFamilies, $scope.familyGuid);

                if ($scope.family) {
                    $scope.makeIconUrl();

                    if ($scope.family.children && $scope.family.children.length > 0) {
                        var familyTreeData = $scope.family.children;
                        initFamilyTreeDataAttrs(familyTreeData);
                        initFamilyTree($scope.familyTreeSelector, familyTreeData);
                    }
                }

                $scope.searchProductByFamily();
            },
            function (res) {
                robinDialog.alert('server error, please try again.'); // TODO: improve this error handling
                $scope.loading = false;
            }
        );
    };

    $scope.searchProductByFamily = function() {
        $scope.products = [];
        $scope.selectedProductsCount = 0;
        $scope.refreshSearchResults(true);
    };

    //---------------------------------------------------------------------------



    //---------------------------------------------------------------------------
    // products list
    $scope.productsPerPage = {
        selected: 10,
        options: [10, 20, 50, 100]
    };
    $scope.currentPage = 0;
    $scope.onItemsOnPageChanged = function() {
        $scope.currentPage = 0;
        $scope.refreshSearchResults(true);
    };
    $scope.products = [];
    $scope.selectedProductsCount = 0;
    $scope.refreshSearchResults = function(isRefreshPagination) {
        if (!$scope.family) return;

        $scope.loading = true;
        var param = {
            offset: $scope.currentPage * $scope.productsPerPage.selected,
            limit: $scope.productsPerPage.selected,
            categoryIds: getFamilyIds($scope.family)
        }
        ProductIO.searchProducts(param).then(function (res){
            $scope.loading = false;

            var data = res.data;
            if (data) {
                $scope.totalProductsCount = data.total;
                $scope.products = data.products;

                // init products ui data
                $.each($scope.products, function(index, product) {
                    product.updatedFamilyIds = product.familyIds ? product.familyIds.slice() : [];
                    product.isSelected = false;
                });

                if (isRefreshPagination) {
                    $scope.refreshPagination($scope.totalProductsCount);
                }
            }
        }, function (res) {
            $scope.loading = false;
            robinDialog.alert('server error, please try again.');
        });
    };

    // sets a product as selected, called both from the ui click handler and upon 'toggle all' action
    $scope.onProductChecked = function(product, check) {
        if (!$scope.loading) {

            product.isSelected = check ? check.isOn : !product.isSelected;
            $scope.selectedProductsCount += product.isSelected ? 1 : -1;

            $scope.toggleFamilyTree($scope.selectedProductsCount);
            $scope.updateFamiliesSelection();
        }
    };

    // all products select/deselect
    $scope.onToggleAllProductsSelection = function(toggle) {
        var check = {isOn: toggle};
        $.each($scope.products, function(index, product) {
            if (product.isSelected != toggle)
                $scope.onProductChecked(product, check);
        });
    };


    // pagination
    $scope.refreshPagination = function(totalProducts) {
        if ($scope.products.length > 0) {
            var totalPages = Math.ceil(totalProducts / $scope.productsPerPage.selected);
            var twbsParams = {
                totalPages: totalPages,
                visiblePages: Math.min(5, totalPages),
                //startPage: 1,
                onPageClick: function (event, page) {
                    $scope.currentPage = page - 1;
                    $scope.refreshSearchResults(false);
                }
            };
            $('#products-pagination').empty();
            $('#products-pagination').removeData("twbs-pagination");
            $('#products-pagination').unbind("page");
            $('#products-pagination').twbsPagination(twbsParams);
        }
    };

    $scope.openDeleteDialog = function () {
        var onDeleteConfirmed = function () {
            $scope.closeCurrentDialog();
            $scope.deleteSelectedProductsFromFamily();
        };

        // Get select products id
        var ids = [];
        $scope.products.forEach(function (product) {
            if (product.isSelected) {
                ids.push(product.id);
            }
        });

        var msg = $filter('translate')('delete_confirm');
        msg += " " + ids.join(", ") + " ?";
        $scope.openConfirmationDialog('editfamily_delete_product', msg, onDeleteConfirmed);
    };

    $scope.openAddDialog = function () {

        $scope.currentModalSelector = '#add-product-modal';

        var onSaveChanges = function () {
            if ($scope.addProductPopup.guid == "") {
                robinDialog.alert('Product GUID is empty.');
                return;
            }

            $scope.closeCurrentDialog();

            var familyId = $scope.family.id;
            $scope.addProductToFamily(familyId, $scope.addProductPopup.guid);
        };

        // save data on the popup for later use
        $scope.addProductPopup = {
            popupTitle: 'editfamily_add_product',
            popupSaveText: 'editfamily_add',
            guid: '',
            onConfirm: onSaveChanges
        };

        $($scope.currentModalSelector).modal('show');
    };

    // called after 'ok' is clicked at 'delete family' popup
    $scope.deleteSelectedProductsFromFamily = function () {
        var selectedProducts = $scope.products.filter(function(product) {
            return product.isSelected;
        });

        // Remove family id and its children family ids from its reference list
        var ids = getFamilyIds($scope.family);
        selectedProducts.forEach(function (product) {
            ids.forEach(function(id) {
                product.updatedFamilyIds.splice($.inArray(id, product.updatedFamilyIds), 1);
            })
        });

        $scope.updateProductsFamilies(selectedProducts).then(function (res) {
            // Remove them from current scope products
            selectedProducts.forEach(function (product) {
                $scope.products.splice($.inArray(product, $scope.products), 1);
            });

            $scope.updateFamiliesSelection();

            robinDialog.alert('Delete products successfully.');
        }, function (res) {
            robinDialog.alert('server error, please try again.');
        });
    };

    $scope.addProductToFamily = function (familyId, productId) {
        if (productId != "") {
            ProductIO.searchProducts({guid: productId}).then(function (res) {
                var data = res.data;
                if (data.products.length > 0) {

                    // Add current family id to its families list
                    data.products.forEach(function(product) {
                        product.updatedFamilyIds = product.familyIds ? product.familyIds.slice() : [];
                        if ($.inArray(familyId, product.updatedFamilyIds) < 0) {
                            product.updatedFamilyIds.push(familyId);
                        }
                    });

                    $scope.updateProductsFamilies(data.products).then(function (res) {
                        $scope.totalProductsCount = $scope.totalProductsCount + data.products.length;
                        $scope.products = $scope.products.concat(data.products);

                        $.each(data.products, function(index, product) {
                            $scope.onProductChecked(product, {isOn: true});
                        });

                        robinDialog.alert('Add product successfully.');
                    }, function (res) {
                        robinDialog.alert('server error, please try again.');
                    });
                } else {
                    robinDialog.alert("The product: " + id + "doesn't exists.");
                }
            }, function (res) {
                robinDialog.alert("Server error, please try again.");
            });
        }
    };

    $scope.updateProductsFamilies = function (products) {
        var updatedProducts = {};
        products.forEach(function (product) {
            updatedProducts[product.id] = product.updatedFamilyIds;
        });

        $scope.loading = true;

        return ProductIO.updateProductFamilies(updatedProducts).then(function (res) {
                // Update the product family ids
                products.forEach(function (product) {
                    product.familyIds = product.updatedFamilyIds ? product.updatedFamilyIds.slice() : [];
                });

                $scope.loading = false;
            }, function (res) {
                $scope.loading = false;
            }
        );
    };

    $scope.onEditFamilySave = function () {

        var family = $scope.family;
        var postData = {
            id: family.id,
            name: family.title,
            logo: family.logo,
            status: family.status,
            parentId: family.parentId
        };

        $scope.loading = true;

        FamilyIO.updateFamily(postData, family.id).then(function (res) {
            $scope.closeCurrentDialog();

            robinDialog.alert('family has been updated.');

            $scope.loading = false;
        }, function (res) {
            robinDialog.alert('server error, please try again.');

            $scope.loading = false;
        });
    };

    $scope.onEditFamilyTreeSave = function () {
        var selectedProducts = $scope.products.filter(function(product) {
            return product.isSelected;
        });
        $scope.updateProductsFamilies(selectedProducts).then(function (res) {
            robinDialog.alert("Product families has been updated.");
        }, function (res) {
            robinDialog.alert('server error, please try again.');
        });
    };

    $scope.onEditFamilyTreeCancle = function () {
        // Reset the product family ids
        $scope.products.forEach(function (product) {
            product.updatedFamilyIds = product.familyIds ? product.familyIds.slice() : [];
        });

        $scope.updateFamiliesSelection();
    };

    /*
    * called after selecting an icon to upload.
    * server will upload the icon to a temp location, and will return the location
    */
    $scope.submitIconForm = function (formId, progressSelector) {

        $scope.family.fileTooBig = false;

        $scope.loading = true;

        var params = [];

        var subtype;
        if (formId == 'file-logo-upload-form') {
            subtype = ""; // lagecy code: The logo is used by mobile and named as 'category-id' by default.
        } else if (formId == 'file-icon-upload-form') {
            subtype = "icon";
        } else if (formId == 'file-thumbnail-upload-form') {
            subtype = "thumbnail";
        }
        if (subtype) {
            params.push("subtype=" + subtype)
        }

        var key = $scope.family.id;
        if (key) {
            params.push("key=" + key);
        }

        query = params.join("&")
        var url = '/mw/upload/icon/' + $rootScope.tenantId + '/category';
        if (query != "") {
            url = url + '?' + query;
        }

        var progressSelector = '.progress-upload-icon';
        robinCommon.submitFilesForm(
            '#' + formId, // form selector
            progressSelector,
            url,
            // on success
            function (data) {
                $scope.family.logo = data.tempKey;

                $scope.makeIconUrl();

                $(progressSelector).hide();

                $scope.loading = false;
            },
            // on error
            function (data) {
                if (data.status == 400) {
                    $scope.family.fileTooBig = true;
                } else {
                    robinDialog.alert('Unable to upload icon, error: ' + data.statusText);
                }

                $(progressSelector).hide();

                $scope.loading = false;
            },
            // on before send
            function () {
                $(progressSelector).show();
            },
            // on during upload
            function (e) {
                if (e.lengthComputable) {
                    $('progress').attr({
                        value: e.loaded,
                        max: e.total
                    });
                }
            }
        );
    };

    $scope.clearIcon = function () {
        $scope.family.logo = "";
        $scope.family.logoUrl = null;
    };

    $scope.makeIconUrl = function () {
        if ($scope.family.logo) {
            $scope.family.logoUrl = $scope.s3IconPathCategory + $scope.family.logo + ".png";
        }
    };

    $scope.resetForm = function () {
        $('.progress-upload-icon').hide();
    };


    //---------------------------------------------------------------------------
    /*
     * families tree
     */
    $scope.familyTreeSelector = '.families-tree';
    $scope.familyTreeObject = null;
    $scope.familyTree = null;
    $scope.familyTreeRoot = null;
    $scope.isFamilyTreeDirty = false;


    $scope.updateFamiliesSelection = function() {
        var mapFamilyProductsCount = $scope.createFamilyProductsCountMap();
        $scope.updateFamilyTreeSelection(mapFamilyProductsCount);
        $scope.updateFamilyTreeUI(mapFamilyProductsCount);
    };

    $scope.updateSelectedProductsFamilyIds = function(familyId, checked) {
        $.each($scope.products, function(index, product) {
            if (product.isSelected) {
                if (checked) {
                    if ($.inArray(familyId, product.updatedFamilyIds) < 0) {
                        product.updatedFamilyIds.push(familyId);
                    }
                } else {
                    product.updatedFamilyIds.splice($.inArray(familyId, product.updatedFamilyIds), 1);
                }
            }
        });
        $scope.updateFamilyTreeUI($scope.createFamilyProductsCountMap());
    };

    // returns a map from family id to num of selected products with that category
    $scope.createFamilyProductsCountMap = function() {
        var mapFamilyProductsCount = {};
        $.each($scope.products, function(index, product) {
            if (product.isSelected) {
                $.each(product.updatedFamilyIds, function(index, famId) {
                    var famCount = mapFamilyProductsCount[famId];
                    mapFamilyProductsCount[famId] = famCount ? famCount + 1 : 1;
                });
            }
        });
        return mapFamilyProductsCount;
    };

    // for each tree node, set its selection state, according to the selected products
    $scope.updateFamilyTreeSelection = function(mapFamilyProductsCount) {
        $scope.familyTreeRoot.visit(function(node) {
            var nd = node.data;
            // set the node's selection mode
            $scope.familyTree.selectKey(nd.key, mapFamilyProductsCount[nd.id] >= 0);
        });
    };

    // for each tree node, set the title, style, and icon
    $scope.updateFamilyTreeUI = function(mapFamilyProductsCount) {
        $scope.familyTreeRoot.visit(function(node) {
            var nd = node.data;
            // how many products have this node
            nd.count = mapFamilyProductsCount[nd.id];
            // set the node's title
            nd.title = nd.count ? nd.name + ' (' + nd.count + '/' + $scope.selectedProductsCount + ')' : nd.name;
            // set the node's icon
            nd.addClass = '';
            if ((nd.isParent && (0 < nd.count)) ||
                (!nd.isParent && (0 < nd.count && nd.count < $scope.selectedProductsCount))) {
                nd.addClass = 'dynatree-partsel';
            }
        });
        $scope.familyTree.redraw();
    };

    // add extra tree attributes to raw data
    function initFamilyTreeDataAttrs(nodesList) {
        $(nodesList).each(function(index, node) {
            node.count = 0; // how many products are tagged with this category
            node.name = node.title; // save for later use
            if (node.children && node.children.length > 0) {
                //node.isFolder = true;
                node.isParent = true; // mark for later use
                //node.hideCheckbox = true;
                node.expand = true;
                //node.unselectable = true;
                initFamilyTreeDataAttrs(node.children);
            }
        });
    }

    // init dynatree with data
    function initFamilyTree(rootSelector, data) {

        $(rootSelector).dynatree({
            onSelect: function(flag, node) {
                if (this.phase == 'userEvent') { // prevent actions from external events
                    $scope.updateSelectedProductsFamilyIds(node.data.id, node.bSelected);
                    $scope.isFamilyTreeDirty = true;
                    $scope.$digest();
                }
            },
            //persist: true,
            imagePath:" ",
            checkbox: true,
            selectMode: 3,
            clickFolderMode: 1,
            children: data
        });

        // init scope vars
        $scope.familyTreeObject = $(rootSelector);
        $scope.familyTree = $(rootSelector).dynatree("getTree");
        $scope.familyTreeRoot = $(rootSelector).dynatree("getRoot");

        // disable tree
        $scope.familyTree.disable(); // TODO: refactor this mechanism

    }

    $scope.toggleFamilyTree = function(enable) {
        $scope.familyTreeObject.dynatree(enable > 0 ? "enable" : "disable");
    };


    $scope.openConfirmationDialog = function(title, message, onConfirm, okText, cancelText) {
        $scope.currentModalSelector = '#confirmation-modal';
        $scope.confirmationPopup = {
            title: title,
            message: message,
            onConfirm: onConfirm,
            okText: okText ? okText : 'ok',
            cancelText: cancelText ? cancelText : 'cancel'
        };
        $($scope.currentModalSelector).modal('show');
    };

    $scope.closeCurrentDialog = function() {
        $($scope.currentModalSelector).modal('hide');
    };

    $scope.cancelChanges = function(form) {
        $scope.selectedFamily = angular.copy($scope.families[$scope.selectedFamilyIndex]);
        form.$setPristine();
    };

    $scope.populateData();
}]);
