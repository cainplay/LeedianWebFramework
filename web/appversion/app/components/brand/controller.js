robinApp.controller('CtrlBrands', ['$rootScope', '$scope', '$filter', 'LocaleService','BrandIO', function ($rootScope, $scope, $filter, LocaleService, BrandIO) {

    // declaring vars used by angular
    $scope.isPageDataLoaded = false;
    $scope.loading = false;
    $scope.activeBrand = null;
    $scope.editBrandPopup = null;
    $scope.confirmationPopup = null;
    $scope.extTimestamp = '';
    $scope.refreshImage = false;

    /*
     * functions handling brands tree
     */

    $scope.cTreeSelector = '.tree-container';
    $scope.cTreeObject = null;
    $scope.cTree = null;
    $scope.cTreeRoot = null;

    // get tree data from server and init dynatree with that data
    $scope.refreshBrandsTree = function() {
        BrandIO.getBrands().success(function(data) {
            $scope.updateTreeData(data);
            $scope.isPageDataLoaded = true;
        }).error(function(data, status, headers, config) {
            robinDialog.alert('server error, please try again.');
            $scope.loading = false;
        });
    };

    $scope.updateTreeData = function(treeData) {
        $scope.initTreeDataAttrs(treeData); // attach needed attrs to raw data
        $scope.initTree($scope.cTreeSelector, treeData);
    };

    // add extra tree attributes to raw data
    $scope.initTreeDataAttrs = function(nodesList) {
        $(nodesList).each(function(index, node) {
            if (node.children && node.children.length > 0) {
                //node.isFolder = true;
                //node.expand = true;
                $scope.initTreeDataAttrs(node.children);
            }
        });
    };

    // init dynatree with data
    $scope.initTree = function(rootSelector, data) {

        $(rootSelector).dynatree({
            onActivate: function(node) {
                $scope.activeBrand = node.data;
                $scope.$digest();
            },
            //persist: true,
            dnd: {
                preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
                onDragStart: function(node) {
                    // enable / disable the drag and drop
                    return $scope.editTreeOrder.isEditable; // TODO: remove this dependency
                },
                onDragEnter: function(node, sourceNode) {
                    // Prevent dropping a parent below another parent (only sort nodes under the same parent)
                    if(node.parent !== sourceNode.parent){
                        return true;
                    }
                    // Don't allow dropping *over* a node (would create a child)
                    return ['before', 'after'];
                },
                onDrop: function(node, sourceNode, hitMode, ui, draggable) {
                    sourceNode.move(node, hitMode);
                    // TODO: expand node here
                }
            },
            clickFolderMode: 1,
            children: data
        });

        // init scope vars
        $scope.cTreeObject = $(rootSelector);
        $scope.cTree = $(rootSelector).dynatree("getTree");
        $scope.cTreeRoot = $(rootSelector).dynatree("getRoot");

    };

    /*
     * tree updates
     */

    $scope.addBrandNode = function(parentNode, id, title, link, url) {
        var node = parentNode ? parentNode : $scope.cTreeRoot;
        var childNode = node.addChild({
            id: id,
            title: title,
            link: link,
            url: url,
            parentId: node.data.id
        });
        node.expand(true);
    };

    $scope.updateBrandNode = function(node, title, link, url) {
        node.data.title = title;
        node.data.link = link;
        node.data.url = url;
        node.render();
        $scope.refreshImages();
    };

    $scope.removeBrandNode = function(node) {
        node.remove();
        $scope.activeBrand = null;
    };

    /*
     * functions handling popups
     */

    $scope.currentModalSelector = null; // saves current modal

    // open the edit popup, and save data for use after 'ok' is clicked
    // if brand is provided, then open 'edit' dialog, otherwise open 'add' dialog
    $scope.openEditDialog = function(brand) {

        $scope.currentModalSelector = '#edit-brand-modal';
        $scope.resetForm();
        var curNode = $scope.cTreeObject.dynatree("getActiveNode");

        var onSaveChanges = function() {
            if (brand) {
                $scope.editBrand(curNode);
            } else {
                //$scope.addBrand(curNode, ecp.cTitle, ecp.cLink, ecp.cUrl);
                $scope.addBrand(null); // don't pass curNode as a parent
            }
        };

        // save data on the popup for later use
        $scope.editBrandPopup = {
            popupTitle: (brand ? 'brands_edit_brand' : 'brands_add_brand'),
            popupSaveText: brand ? 'brands_save_changes' : 'brands_add_brand',
            cId: brand ? brand.id : null,
            cParentId: brand ? brand.parentId : null,
            cTitle: brand ? brand.title : null,
            cLink: brand ? brand.link : null,
            cUrl: brand ? brand.url : null,
            onConfirm: onSaveChanges
        };

        $($scope.currentModalSelector).modal('show');

    };

    $scope.resetForm = function() {
        $('#file-upload-form').get(0).reset();
        $('.progress-upload-icon').hide();
    };

    $scope.openDeleteDialog = function(brand) {
        var onDeleteConfirmed = function() {
            $scope.deleteBrand($scope.cTreeObject.dynatree("getActiveNode"), brand.id);
        };

        var msg = $filter('translate')('delete_confirm');
        $scope.openConfirmationDialog('brands_delete_brand', msg + ' "' + brand.title + '"?', onDeleteConfirmed);
    };

    $scope.openConfirmationDialog = function(title, message, onConfirm, okText, cancelText) {
        $scope.currentModalSelector = '#confirmation-modal';
        $scope.confirmationPopup = {
            title: title,
            message: message,
            onConfirm: onConfirm,
            okText: okText ? okText : 'ok',
            cancelText: cancelText ? cancelText : 'cancel'
        };
        $($scope.currentModalSelector).modal('show');
    };

    $scope.closeCurrentDialog = function() {
        $($scope.currentModalSelector).modal('hide');
    };

    /*
     * action handlers
     */

    /*
     * called after selecting an icon to upload.
     * server will upload the icon to a temp location, and will return the location
     */
    $scope.submitIconForm = function() {

        $scope.editBrandPopup.fileTooBig = false;
        $scope.setLoading(true);
        var progressSelector = '.progress-upload-icon';
        robinCommon.submitFilesForm(
            '#file-upload-form', // form selector
            progressSelector,
            // url for sending the form
            '/mw/upload/icon/' + $rootScope.tenantId + '/brand',
            // on success
            function(data) {
                $scope.editBrandPopup.cUrl = data.iconUrl;
                $scope.refreshImages(true);
                $scope.setLoading(false);
            },
            // on error
            function(data) {
                if (data.status == 400) {
                    $scope.editBrandPopup.fileTooBig = true;
                } else {
                    robinDialog.alert('Unable to upload icon, error: ' + data.statusText);
                }
                $(progressSelector).hide();
                $scope.setLoading(false);
            },
            // on before send
            function() {
                $(progressSelector).show();
            },
            // on during upload
            function(e) {
                if (e.lengthComputable){
                    $('progress').attr({
                        value: e.loaded,
                        max: e.total
                    });
                }
            }
        );

    };

    $scope.clearIcon = function() {
        $scope.editBrandPopup.cUrl = null;
    };

    // called after 'ok' is clicked at 'add brand' popup
    $scope.addBrand = function(parentNode) {

        var ecp = $scope.editBrandPopup;

        if (ecp.cTitle) {

            // clear errors
            ecp.titleRequired = false;

            var postData = {
                name: ecp.cTitle,
                link: ecp.cLink,
                url: ecp.cUrl,
                parentId: parentNode ? parentNode.data.id : null
            };

            $scope.loading = true;
            BrandIO.addBrand(postData).success(function(dto, status, headers, config) {
                // update tree
                $scope.addBrandNode(parentNode, dto.id, ecp.cTitle, ecp.cLink, dto.url ? dto.url : null);
                // message the user
                robinDialog.alert('Brand has been added.');
                // close dialog
                $scope.closeCurrentDialog();
                $scope.loading = false;
            }).error(function(data, status, headers, config) {
                robinDialog.alert('server error, please try again.');
                $scope.loading = false;
            });

        } else {
            // set error
            ecp.titleRequired = true;
        }

    };

    // called after 'ok' is clicked at 'edit brand' popup
    $scope.editBrand = function(node) {

        var ecp = $scope.editBrandPopup;

        var postData = {
            id: ecp.cId,
            name: ecp.cTitle,
            link: ecp.cLink,
            url: ecp.cUrl,
            parentId: ecp.cParentId
        };

        $scope.loading = true;
        BrandIO.updateBrand(postData, ecp.cId).success(function(dto, status, headers, config) {
            // update tree
            $scope.updateBrandNode(node, ecp.cTitle, ecp.cLink ? ecp.cLink : null, dto.url ? dto.url : null);
            // message the user
            robinDialog.alert('Brand has been updated.');
            // finish the flow
            $scope.closeCurrentDialog();
            $scope.loading = false;
        }).error(function(data, status, headers, config) {
            robinDialog.alert('server error, please try again.');
            $scope.loading = false;
        });

    };

    // called after 'ok' is clicked at 'delete brand' popup
    $scope.deleteBrand = function(node, id) {
        $scope.loading = true;
        BrandIO.deleteBrand(id).success(function(data, status, headers, config) {
            // update tree
            $scope.removeBrandNode(node);
            // message the user
            robinDialog.alert('Brand has been deleted.');
            // finish the flow
            $scope.closeCurrentDialog();
            $scope.loading = false;
        }).error(function(data, status, headers, config) {
            robinDialog.alert('server error, please try again.');
            $scope.loading = false;
        });
    };

    /*
     * functions handling tree order editing
     */

    $scope.editTreeOrder = {

        isEditable: false,

        onEditOrder: function() {
            // save current tree data
            this.curTreeData = $scope.cTree.toDict();
            // enter edit mode
            this.isEditable = true;
        },

        onDoneEditingOrder: function() {
            // save tree order
            var that = this;
            $scope.loading = true;

            var buildCatTree = function(nodes) {
                var catTree = [];
                $.each(nodes, function(index, node) {
                    catTree.push({
                        id: node.id,
                        children: node.children ? buildCatTree(node.children) : []
                    });
                });
                return catTree;
            };
            var tree = buildCatTree($scope.cTree.toDict());

            BrandIO.updateBrands(tree).success(function(data, status, headers, config) {
                // exit edit mode
                that.isEditable = false;
                $scope.loading = false;
            }).error(function(data, status, headers, config) {
                robinDialog.alert('server error, please try again.'); // TODO: improve this error handling
                $scope.loading = false;
            });
        },

        onCancelEditOrder: function() {
            // revert tree structure
            $scope.updateTreeData(this.curTreeData);
            $scope.cTree.reload();
            this.isEditable = false;
        },

        sortTreeByTitle: function() {
            var sorter = LocaleService.localeStringSort(undefined, function(node) {
                return node.data.title;
            });
            $scope.cTree.getRoot().sortChildren(sorter, true);
        }
    }

    /*
     * utils
     */

    $scope.setLoading = function(toggle) {
        $scope.loading = toggle;
        $scope.$digest();
    };

    $scope.refreshImages = function(digest) {
        $scope.extTimestamp = '?' + (new Date().getTime());
        if (digest)
            $scope.$digest();
    };

    /*
     * c'tor
     */
    $scope.refreshBrandsTree();

}]);
