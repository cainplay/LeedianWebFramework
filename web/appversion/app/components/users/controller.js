robinApp.controller('CtrlUsers', ["$scope", "$http", "$q", 'TenantIO', function($scope, $http, $q, TenantIO) {

    $scope.isPageDataLoaded = false;
    $scope.users = [];
    $scope.tenants = [];
    $scope.roles = [];
    
    $scope.onLoad = function() {
        $scope.loadRoles();
        $scope.loadTenants();
        $scope.loadUsers();
    };
    
    $scope.loadUsers = function() {
        $http({
            method : 'GET',
            url : '/mw/users/all',
            headers : {
                'Content-Type' : 'application/json'
            }
        }).success(function(data) {
            // return data contains "er" code
            // -1 mean success
            if (data.er == -1) {
                // value is Json array
                data.users.forEach(function(user) {
                    // model to bind tenant
                    if (!user.tenants) {
                        //no tenant assigned
                        user.tenants = []
                    }
                    $scope.users.push(user)
                })
                // sort users
                $scope.users.sort(function(a,b) 
                        { return a.email.localeCompare(b.email)} 
                );
                $scope.isPageDataLoaded = true;
            } else {
                // data.erMessage contains error
                robinDialog.alert(data.erMessage);
                $scope.isPageDataLoaded = true;
            }
        }).error(function(data, status, headers, config) {
            robinDialog.alert('Server error, please try again.');
        });
    };

    $scope.loadTenants = function() {
        TenantIO.getTenants().success(function(data) {
            // return data contains "er" code
            // -1 mean success
            if (data.er == -1) {
                // value is Json array
                $scope.tenants = data.tenants; 
            } else {
                // data.erMessage contains error
                robinDialog.alert(data.erMessage);
            }
        }).error(function(data, status, headers, config) {
            robinDialog.alert('Server error, please try again.');
        });
    };

    $scope.loadRoles = function() {
        $http({
            method : 'GET',
            url : '/mw/roles',
            headers : {
                'Content-Type' : 'application/json'
            }
        }).success(function(data) {
            // return data contains "er" code
            // -1 mean success
            if (data.er == -1) {
                // value is Json array
                $scope.roles = data.roles; 
            } else {
                // data.erMessage contains error
                robinDialog.alert(data.erMessage);
            }
        }).error(function(data, status, headers, config) {
            robinDialog.alert('Server error, please try again.');
        });
    } ;

    $scope.saveUser = function(selectData, user) {
      // for multi select, data.tenant is array
      var d = $q.defer();
      var postData = {
              userId: user.userId,
              role: selectData.role,
              tenants: selectData.tenant
          };
      $http({
          method : 'PUT',
          url : '/mw/user',
          headers : {
              'Content-Type' : 'application/json'
          },
          data : postData
      }).success(function(data) {
          // return data contains "er" code
          // -1 mean success
          if (data.er == -1) {
              d.resolve();
          } else {
              // data.erMessage contains error
              robinDialog.alert(data.erMessage);
              d.resolve(false);
          }
      }).error(function(data, status, headers, config) {
          robinDialog.alert('Server error, please try again.');
          d.resolve("error");
      });
      return d.promise;
    };

    // remove user
    $scope.deleteUser = function(index, user) {
        robinDialog.showConfirmation(
            'Delete User', 'Are you sure you want to delete "' + user.email + '"?', 'Delete', 'Cancel',
            function() {
                robinDialog.dismissDialog();
                $http({
                    method: 'DELETE',
                    url: '/mw/user/' +  user.userId
                }).success(function(data, status, headers, config) {
                    if (data.er == -1) {
                        $scope.users.splice(index, 1);
                        return true
                    } else {
                        // data.erMessage contains error
                        //robinDialog.alert(data.erMessage);
                        alert(data.erMessage)
                    }
                }).error(function(data, status, headers, config) {
                    //robinDialog.alert('Server error, please try again.');
                    alert('Server error, please try again.')
                });
            })
    };

    $scope.openDialog = function() {
        $scope.currentModalSelector = '#add-user-modal';
        $scope.errorMessage = null;
        $scope.posting = false;
        $scope.formData = {
                email: null,
                password1: null,
                password2: null
        };
        $($scope.currentModalSelector).modal('show');
    };
    
    $scope.onSubmit = function() {
        if ($scope.formData.password1 != $scope.formData.password2) {
            // show error
            $scope.errorMessage = "Password does not match.";
            return;
        }
        $scope.errorMessage = null;
        $scope.posting = true;
        var postData = {
                email: $scope.formData.email,
                password: $scope.formData.password1
        };
        $http({
            method: 'POST',
            url: '/mw/signup',
            headers: {
                'Content-Type': 'application/json'
            },
            data: postData
        }).success(function(data) {
            // return data contains "er" code
            // -1 mean success
            $scope.posting = false;
            if (data.er == -1) {
                // register OK, reload users
                $($scope.currentModalSelector).modal('hide');
                $scope.users = [];
                $scope.loadUsers();
                return true;
            } else {
                // data.erMessage contains error
                $scope.errorMessage = data.erMessage;
                return false
            }
        }).error(function(data, status, headers, config) {
            $scope.posting = false;
            $scope.errorMessage = 'Server error, please try again.';
            return false;
        });
    };
    // initialize
    $scope.onLoad();
}]);
