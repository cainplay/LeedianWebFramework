robinApp.controller('CtrlFamilies',['$rootScope', '$scope', '$filter', 'FamilyIO',
  function ($rootScope, $scope, $filter, FamilyIO) {

  // globals
  $scope.s3IconPathCategory = robinConsts.s3IconPathCategory;

  // declaring vars used by angular
  $scope.isPageDataLoaded = false;
  $scope.loading = false;
  $scope.activeCategory = null;
  $scope.editCategoryPopup = null;
  $scope.confirmationPopup = null;
  $scope.extTimestamp = '';
  $scope.refreshImage = false;
  $scope.haveTranslations = false;
  $scope.uploadingCsv = false;

  $scope.languages = robinConsts.languages;
  $scope.language = $scope.languages[0];
  $.each($scope.languages, function (index, l) {
    if (l.code == robinConsts.lang)
      $scope.language = l;
  });

  $scope.statuses = [
      { id: 0, description: "Deleted" },
      { id: 1, description: "Active" },
      { id: 2, description: "Hidden" },
      { id: 3, description: "Private" },
      { id: 4, description: "Group" }
  ];

  $scope.translations = null;

  /*
   * functions handling categories tree
   */

  $scope.cTreeSelector = '.tree-container';
  $scope.cTreeObject = null;
  $scope.cTree = null;
  $scope.cTreeRoot = null;

  // get tree data from server and init dynatree with that data
  $scope.refreshCategoriesTree = function () {
    FamilyIO.getFamilyTree($scope.language.code).then(function (data) {
      $scope.updateTreeData(data);
      $scope.isPageDataLoaded = true;
    }, function (data, status, headers, config) {
      robinDialog.alert('server error, please try again.');
      $scope.loading = false;
    });
  };

  $scope.updateTreeData = function (treeData) {
    $scope.initTreeDataAttrs(treeData); // attach needed attrs to raw data
    $scope.initTree($scope.cTreeSelector, treeData);
  };

  // add extra tree attributes to raw data
  $scope.initTreeDataAttrs = function (nodesList) {
    $(nodesList).each(function (index, node) {
      if (node.children && node.children.length > 0) {
        //node.isFolder = true;
        //node.expand = true;
        $scope.initTreeDataAttrs(node.children);
      }
    });
  };

  // init dynatree with data
  $scope.initTree = function (rootSelector, data) {

    $(rootSelector).dynatree({
      onActivate: function (node) {
        $scope.activeCategory = node.data;
        $scope.$digest();
      },
      //persist: true,
      dnd: {
        preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
        onDragStart: function (node) {
          // enable / disable the drag and drop
          return $scope.editTreeOrder.isEditable; // TODO: remove this dependency
        },
        onDragEnter: function (node, sourceNode) {
          // Prevent dropping a parent below another parent (only sort nodes under the same parent)
          if (node.parent !== sourceNode.parent) {
            return true;
          }
          //return ['before', 'after']; // Don't allow dropping *over* a node (would create a child)
          return true;
        },
        onDrop: function (node, sourceNode, hitMode, ui, draggable) {
          sourceNode.move(node, hitMode);
          // TODO: expand node here
        }
      },
      clickFolderMode: 1,
      children: data
    });

    // init scope vars
    $scope.cTreeObject = $(rootSelector);
    $scope.cTree = $(rootSelector).dynatree("getTree");
    $scope.cTreeRoot = $(rootSelector).dynatree("getRoot");

  };

  /*
   * tree updates
   */

  $scope.addCategoryNode = function (parentNode, id, title, logo, status) {
    var node = parentNode ? parentNode : $scope.cTreeRoot;
    var childNode = node.addChild({
      id: id,
      title: title,
      logo: logo,
      status: status,
      parentId: node.data.id
    });
    node.expand(true);
  };

  $scope.updateCategoryNode = function (node, title, logo, status) {
    node.data.title = title;
    node.data.logo = logo;
    node.data.status = status;
    node.render();
    $scope.refreshImages();
  };

  $scope.removeCategoryNode = function (node) {
    node.remove();
    $scope.activeCategory = null;
  };

  /*
   * functions handling popups
   */

  $scope.currentModalSelector = null; // saves current modal

  // open the edit popup, and save data for use after 'ok' is clicked
  // if category is provided, then open 'edit' dialog, otherwise open 'add' dialog
  $scope.openEditDialog = function (category) {

    $scope.currentModalSelector = '#edit-category-modal';
    $scope.resetForm();
    var curNode = $scope.cTreeObject.dynatree("getActiveNode");

    var onSaveChanges = function () {
      if (category) {
        $scope.editCategory(curNode);
      } else {
        $scope.addCategory(curNode);
      }
    };

    // save data on the popup for later use
    $scope.editCategoryPopup = {
      popupTitle: (category ? 'families_edit_family' : 'families_add_family'),
      popupSaveText: category ? 'families_save_changes' : 'families_add_family',
      cId: category ? category.id : null,
      cParentId: category ? category.parentId : null,
      cTitle: category ? category.title : null,
      cIconUrl: category ? category.logo : null,
      cStatus: category ? $scope.getCateogryFullDetailsById(category.status) : $scope.statuses[1],
      onConfirm: onSaveChanges
    };

    $scope.makeIconUrls();

    $($scope.currentModalSelector).modal('show');

  };

  $scope.getCateogryFullDetailsById = function (categoryId) {
    for (var i = 0; i < $scope.statuses.length; ++i) {
      if (categoryId == $scope.statuses[i].id)
        return $scope.statuses[i]
    }

    return null;
  };

  $scope.$watch("editCategoryPopup.cId", function () { $scope.makeIconUrls(); });

  $scope.makeIconUrls = function () {
    if ($scope.editCategoryPopup != null && $scope.editCategoryPopup.cId != null) {
      $scope.editCategoryPopup.cIconUrl2x = $scope.editCategoryPopup.cId + '@2x.png';
      $scope.editCategoryPopup.cIconUrl3x = $scope.editCategoryPopup.cId + '@3x.png';
      $scope.editCategoryPopup.cIconUrlsvg = $scope.editCategoryPopup.cId + '.svg';

      $(".extendedImage").attr("style", "display: inline;");
      $(".extendedImage").attr("onerror", 'this.style.display = "none"');
      /*$(".extendedImage").success(function () {
          $(this).show();
      });*/
    }
  };

  $scope.isImageLoaded = function (image) {
    switch (image) {
      case 0:
        return $("#Image2x")[0].getAttribute("style") == "display: none;";
    }
  };

  $scope.resetForm = function () {
    $('#file-upload-form').get(0).reset();
    $('.progress-upload-icon').hide();
    $('.progress-upload-icon2').hide();
  };

  $scope.openDeleteDialog = function (category) {
    var onDeleteConfirmed = function () {
      $scope.deleteCategory($scope.cTreeObject.dynatree("getActiveNode"), category.id);
    };
    var msg = $filter('translate')('delete_confirm');
    $scope.openConfirmationDialog('families_delete_category', msg + ' "' + category.title + '"?', onDeleteConfirmed);
  };

  $scope.openConfirmationDialog = function (title, message, onConfirm, okText, cancelText) {
    $scope.currentModalSelector = '#confirmation-modal';
    $scope.confirmationPopup = {
      title: title,
      message: message,
      onConfirm: onConfirm,
      okText: okText ? okText : 'ok',
      cancelText: cancelText ? cancelText : 'cancel'
    };
    $($scope.currentModalSelector).modal('show');
  };

  $scope.closeCurrentDialog = function () {
    $($scope.currentModalSelector).modal('hide');
  };

  /*
   * action handlers
   */

  $scope.onLanguageChange = function () {
    // TODO: use something more generic
    window.location = '/robin/' + $rootScope.tenantId + '/categories?lang=' + $scope.language.code;
  };

  /*
   * called after selecting an icon to upload.
   * server will upload the icon to a temp location, and will return the location
   */
  $scope.submitIconForm = function () {

    $scope.editCategoryPopup.fileTooBig = false;
    $scope.setLoading(true);
    var progressSelector = '.progress-upload-icon';
    robinCommon.submitFilesForm(
        '#file-upload-form', // form selector
        progressSelector,
        // url for sending the form
        '/mw/upload/icon/' + $rootScope.tenantId + '/category',
        // on success
        function (data) {
          $scope.editCategoryPopup.cIconUrl = data.tempKey;
          $scope.refreshImages(true);
          $(progressSelector).hide();
          $scope.setLoading(false);
        },
        // on error
        function (data) {
          if (data.status == 400) {
            $scope.editCategoryPopup.fileTooBig = true;
          } else {
            robinDialog.alert('Unable to upload icon, error: ' + data.statusText);
          }
          $(progressSelector).hide();
          $scope.setLoading(false);
        },
        // on before send
        function () {
          $(progressSelector).show();
        },
        // on during upload
        function (e) {
          if (e.lengthComputable) {
            $('progress').attr({
              value: e.loaded,
              max: e.total
            });
          }
        }
    );

  };

  $scope.showTranslationModal = function () {
    $scope.currentModalSelector = '#translations-modal';
    $($scope.currentModalSelector).modal('show');
  };

  $scope.showTranslationInfoModal = function () {
    $scope.currentModalSelector = '#translations-info-modal';
    $($scope.currentModalSelector).modal('show');
  };

  $scope.submitCsvTranslationsForm = function (iconType) {
    $scope.uploadingCsv = true;
    formSelected = '#file-upload-form-csv';
    $scope.setLoading(true);

    var progressSelector = '.progress-upload-icon2';
    robinCommon.submitFilesForm(
        formSelected, // form selector
        progressSelector,
        // url for sending the form
        '/mw/category/csv/' + $rootScope.tenantId,
        // on success
        function (data) {

          $scope.translations = data;

          $.map($scope.translations, function (a) {
            if (a.id == "-1") {
              a.langs.en_US = a.langs.erMsg;
              a.langs.zh_CN = a.langs.input;
              return a;
            }
            else return a
          });

          $scope.currentModalSelector = '#translations-modal';
          $($scope.currentModalSelector).modal('show');
          $scope.haveTranslations = true;
          $scope.uploadingCsv = false;

          $(progressSelector).hide();
          $scope.setLoading(false);
        },
        // on error
        function (data) {
          if (data.status == 400) {

          } else {
            robinDialog.alert('Unable to upload icon, error: ' + data.statusText);
          }
          $(progressSelector).hide();
          $scope.setLoading(false);
          $scope.uploadingCsv = false;
        },
        // on before send
        function () {
          $(progressSelector).show();
          $scope.uploadingCsv = true;
        },
        // on during upload
        function (e) {
          if (e.lengthComputable) {
            $('progress').attr({
              value: e.loaded,
              max: e.total
            });
          }
        }
    );
  };

  $scope.submitExtendedIconForm = function (iconType) {

    var iconName = "";
    var formSelected = "";
    switch (iconType) {
      case 0: {
        iconName = $scope.editCategoryPopup.cIconUrl2x;
        formSelected = '#file-upload-form2';
      } break;
      case 1: {
        iconName = $scope.editCategoryPopup.cIconUrl3x;
        formSelected = '#file-upload-form3';
      } break;
      case 2: {
        iconName = $scope.editCategoryPopup.cIconUrlsvg;
        formSelected = '#file-upload-form4';
      } break;
    }


    $scope.setLoading(true);
    var progressSelector = '.progress-upload-icon2';
    robinCommon.submitFilesForm(
        formSelected, // form selector
        progressSelector,
        // url for sending the form
        '/mw/upload/icon/' + $rootScope.tenantId + '/category?key=' + iconName,
        // on success
        function (data) {
          refreshExtendedIcons();
          $(progressSelector).hide();
          $scope.setLoading(false);
        },
        // on error
        function (data) {
          if (data.status == 400) {
            $scope.editCategoryPopup.fileTooBig = true;
          } else {
            robinDialog.alert('Unable to upload icon, error: ' + data.statusText);
          }
          $(progressSelector).hide();
          $scope.setLoading(false);
        },
        // on before send
        function () {
          $(progressSelector).show();
        },
        // on during upload
        function (e) {
          if (e.lengthComputable) {
            $('progress').attr({
              value: e.loaded,
              max: e.total
            });
          }
        }
    );

  };

  function refreshExtendedIcons() {
    $("#Image2x").attr("src", $scope.s3IconPathCategory + $scope.editCategoryPopup.cIconUrl2x + '?' + (new Date().getTime()));
    $("#Image3x").attr("src", $scope.s3IconPathCategory + $scope.editCategoryPopup.cIconUrl3x + '?' + (new Date().getTime()));
    $("#ImageSvg").attr("src", $scope.s3IconPathCategory + $scope.editCategoryPopup.cIconUrlsvg + '?' + (new Date().getTime()));

    $(".extendedImage").attr("style", "display: inline;");
    $(".extendedImage").attr("onerror", 'this.style.display = "none"');
  }

  $scope.isCategoryIconExists = function () {
    return $scope.editCategoryPopup != null && $scope.editCategoryPopup.cIconUrl != null && $scope.editCategoryPopup.cIconUrl != "";
  };

  $scope.clearIcon = function () {
    $scope.editCategoryPopup.cIconUrl = null;
  };

  // called after 'ok' is clicked at 'add category' popup
  $scope.addCategory = function (parentNode) {

    var ecp = $scope.editCategoryPopup;

    if (ecp.cTitle) {

      // clear errors
      ecp.titleRequired = false;

      // prepare data for sending
      var postData = {
        name: ecp.cTitle,
        logo: ecp.cIconUrl,
        status: ecp.cStatus.id,
        parentId: parentNode ? parentNode.data.id : null
      };

      $scope.loading = true;
      FamilyIO.addFamily(postData, $scope.language.code).success(function (data, status, headers, config) {
        // update tree (the 'data' returned is the key of the category just created)
        $scope.addCategoryNode(parentNode, data, ecp.cTitle, ecp.cIconUrl ? data : null, ecp.cStatus.id);
        // message the user
        robinDialog.alert('Family has been added.');
        // close dialog
        $scope.closeCurrentDialog();
        $scope.loading = false;
      }).error(function (data, status, headers, config) {
        robinDialog.alert('server error, please try again.');
        $scope.loading = false;
      });

    } else {
      // set error
      ecp.titleRequired = true;
    }

  };

  // called after 'ok' is clicked at 'edit category' popup
  $scope.editCategory = function (node) {

    var ecp = $scope.editCategoryPopup;

    var postData = {
      id: ecp.cId,
      name: ecp.cTitle,
      logo: ecp.cIconUrl,
      status: ecp.cStatus.id,
      parentId: ecp.cParentId
    };

    $scope.loading = true;
    FamilyIO.updateFamily(postData, ecp.cId).success(function (data, status, headers, config) {
      // update tree (the 'data' returned is the key of the category, which is also its logo content)
      $scope.updateCategoryNode(node, ecp.cTitle, ecp.cIconUrl ? data : null, ecp.cStatus.id);
      // message the user
      robinDialog.alert('family has been updated.');
      // finish the flow
      $scope.closeCurrentDialog();
      $scope.loading = false;
    }).error(function (data, status, headers, config) {
      robinDialog.alert('server error, please try again.');
      $scope.loading = false;
    });

  };

  // called after 'ok' is clicked at 'delete category' popup
  $scope.deleteCategory = function (node, id) {
    $scope.loading = true;
    FamilyIO.deleteFamily(id).success(function (data, status, headers, config) {
      // update tree
      $scope.removeCategoryNode(node);
      // message the user
      robinDialog.alert('Family has been deleted.');
      // finish the flow
      $scope.closeCurrentDialog();
      $scope.loading = false;
    }).error(function (data, status, headers, config) {
      robinDialog.alert('server error, please try again.');
      $scope.loading = false;
    });
  };

  /*
   * functions handling tree order editing
   */

  $scope.editTreeOrder = {

    isEditable: false,

    onEditOrder: function () {
      // save current tree data
      this.curTreeData = $scope.cTree.toDict();
      // enter edit mode
      this.isEditable = true;
    },

    onDoneEditingOrder: function () {
      // save tree order
      var that = this;
      $scope.loading = true;

      var buildCatTree = function (nodes) {
        var catTree = [];
        $.each(nodes, function (index, node) {
          catTree.push({
            id: node.id,
            children: node.children ? buildCatTree(node.children) : []
          });
        });
        return catTree;
      };
      var tree = buildCatTree($scope.cTree.toDict());
      FamilyIO.updateFamilyTree(tree, $scope.language.code).success(function (data, status, headers, config) {
        // exit edit mode
        that.isEditable = false;
        $scope.loading = false;
      }).error(function (data, status, headers, config) {
        robinDialog.alert('server error, please try again.'); // TODO: improve this error handling
        $scope.loading = false;
      });
    },

    onCancelEditOrder: function () {
      // revert tree structure
      $scope.updateTreeData(this.curTreeData);
      $scope.cTree.reload();
      this.isEditable = false;
    }
  };

  $scope.getDescriptionByStatus = function (status) {

    switch (status) {
      case 0: return "Deleted";
      case 1: return "Active";
      case 2: return "Hidden";
      case 3: return "Private";
      case 4: return "Group";
      default: return "Unknown Status";
    }
  };

  /*
   * utils
   */

  $scope.setLoading = function (toggle) {
    $scope.loading = toggle;
    $scope.$digest();
  };

  $scope.refreshImages = function (digest) {
    $scope.extTimestamp = '?' + (new Date().getTime());
    if (digest)
      $scope.$digest();
  };

  $scope.isUploadingCsv = function () {
    return $scope.uploadingCsv;
  };

  /*
   * c'tor
   */
  $scope.refreshCategoriesTree();
  /*$(".extendedImage").error(function () {
      $(this).hide();
  });*/


}]);
