robinApp.controller('CtrlNavigator',
    ["$rootScope", "$scope", "$http", "UserService", "AuthService", "AUTH_EVENTS", '$location',
    function($rootScope, $scope, $http, UserService, AuthService, AUTH_EVENTS, $location) {

    $scope.title = "Robin";
    $scope.dbg = ($location.search().dbg === "true");

    $scope.showAbout = function(){
        robinDialog.alert('Robin, version ' + robinConsts.appVersion);
    };

    $scope.logout = function () {
        AuthService.logout().then(function() {
            $rootScope.redirectToLogin();
        }, function(message) {
            // Pop up error window.
        });
    };

    var update = function () {
        $scope.moduleId = "";
        $scope.user = UserService.get();

        var user = $scope.user;
        var tenantId = $rootScope.tenantId;

        // Tags group
        $scope.category = user && user.checkPermission(tenantId, AppModule.Categories);
        $scope.brand = user && user.checkPermission(tenantId, AppModule.Brands);
        $scope.retailer = user && user.checkPermission(tenantId, AppModule.Retailers);
        $scope.attribute = user && user.checkPermission(tenantId, AppModule.Attributes);
        $scope.family = user && user.checkPermission(tenantId, AppModule.Families);

        // Products group
        $scope.products = user && user.checkPermission(tenantId, AppModule.Products);
        $scope.searchFamily = user && user.checkPermission(tenantId, AppModule.SearchFamily);
        $scope.categorization = user && user.checkPermission(tenantId, AppModule.ProductsCategorization);

        // User group
        $scope.users = user && user.checkPermission(tenantId, AppModule.Users);

        // Only admin and current tenant is "hsm" have permission
        $scope.migration = user && user.checkPermission(tenantId, AppModule.Migration) && tenantId ==='hsm';

        // Only admin and tenant_admin can edit tenant
        $scope.editTenant = user && user.checkPermission(tenantId, AppModule.EditTenant);

        $scope.showTenant = !$scope.editTenant && user && tenantId != "";
    };
    $scope.$on(AUTH_EVENTS.loginSuccess, update);
    $scope.$on(AUTH_EVENTS.loginFailed, update);
    $scope.$on(AUTH_EVENTS.logoutSuccess, update);
    $scope.$on(AUTH_EVENTS.sessionTimeout, update);
    $scope.$on(AUTH_EVENTS.notAuthenticated, update);
    $scope.$on(AUTH_EVENTS.authenticated, update);

    update();
}]);
