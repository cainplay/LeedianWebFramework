var robinConsts = {
    env: 'ezhome-uat',
    appVersion: '2.0.0',
    lang: 'zh-CN',

    s3IconPathCategory: 'https://juran-staging-assets.s3.cn-north-1.amazonaws.com.cn/Categories/NewVersion/',
    //s3IconPathTenant: 'https://hsm-dev-assets.s3.amazonaws.com/Tenants/',
    //s3ISOPathCategory: 'https://hsm-dev-assets.s3.amazonaws.com/Categories/iso/',

    // js global values
    languages: [
        { code: "en_US", name: "English" },
        { code: "zh_CN", name: "Chinese" },
        { code: "de_DE", name: "German" },
        { code: "es_ES", name: "Spanish" },
        { code: "fr_FR", name: "French" },
        { code: "it_IT", name: "Italian" },
        { code: "ja", name: "Japanese" },
        { code: "pt_BR", name: "Portuguese" },
        { code: "ru", name: "Russian" }
    ],

    apiEndpoint: "http://uat-pim.homestyler.com",

    URLFor3DViewer: "http://uat-3d.homestyler.com/viewer3d.html?t=ezhome&seekid="
};
