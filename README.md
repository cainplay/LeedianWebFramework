product-management-tool
=======================
This project is for Homestyler team to build product management tool.
Check out the running version of tool:
* Global:
    - Alpha: http://alpha-pbo.homestyler.com
    - Beta: http://beta-pbo.homestyler.com
    - Prod: http://pbo.homestyler.com
* EZHome:
    - UAT: http://uat-pbo.homestyler.com
    - Prod: http://ezhome-pbo.homestyler.com


## Set up dev environment

### Get code
Install following tools:
* Git: https://git-scm.com/download

Get the repo cloned to local using Git:
`$ git clone git@github.com:AutodeskHome/Robin-Portal.git`

### Install development tools
* node and npm: https://nodejs.org/
    * Mac: it's better to install node by brew on OS X

### Up and run
* Run `npm install` to install dependencies managed by npm.
* Run `npm run dev` to start develop version tool. The web server: http://localhost:8001/
* Run `npm run minified` to start minified version tool. The web server: http://localhost:8002/ (*Always check the AngularJs injection codes with this version*)


## Build

### Local Build
* Run `npm run build` to compile release (minified) build. The minified version will be in `/webminified` folder
* Run `npm run package` to package the minified web files. The archive is in `/dist` folder

### Build and Deploy with CI system
* Run `./build/build <env>` to build a environment specific version. The env value is:
    - alpha
    - beta
    - prod
    - ezhome-uat
    - ezhome-prod
* Run `./build/deploy <env>` to upload the environment specific version to AWS S3. The env value is:
    - alpha
    - beta
    - prod
    - ezhome-uat
    - ezhome-prod


## Test
Run `npm test` to execute test scripts

## Contacts
* Apply for code access authority(write): <Jeffrey.Sun@autodesk.com>
* Issues in setting up dev environment: <senhua.liu@autodesk.com>

## Tips && FAQ
### Folder structure
* `build/` the build and deploy scripts
* `configuration/` config definitions that targets different environments
* `node_modules/` npm managed folder for installing dependency packages
* `web/` main web app source code
    * `index.html` the home page, which reference the asset files at `appversion` folder
    * `appversion` the asset files referenced by index.html. This folder will be renamed as `BUILD_VERSION` in the release version.
        * `app/` the JavaScript files
            * `core/` the core scripts, raw JavaScript file.
            * `components/` the app functionality component.

        * `lib/` the 3rd party libs that can't be managed by npm
        * `res/` the app static assets files: css/image, etc
* `webminified/` the compiled (minified) version
* `dist/` the distribute package for this tool
* `Gruntfile.js` the build configuration file used by grunt which is a javascript task runner to support automation task: magnification, compress, testing, linting, etc.
* `.jshint` the grunt jshint tool configuration

